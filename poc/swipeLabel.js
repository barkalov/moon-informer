(function ($) {

    var methods = {
        init: function (optionsExplit) {

            // Create some defaults, extending them with any options that were provided
            globalOptions = $.extend({
                "duration": 100,
                "direction": "left",
                "content": null,
                "contentWidthMode": "inline", // or "self"
                "constantBlur": 0,

            }, optionsExplit);

            return this.each(function () {

                var $this = $(this),
                    data = $this.data('swipeLabel'),
                    swipeLabel;                
                var options = globalOptions;                
                // If the plugin hasn't been initialized yet
                if (!data) {

                    $this.addClass("swipeLabel")
                    var existedContent = $this.html();
                    $this.html("")

                    if (existedContent) {
                        var present = $("<div/>")
                            .addClass("present")
                            .css({
                                "overflow": "hidden",
                                "white-space": (options.contentWidthMode === "inline") ? "nowrap": "inherit",
                                "display": "inline-block",
                                "margin-bottom": "-2px"
                                });
                        var presentSpan= $("<span/>")
                            .html(existedContent)
                            .appendTo(present);
                        present.appendTo($this);

                        if (options.contentWidthMode === "self") {
                            presentSpan.css({
                                width: $this.width(),
                                position: "relative",
                                display: "block",
                            });
                            present.css({
                                float: "left",
                            });
                        }

                    }
                    
                     

                    $this.data("swipeLabel", {
                        target: $this,
                        options: options
                    });
                } else {
                    options = $.extend(data.options, optionsExplit); //update explicit
                    $this.data("swipeLabel", {
                        options: options
                    });

                }

                //HotStart/reuse from here                
                if ((optionsExplit!=undefined)&&(optionsExplit.content != null)) {
                    var goner = $(".present", $this);
                    var newPresent = goner.clone();
                    goner.removeClass("present").addClass("goner")
                        .stop(true,false)
                        .animate({
                            "width": 0,
                            "opacity": 0
                        }, options.duration, function () { goner.remove() });
                    var newPresentSpan = $("span", newPresent);

                    if (options.contentWidthMode === "self") {
                        newPresentSpan.css({
                            width: $this.width(),
                            position: "relative",
                            display: "block",
                        });
                        newPresent.css({
                            float: "left",
                        });
                    } else {
                        newPresentSpan.css({
                            width: "auto",
                            position: "static",
                            display: "inline",
                        });
                    };
                                    
                    
                        newPresentSpan.html("");
                    if ($.type(options.content) === "string") { //do need i?        
                        newPresentSpan.html(options.content);                 
                    } else {
                        options.content.appendTo(newPresentSpan);
                    }

                    newPresent
                        .css({"width": 0, //collapse before adding to document and before animate
                        "opacity": 0}); //collapse before adding to document and before animate
                    if (options.direction=="left"){
                        newPresent.appendTo($this);
                    } else {
                        newPresent.prependTo($this);
                    }
                    
                    var widthGoal = (options.contentWidthMode === "inline") ? newPresentSpan.width() : $this.width();                    
                    newPresent.animate({
                        "width": widthGoal,
                        "opacity": 1
                    }, options.duration);
                    

                    $this.stop(true,false).animate({
                        "min-width": widthGoal,
                    }, Math.max(options.duration,200));
                }

                //_constantBlur
                var shadesCount = Math.min(10, Math.round(Math.abs(options.constantBlur / 1)));
                var cssShadowSet = "";
                for (var i = 0; i < shadesCount; i++) {
                    iBri = i / shadesCount; //beware shadesCount=0, pass in loop only i>0!
                    cssShadowSet += + (iBri * Math.sqrt(Math.abs(options.constantBlur)) * 9) + "px 0 0 rgba(255,255,255," + iBri/2 + "), ";
                    cssShadowSet += -(iBri * Math.sqrt(Math.abs(options.constantBlur)) * 9) + "px 0 0 rgba(255,255,255," + iBri / 2 + "), ";
                }
                $this.css("text-shadow", cssShadowSet.slice(0, -2));                


            });
        },
        destroy: function () {

            return this.each(function () {

                var $this = $(this),
                    data = $this.data('swipeLabel');

                // Namespacing FTW
                $(window).unbind('.swipeLabel');
                data.swipeLabel.remove();
                $this.removeData('swipeLabel');

            })

        },
        reposition: function () { },
        show: function () { },
        hide: function () { },
        update: function (content) { }
    };

    $.fn.swipeLabel = function (method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.swipeLabel');
        }

    };
})(jQuery);