(function ($) {

    var methods = {
        init: function (optionsExplit) {

            // Create some defaults, extending them with any options that were provided
            globalOptions = $.extend({
                "outlayWidth": 15,
                "outlayTop": 2,
                "outlayBottom": 4,
                "hovered": false,
                
                //callbacks
                clickLeft: null,
                clickRight: null
            }, optionsExplit);
            return this.each(function () {
                function _updateCss(wrapper, trangleLeft, trangleRight) {
                    if (options.hovered) {
                        
                        wrapper.css({
                            "margin":0,
                            "padding":0,
                            "background-color": "transparent"                            
                        });
                        
                        trangleLeft.add(trangleRight).css({
                            display:none,
                        });
                                                                        
                    } else {

                        wrapper.css({
                            "margin-left": -options.outlayWidth,
                            "margin-right": -options.outlayWidth,
                            "margin-top": -options.outlayTop,
                            "margin-bottom": -options.outlayBottom,
                            "padding-top": options.outlayTop,
                            "padding-bottom": options.outlayBottom,
                            "background-color": "rgba(0,0,0,0.7)"                            
                        });
                        
                        trangleLeft.add(trangleRight).css({
                            display:"inline-block",
                        });
                            
                    }                



                var $this = $(this),
                    data = $this.data('trangleWrapper'),
                    options = globalOptions;
                var wrapper, trangleLeft, trangleRight;
                
                // If the plugin hasn't been initialized yet
                if (!data) {

                    wrapper = $("<div/>"); //not span, because inner can not be div in that case (atr least gecko thinks), but it's almost span displayed inline-block
                    wrapper.css({ display: "inline-block" });
                    trangleLeft = $("<div/>"); 
                    trangleRight = $("<div/>");

                    $this.wrap(wrapper).preppend(trangleLeft).append(trangleRight);

                    $this.addClass("trangleWrapper");
                    var existedContent = $this.html();
                    $this.html("");

                    present = _buildPresent(existedContent).appendTo($this);

                } else {
                    wrapper = data.wrapper;
                    trangleLeft = data.trangleLeft;
                    trangleRight = data.trangleRight;
                    options = $.extend({}, data.options, optionsExplit); //update explicit
                }



                //HotStart/reuse from here                


                //if ((optionsExplit != undefined) && (optionsExplit.content != null))
                    

                $this.data("trangleWrapper", {
                    options: options,
                    wrapper: wrapper,
                    trangleLeft: trangleLeft,
                    trangleRight: trangleRight
                });


            });
        },
        destroy: function () {

            return this.each(function () {

                var $this = $(this),
                    data = $this.data('trangleWrapper');

                // Namespacing FTW
                $(window).unbind('.trangleWrapper');
                data.trangleWrapper.remove();
                $this.removeData('trangleWrapper');

            });

        },
        reposition: function () { },
        show: function () { },
        hide: function () { },
        update: function (content) { }
    };

    $.fn.trangleWrapper = function (method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.trangleWrapper');
        }

    };
})(jQuery);