﻿"use strict";
(function ($) {
    // =================
    //
    // swipeLabel plugin
    //
    // =================
    var methods = {
        init: function (optionsExplit) {

            // Create some defaults, extending them with any options that were provided
            var globalOptions = $.extend({
                "duration": 100,
                "direction": "left",
                "content": null,
                "contentWidthMode": "inline", // or "self" or "manual"
                "contentWidth": null, // may be set for manual mode 
                "constantBlur": 0,
                "charsAt100": null, // if not null, used to scale font-size
                "uniPos": null // if not null, used to shift up-duwn

            }, optionsExplit);
            return this.each(function () {

                function _fillPresentSpan(content, presentSpan) { //part of _buildPresend, used for optimized part-execution
                    if ($.type(content) === "string") { //do need i?        
                        presentSpan.html(content);
                    } else {
                        presentSpan.html("");
                        content.appendTo(presentSpan);
                    }

                    presentSpan.css({
                        "font-size": (options.charsAt100 == null) ? "100%" : (Math.sqrt(options.charsAt100 / presentSpan.text().length) * 100) + "%",
                        "line-height": (options.charsAt100 == null) ? "inherit" : "120%",
                    });

                    if (options.contentWidthMode !== "inline") {
                        presentSpan.css({
                            width: (options.contentWidthMode === "self") ? $this.width() : options.contentWidth, //or "manual"
                            position: "relative",
                            display: "block",
                        });
                    } else { //if inline
                        presentSpan.css({
                            width: "auto",
                            position: "static",
                            display: "inline",
                        });
                    }
                    return presentSpan;

                }
                function _updatePresent(buildedPresent) { //part of _buildPresend, used for optimized part-execution: when mode switched at run-time and reuse existed goner-presents stuff
                    return buildedPresent.css({
                        "white-space": (options.contentWidthMode === "inline") ? "nowrap" : "inherit",
                        float: (options.contentWidthMode === "inline") ? "none" : "left",
                        width: (options.contentWidthMode === "self") ? $this.width()
                                : (options.contentWidthMode === "manual") ? options.contentWidth
                                : "auto", //when "inline"
                    });
                }
                function _buildPresent(content, respawnedGoner) {
                    var goner = respawnedGoner ? respawnedGoner : $("<div/>"); //default value = fresh div
                    var buildedPresent = goner
                        .removeClass("goner")
                        .addClass("present")
                        .css({
                            "overflow": "hidden",
                            "display": "inline-block",
                            "margin-bottom": "-2px",
                        });
                    _updatePresent(buildedPresent);
                    var presentSpan = (respawnedGoner) ? respawnedGoner.presentSpan : $("<span/>").appendTo(buildedPresent);
                    _fillPresentSpan(content, presentSpan);
                    buildedPresent.presentSpan = presentSpan;
                    return buildedPresent;


                }

                var $this = $(this),
                    data = $this.data('swipeLabel'),
                    options = globalOptions;
                var present, //dom (jquery) onbject
                    grave = []; //object pool, empty on start
                // If the plugin hasn't been initialized yet
                if (!data) {

                    $this.addClass("swipeLabel");
                    var existedContent = $this.html();
                    $this.html("");

                    present = _buildPresent(existedContent);
                    present.appendTo($this);

                } else {
                    present = data.present;
                    grave = data.grave;
                    options = $.extend(data.options, optionsExplit); //update explicit
                }



                //HotStart/reuse from here                
                if ((optionsExplit != undefined) && (optionsExplit.content != null)) {
                    var newPresent,
                        newPresentSpan;
                    if (options.duration > 30) {


                        var goner = present;
                        newPresent = grave.pop();
                        newPresent = _buildPresent(options.content, newPresent); //fire in both cases, create new or update graved - see logic in function

                        goner.removeClass("present").addClass("goner")
                            .stop(true, false)
                            .animate({
                                "width": 0,
                                "opacity": 0
                            }, options.duration, function () {
                                //goner.remove()
                                goner.css({ display: "none" });
                                (grave.length < 10) ? grave.push(goner) : goner.remove();
                            });



                        newPresent
                            .css(
                            {
                                width: 0, //collapse before adding to document and before animate
                                opacity: 0,
                                display: "inline-block"
                            }); //collapse before adding to document and before animate
                        if (options.direction === "left") {
                            newPresent.appendTo($this);
                        } else {
                            newPresent.prependTo($this);
                        }

                        newPresentSpan = newPresent.presentSpan;


                        var widthGoal = (options.contentWidthMode === "inline") ? newPresentSpan.width() : (options.contentWidthMode === "self") ? $this.width() : options.contentWidth;  //or "manual"
                        newPresent.animate({
                            "width": widthGoal,
                            "opacity": 1
                        }, options.duration);

                        $this.stop(true, false).animate({
                            "min-width": widthGoal,
                        }, Math.max(options.duration, 200));


                    } else { //fast 

                        newPresent = present;
                        newPresentSpan = newPresent.presentSpan;
                        _fillPresentSpan(options.content, newPresentSpan);
                        _updatePresent(newPresent);

                        var widthGoal = (options.contentWidthMode === "inline") ? newPresentSpan.width() : (options.contentWidthMode === "self") ? $this.width() : options.contentWidth;  //or "manual"

                        $this.stop(true, false).css({ "min-width": widthGoal });

                    }





                    present = newPresent;

                }



                //_constantBlur
                var shadesCount = Math.min(10, Math.round(Math.abs(options.constantBlur / 1)));
                var cssShadowSet = "";
                for (var i = 0; i < shadesCount; i++) {
                    var iBri = i / shadesCount; //beware shadesCount=0, pass in loop only i>0!
                    cssShadowSet += +(iBri * Math.sqrt(Math.abs(options.constantBlur)) * 9) + "px 0 0 rgba(255,255,255," + iBri / 2 + "), ";
                    cssShadowSet += -(iBri * Math.sqrt(Math.abs(options.constantBlur)) * 9) + "px 0 0 rgba(255,255,255," + iBri / 2 + "), ";
                }
                $this.css("text-shadow", cssShadowSet.slice(0, -2));


                //_uniPos
                if (options.uniPos !== null) {
                    var presentHeight = present.height();
                    var swipeHeight = $this.height();
                    var hiddenPartHeight = swipeHeight - presentHeight;
                    present.css(
                    {
                        "margin-top": Math.round(hiddenPartHeight * options.uniPos)
                    });

                } else { // options.uniPos===null
                    present.css(
                    {
                        "margin-top": 0
                    });
                }


                $this.data("swipeLabel", {
                    options: options,
                    present: present,
                    grave: grave,
                });


            });
        },
        destroy: function () {

            return this.each(function () {

                var $this = $(this),
                    data = $this.data('swipeLabel');

                // Namespacing FTW
                $(window).unbind('.swipeLabel');
                data.swipeLabel.remove();
                $this.removeData('swipeLabel');

            });

        },
    };
    $.fn.swipeLabel = function (method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.swipeLabel');
        }

    };



    
    // =================
    //
    // you know it
    //
    // =================
    window.requestAnimationFrame || (window.requestAnimationFrame =
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function (callback, element) {
        return window.setTimeout(function () {
            callback(+new Date());
        }, 1000 / 30);
    });




    // =================
    //
    // astronomic out-of-the-widget functionality aviable from outer scope
    //
    // =================
    var astro = {
        //functions
        normalize: function (value) {
            value = value % 1;
            if (value < 0) { value += 1; }
            return value;
        },
        dateToJDays: function (date) { //original Zeno code by Stephen R. Schmitt
            var cDay = date.getDate();
            var cMonth = date.getMonth() + 1;
            var cYear = date.getFullYear();

            // calculate the Julian date at 12h UT
            var jYear = cYear - Math.floor((12 - cMonth) / 10);
            var jMonth = cMonth + 9;
            if (jMonth >= 12) { jMonth -= 12; }

            var k1 = Math.floor(365.25 * (jYear + 4712));
            var k2 = Math.floor(30.6 * jMonth + 0.5);
            var k3 = Math.floor(Math.floor((jYear / 100) + 49) * 0.75) - 38;

            var jDays = k1 + k2 + cDay + 59; // for dates in Julian calendar
            if (jDays > 2299160) {
                jDays -= k3; // for Gregorian calendar
            }
            return jDays + astro.timeOffset / (60 * 24);
        },
        jDaysToDate: function (jDays) {
            var a = jDays + 32044 - astro.timeOffset / (60 * 24);
            var b = Math.floor((4 * a + 3) / 146097);
            var c = a - Math.floor(146097 * b / 4);
            var d = Math.floor((4 * c + 3) / 1461);
            var e = c - Math.floor((1461 * d) / 4);
            var m = Math.floor((5 * e + 2) / 153);

            var day = e - Math.floor((153 * m + 2) / 5) + 1;
            var month = m + 3 - (12 * Math.floor(m / 10));
            var year = 100 * b + d - 4800 + Math.floor(m / 10);
            return new Date(year, month - 1, day);
        },
        bvToRgb: function (bv) {
            var result = {};
            result.r = Math.round(Math.max(0, Math.min(255, 255 - Math.max(0, -bv * 50))));
            result.b = Math.round(Math.max(0, Math.min(255, 255 - Math.max(0, bv * 50))));
            result.g = Math.round((result.r + result.b) / 2);
            return result;
        },
        vToOpacity: function (v, mulOnFive) {
            if (mulOnFive === undefined) mulOnFive = 100;
            return Math.pow(mulOnFive, -v / 5);
        },
        // constants
        timeOffset: new Date().getTimezoneOffset(),
        jMoonDayLongs: 29.530588853,
        jNewShift: -2451550.1,
    };




    // =================
    //
    // informer widget
    //
    // =================
    $.widget("moon.informer", {
        // default options
        options: {
            sprites: {
                ballWidth: 190, //px mouse = 360 degrees engine - 1/28.. of jDay
                ballHeight: 145,
                ballFramesCountMidHifi: 90,
                ballFramesCountHifiAlpha: 30,
                ballFramesCountLofi: 30,
                ballFramesUrlMidHifi: "moon-informer/img/atlas-190x145x90.jpg",
                ballFramesUrlLofi: "moon-informer/img/atlas-190x145x30.jpg",
                ballFramesUrlHifiAlpha: "moon-informer/img/atlas-alpha-190x145x90.png"
            },

            locale: {
                monthNames: ["января", "февраля", "матра", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
                weekDays: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],                
                //moonStages: [                    
                //    "новолуние",
                //    "растущий месяц",
                //    "I четверть",
                //    "растущая луна",
                //    "полнолуние",
                //    "убывающая луна",
                //    "III четверть",
                //    "спадающий месяц"
                //],
                //moonStages: [                    
                //    "новолуние",
                //    "I четверть",
                //    "полнолуние",
                //    "III четверть"
                //],               
                moonStages: [
                    "новолуние",
                    "IV четверть",
                    "I четверть",
                    "II четверть",
                    "полнолуние",
                    "II четверть",
                    "III четверть",
                    "IV четверть"
                ],

                
                zodiacNames: ["Овен", "Телец", "Близнецы", "Рак", "Лев", "Дева", "Весы", "Скорпион", "Стрелец", "Козерог", "Водолей", "Рыбы"],
                booleanshit: {
                    weekDayDetails: [
                        "Сегодня велика вероятность того, что сон вещий, в особенности для тех, <b>кто родился в понедельник</b>.",
                        "Сны вторника важны и могут сбыться, но только <b>не позднее чем через десять дней</b>.",
                        "Сон может быть вещим, если приснится <b>до начала</b> новых лунных суток.",
                        "В этот день большинство снов являются вещими.",
                        "Сон, скорее всего, исполнится, если он как-то связан с личной жизнью.",
                        "Сегодня стоит уделять внимание тем снам, которые вы видели <b>в предрассветные часы</b>.",
                        "Сны сегодня очень важны, особенно те, что описывают <b>сюжеты, связанные с развлечениями и удовольствиями</b>."
                    ],
                    moonDayDetails: [
                        "К тому же, сегодня вы можете по средствам сна предугадать грядущие перемены.",
                        "Однако если сон не имеет ясного сюжета, значения ему лучше не придавать.",
                        "К тому же, события сна довольно скоро станут явью.",
                        "<span class='ml'>Также сегодня сюжет сновидения может содержать</span><b>важные предостережения</b>.",
                        "<span class='ml'> тому же, сегодня сны способны нести информацию о состоянии здоровья.</span>",
                        "Однако, отдельные события сна могут стать явью и через продолжительное время.",
                        "Сон может стать явью, если вы <b>сохраните его в тайне</b>.",
                        "Как правило, сегодняшний сон, так или иначе, связан с осуществлением заветной мечты.",
                        "И нельзя забывать о том, что сегодня сон может предостеречь от беды.",
                        "К тому же, сегодняшний сон в некоторых случаях может сулить неприятные события.",
                        "Однако, сегодняшний сон подчиняется влиянию Луны и если сбудется, то только через <b>11 дней</b>.",
                        "Помните, что сегодня сны могут дать <b>важные советы</b>.",
                        "Некоторые из снов могут нести предостережения о развитии прежних проблем.",
                        "Большинство сновидений сегодня обманчиво пугающие, не стоит придавать таким сюжетам существенного значения.",
                        "Однако, сегодня Луна делает сны очень важными, они способны предсказывать скорые <b>приятные перемены</b>.",
                        "Некоторые сны этого дня под влиянием Луны способны выражать накопившиеся эмоции.",
                        "Если вы в замешательстве, сегодняшний сон может дать важную подсказку.",
                        "Сон о какой либо вещи сегодня может сулить ее скорое приобретение.",
                        "Если в семье накалена обстановка, сон этого дня может предупреждать о серьезных семейных неприятностях.",
                        "Сны этого дня могут дать ответ на вопрос, который вас мучил последнее время и в особенности в день накануне.",
                        "Плохие сны сегодня это, скорее исключение из правил, но их ни в коем случае нельзя недооценивать.",
                        "К тому же, сегодня некоторые сны могут нести предупреждение о неприятностях.",
                        "Некоторые сюжеты снов этого дня могут быть связаны с событиями прошлого.",
                        "Так же, сегодняшний сон может отражать то, насколько вы довольны собой.",
                        "В сюжете сегодняшних снов так же могут быть предупреждения о  том, что вас кто-то обманет.",
                        "Некоторые из сегодняшних снов способны пробудить желание действовать,  таким сюжетам стоит верить.",
                        "Иногда в сегодняшних снах можно встретить подсказки интуиции.",
                        "Вам следует обратить внимание на сны, которые намекают на препятствия в делах.",
                        "Тревожные сны сегодня обманчивы.",
                        "Если ваш сон имеет фантастический сюжет, его толкование должно быть внимательным, смысл его скрыт в образах."
                    ],
                    monthDayDetails: [
                        "И не забудьте, сегодняшние сны намекают вам на то, как вы близки к своей цели.",
                        "Помните, сегодняшний сон может подсказать о том, к чему могут привести проделанная вами работа.",
                        "Уделите внимание снам с эмоциональной окраской, они могут предупреждать о судьбоносных переменах.",
                        "При толковании важно обратить внимание на то, насколько эмоционально сновидец переживает события сна.",
                        "Обратите внимание на тот сон, который вас напугал, он особенно важен.",
                        "Обратите внимание на то, что часть снов этого дня может быть связана со здоровьем.",
                        "Знайте, сегодня сон может предупредить вас о некой грядущей и важной новости.",
                        "Помните, что довольно часто сны сегодняшнего дня говорят о том, насколько вы будете обеспечены материально в будущем.",
                        "Особое внимание уделите событиям, которые случились в середине сюжета сна.",
                        "Не оставляйте без внимания пугающие сюжеты, они могут вас предупреждать об опасности.",
                        "При толковании сконцентрируйте внимание на самом начале сновидения.",
                        "Сегодняшний сон о беременности стоит воспринимать только буквально.",
                        "Обратите внимание на самые неприятные моменты сновидения, именно им следует придать ключевое значение.",
                        "Если сон не имел четкого сюжета, придавать ему значения не стоит.",
                        "Сегодня сон может подсказать, что нужно сделать для достижения цели.",
                        "Особенно важны сегодня цветные сновидения.",
                        "Если сон сегодня был очень приятным, это крайне дурной знак.",
                        "Помните, сегодня сны особенно важны, если привиделись они при дождливой погоде.",
                        "Если сны сегодняшнего дня способны подтолкнуть к решительным поступкам, их следует принять в серьез.",
                        "В этот день сны женщин крайне важны, а вот мужчинам лучше не принимать увиденное на веру.",
                        "Обратите внимание на сны, которые видели до полуночи.",
                        "Так же сегодня крайне важны сны, что привиделись до полуночи.",
                        "Сны раннего утра могут оказаться вещими.",
                        "Если сон намекает на грядущие болезни, уделите ему особенное внимание.",
                        "Следует быть внимательнее к сновидениям с участием близких.",
                        "И знайте, сегодняшний сон во многом связан с вашим материальным положением.",
                        "Если вы стоите перед сложным выбором, сегодняшний сон может вам подсказать, как быть.",
                        "Все, что вас напугало во сне сегодня, следует анализировать очень внимательно.",
                        "Особенно важны сегодня сны, в которых можно расслышать чей-то голос.",
                        "Если вы сегодня внезапно проснулись, то сновидение, которое было последним, особенно важно.",
                        "Сны этого дня могут быть предупреждением т о крайне внезапных событиях."
                    ],
                },                
            },            
            //moonStages: [
            //    0.125, //"новолуние",
            //    0.375, //"I четверть",
            //    0.625, //"полнолуние",
            //    0.875 //"III четверть",
            //],
            //moonStages: [                
            //    0.0625, //"новолуние",
            //    0.1875, //"растущий месяц",
            //    0.3125, //"I четверть",
            //    0.4375, //"растущая луна",
            //    0.5625, //"полнолуние",
            //    0.6875, //"убывающая луна",
            //    0.8125, //"III четверть",     
            //    0.9375  //"спадающий месяц"
            //],
            moonStages: [                                                                                                   
                0.05, //"новолуние",
                0.125, //"IV четверть",
                0.375, //"I четверть",
                0.45, //"II четверть",
                0.55, //"полнолуние",
                0.625, //"II четверть",
                0.875, //"III четверть",
                0.95 //"IV четверть",
            ],

            links: {
                root: "http://felomena.com",
                target: "_self",
                zodiac: [
                    "/goroskopy/znak/oven/",
                    "/goroskopy/znak/telec/",
                    "/goroskopy/znak/bliznecy/",
                    "/goroskopy/znak/rak/",
                    "/goroskopy/znak/lev/",
                    "/goroskopy/znak/deva/",
                    "/goroskopy/znak/vesy/",
                    "/goroskopy/znak/skorpion/",
                    "/goroskopy/znak/strelec/",
                    "/goroskopy/znak/kozerog/",
                    "/goroskopy/znak/vodolej/",
                    "/goroskopy/znak/ryby/"
                ],

                moonStages: [
                    "/luna/fazy/novolunie/",
                    "/luna/fazy/chetvertaya/",
                    "/luna/fazy/pervaja/",
                    "/luna/fazy/vtoraya/",
                    "/luna/fazy/polnolunie/",
                    "/luna/fazy/vtoraya/",
                    "/luna/fazy/tretia/",
                    "/luna/fazy/chetvertaya/"
                ],
                esotericDay: "/luna/kalendar/den-",
                monthDay: "/sonnik/kalendar/den-"
            },
            sizes: {
                span1: 105,
                span2: 220,
                span2CharsAt100: null, //how many chars need to whole fit. null means classic unscaled-mode
                span3: 90,
                margin: 10,
            },
            blocks: {
                span1: true,
                span3: true,
                    
            },

            mode: "lofi",
            activeMode: "hifi",
            politeDelay: 5000, //msecs to wait before _firstTouch() init fire
            
            // callbacks
            change: null,
        },
        booleanshit: {
            weekDay: [33, 15, 17, 30, 17, 17, 25],
            moonDay: [30, 0, 30, 25, 25, 15, 30, 30, 30, 30, 0, 25, 30, 5, 30, 25, 15, 15, 5, 30, 3, 30, 0, 15, 15, 17, 10, 30, 0, 10],
            monthDay: [30, 3, 33, 25, 31, 15, 15, 16, 10, 28, 11, 30, 28, 30, 32, 30, 20, 33, 25, 30, 17, 14, 15, 32, 30, 30, 15, 10, 0, 5, 15]
        },
        _engine: {
            position: astro.dateToJDays(new Date()), //jDays
            speed: 0, //jDaysPerSec
            mouseUniPos: 0, //in 0to1 uni
            sleep: false,
            freefly: {
                interval: null, //used for setinterval canceling                
                position: astro.dateToJDays(new Date()), //smooth, not actual version
                speed: 0, //smooth, not actual version
                blurPosition: astro.dateToJDays(new Date()), //even more smoother
                blurSpeed: 0, //even more smoother just interapolates freefly.speed
                blurCalced: 0, //even more smoother calculates delta from blurPositon
                starPosition: astro.dateToJDays(new Date()), //even much more smoother
                starSpeed: 0, //even much more smoother
                starSpeedCalced: 0, //even much more smoother 
            },
            uniflow: {
                interval: null, //used for setinterval binding, no need to cansel it, actually
                blurPos: 0,
                blurSpeed: 0,
                blurMoState: 0, //0to1 smooth true-false of _inputState.mouseOnDetails
                blurMoSwitchState: 0 //0to1 smooth true-false of _inputState.mouseOnDetails
            },
            render: {
                interval: null, // not an id actually, but true if renderloop runned thru requestRenderFrame
                startTime: null,
                fps: 0,
                fpsMean: 0,
            }
        },
        _interface: {
            lastShown: { //used for cute refreshing when changes only
                jDays: null, //used for direction detection
                gregDate: null,
                moonStage: null,
                zodiac: null,
                esotericDay: null,
            },
            toolTipShow: true
        },

        _inputState: {
            lastMoveTimeStamp: null, //Date.now()
            lastActionTimeStamp: null, //Date.now() used for smart sleep
            lastMovePosition: null, //just x as number
            pressed: false,
            kineticDrift: false, // if true enables inertion (only when pressed=true too), false when gotoDate for accuracy 
            pressedPosition: null, //just x as number
            pressedDegrees: null, //just x as number
            mouseOnDetails: false, //used to set speed augmentation in uniflow
            firstTouch: false,
        },

        _preloadStates: {
            lofi: false,
            midfi: false,
            hifi: false,
            milki: false
        },

        _viewport: {
            moonCenter: { x: 140, y: 70 },
            moonSize: 190,
            width: 660,
            height: 130,
            top: -70 + 15, //diff from moon position
            bottom: 130 - 70 + 15,
            left: -140,
            right: 660 - 140,
            canvas: null, //init in _preloadHifi
            yCanvas: null, //init in _preloadHifi
            zCanvas: null, //init in _preloadHifi
            moonCanvasAtlas: null,
            moonCanvas: null, //init in _preloadHifi
            moonLightCanvas: null, //init in _preloadHifi
            moonImage: null,
            moonAlphaImage: null
        },

        _milky: {
            background: [], //not used now
            real: []
        },

        _features: {
            fastInit: null,
            canvas: null,
            tallAtlas: null,
            shadows: null,
            domAnimation: null,
            domAnimationWeakOnly: true //only big text animation (domAnimation must be true too, to be effected)

        },

        // helper functions
        schmittDetails: function (jDays) {

            var result = {
                distance: null,
                ecliptic: {}, // {la,lo}
                zodiac: null
            };
            // calculate moon's age in days
            var mPhase = this.jDaysToMoonPhase(jDays);

            var mPhaseRad = mPhase * 2 * Math.PI; // Convert phase to radians

            // calculate moon's distance
            var distPhase = 2 * Math.PI * astro.normalize((jDays - 2451562.2) / 27.55454988);
            var dist = 60.4 - 3.3 * Math.cos(distPhase) - 0.6 * Math.cos(2 * mPhaseRad - distPhase) - 0.5 * Math.cos(2 * mPhaseRad);
            result.distance = dist; //->

            // calculate moon's ecliptic latitude
            var nPhaseRad = 2 * Math.PI * astro.normalize((jDays - 2451565.2) / 27.212220817);
            var eclipticLa = 5.1 * Math.sin(nPhaseRad);
            result.ecliptic.la = eclipticLa; //->

            // calculate moon's ecliptic longitude
            var rPhase = astro.normalize((jDays - 2451555.8) / 27.321582241);
            var eclipticLo = 360 * rPhase + 6.3 * Math.sin(distPhase) + 1.3 * Math.sin(2 * mPhaseRad - distPhase) + 0.7 * Math.sin(2 * mPhaseRad);
            eclipticLo = (eclipticLo + 360) % 360;
            result.ecliptic.lo = eclipticLo; //->

            // hmm seems to be wrong
            // upd: seems to be perfectly right, as it is in 2000 epoch,
            // but sorry, mr. Schmitt, scientific astronomy sucks,
            // Ancient Greece astrology rules.
            //
            //result.zodiac = (eclipticLo < 33.18) ? 11
            //    : (eclipticLo < 51.16) ? 0
            //    : (eclipticLo < 93.44) ? 1
            //    : (eclipticLo < 119.48) ? 2
            //    : (eclipticLo < 135.30) ? 3
            //    : (eclipticLo < 173.34) ? 4
            //    : (eclipticLo < 224.17) ? 5
            //    : (eclipticLo < 242.57) ? 6
            //    : (eclipticLo < 271.26) ? 7
            //    : (eclipticLo < 302.49) ? 8
            //    : (eclipticLo < 311.72) ? 9
            //    : (eclipticLo < 348.58) ? 10
            //    : 11;

            result.zodiac = (Math.floor(eclipticLo * 12 / 360) % 12);
            return result;
        },
        jDaysToEsotericDay: function (jDays) {
            //dumb placeholder, not a real algorithm
            var result = Math.round(this.jDaysToMoonPhase(jDays) * 29) + 1;
            return result;

        },
        moonPhaseToMoonStageId: function (phase) {
            for (var i = 0; i < this.options.moonStages.length; i++) {
                var stageEnd = this.options.moonStages[i];
                if (phase < stageEnd) {
                    return i;
                }
            }
            return 0; //else, means more then last stage end - >it is first one
        },
        jDaysToMoonPhase: function (jDays) {
            if ((jDays === undefined) || (jDays === null)) { jDays = this._engine.position; } //default
            var unnormalizedPhase = (jDays + astro.jNewShift) / astro.jMoonDayLongs;
            return astro.normalize(unnormalizedPhase);
        },
        moonPhaseToChops: function (phase, chopsCount, soft) {
            var floatChop =
                (
                ((phase * chopsCount) % chopsCount)
                + chopsCount)
                % chopsCount;
            var atlasChopLo,
                atlasChopHi,
                opacityHi,
                atlasChop;

            atlasChopLo = Math.floor(floatChop);
            atlasChopHi = Math.ceil(floatChop) % chopsCount;
            opacityHi = floatChop - atlasChopLo;
            atlasChop = Math.round(floatChop) % chopsCount;

            return { atlasChop: atlasChop, atlasChopLo: atlasChopLo, atlasChopHi: atlasChopHi, opacityHi: opacityHi, floatChop: floatChop };

        },
        jDaysToMoonAtlasFrames: function (jDays, mode) {
            if ((jDays === undefined) || (jDays === null)) { jDays = this._engine.position; } //default
            if ((mode === undefined) || (mode === null)) { mode = this.options._mode; } //default
            var phase = this.jDaysToMoonPhase(jDays);

            switch (mode) {
                case "lofi":

                    var framesCount = this.options.sprites.ballFramesCountLofi;
                    var chops = this.moonPhaseToChops(phase, framesCount, false);
                    return {
                        atlasFrame: chops.atlasChop,
                        atlasPx: chops.atlasChop * this.options.sprites.ballHeight,

                    };
                    break; //yepyep

                case "midfi":
                    var framesCount = this.options.sprites.ballFramesCountMidHifi;

                    var chops = this.moonPhaseToChops(phase, framesCount, true); //rich mode - soft=true

                    return {
                        atlasFrameLo: chops.atlasChopLo,
                        atlasFrameHi: chops.atlasChopHi,
                        atlasPxLo: chops.atlasChopLo * this.options.sprites.ballHeight,
                        atlasPxHi: chops.atlasChopHi * this.options.sprites.ballHeight,
                        opacityHi: chops.opacityHi,
                    };
                case "hifi":
                    var framesCount = this.options.sprites.ballFramesCountMidHifi;
                    var stride = (this._features.tallAtlas) ? 1 : 5;
                    var strideHeight = framesCount / stride;
                    var chops = this.moonPhaseToChops(phase, framesCount, true);
                    return {
                        atlasFrame: chops.atlasChop % strideHeight,
                        atlasPx: (chops.atlasChop % strideHeight) * this.options.sprites.ballHeight,
                        stridePx: Math.floor(chops.atlasChop / strideHeight) * this.options.sprites.ballHeight,

                        atlasPxLo: (chops.atlasChopLo % strideHeight) * this.options.sprites.ballHeight,
                        atlasPxHi: (chops.atlasChopHi % strideHeight) * this.options.sprites.ballHeight,

                        //strideLo: Math.floor(chops.atlasChopLo / (chops.atlasChopLo / stride)),
                        //strideHi: Math.floor(chops.atlasChopHi / (chops.atlasChopHi / stride)),

                        stridePxLo: Math.floor(chops.atlasChopLo / strideHeight) * this.options.sprites.ballWidth,
                        stridePxHi: Math.floor(chops.atlasChopHi / strideHeight) * this.options.sprites.ballWidth,
                        opacityHi: chops.opacityHi,

                    };
                    break; //yepyep
            }


        },
        gregEsoToBooleanshitDetails: function (gregDate, esotericDay) {
            var detailedTextLink = this.options.links.root + this.options.links.monthDay + ((gregDate.getDate() > 9) ? "" : "0") + gregDate.getDate();
            return (
                ((this.options.locale.booleanshit.weekDayDetails !== null) ? (this.options.locale.booleanshit.weekDayDetails[(gregDate.getDay() + 6) % 7] + " ") : (""))            
              + ((this.options.locale.booleanshit.moonDayDetails !== null) ? (this.options.locale.booleanshit.moonDayDetails[esotericDay - 1] + " ") : (""))
              + ((this.options.locale.booleanshit.monthDayDetails !== null) ? (this.options.locale.booleanshit.monthDayDetails[gregDate.getDate() - 1]) : (""))
              + " (<a target='"+this.options.links.target+"' href='"+detailedTextLink+"'>подробнее...</a>)"
            );
        },
        gregEsoToBooleanshitPersent: function (gregDate, esotericDay) {
            return (
                this.booleanshit.weekDay[(gregDate.getDay() + 6) % 7]
              + this.booleanshit.moonDay[esotericDay - 1]
              + this.booleanshit.monthDay[gregDate.getDate() - 1]
            );
        },



        // the constructor
        _detectFeatures: function () {

            this._features.fastInit = (navigator.userAgent.indexOf("Chrome") !== -1);
            try {
                this._features.canvas = (this._viewport.canvas.getContext) ? true : false;
            }
            catch (e) {
                this._features.canvas = false;
            }

            this._features.tallAtlas = (navigator.userAgent.indexOf("WebKit") !== -1) || (navigator.userAgent.indexOf("Firefox") !== -1);
            this._features.shadows = (navigator.userAgent.indexOf("WebKit") !== -1);
            this._features.domAnimationWeakOnly = (navigator.userAgent.indexOf("Chrome") === -1);
            //console.log("Features detected: ");
            //$.each(this._features, function (key, value) {
            //    console.log(key + ": " + value);
            //});

        },
        
        _updateOptionsFromHash: function () {
            var hash = window.location.hash.substring(1);
            var dateStart = hash.toLowerCase().indexOf("date");
            if (dateStart !== -1) {
                var dateJdays = parseFloat((hash.substring(dateStart + 4)));
                if (dateJdays > 0) {
                    this._engine.position =
                        this._engine.freefly.position =
                        this._engine.freefly.blurPosition =
                        this._engine.freefly.starPosition = dateJdays;
                    this.options.politeDelay = 0;
                }
            }
        },
        _create: function () {           
            var that = this;
            this._updateOptionsFromHash();
            this.element
                // add a class for theming
                .addClass("moon-informer");
            // prevent double click to select text
            //.disableSelection();            

            this._viewport.canvas = $(".moon-canvas", this.element)[0];
            this.canvasHifi = $(this._viewport.canvas);
            this._detectFeatures();

            this.ballLofi = $(".moon-ball-lofi", this.element);
            this.ballMidfi = $(".moon-ball-midfi", this.element);

            $(document).on("mousemove", function (e) { that._onMouseMove(e); });
            this.ballLofi.on("mousedown", function (e) { that._onMouseDown(e); });
            this.element.on("mouseup", function (e) { that._onMouseUp(e); });
            $(document).on("mouseup", function (e) { that._onMouseUp(e); });

            this.span1 = $(".mspan1", this.element);
            this.span2 = $(".mspan2", this.element);
            this.span3 = $(".mspan3", this.element);

            this.swipeDate = $(".moon-swipe-date", this.element);

            this.swipeDate.on("mousedown", function (e) { e.preventDefault(); that._datepickerShow(); that._firstTouch(); return false; });
            this.swipeDate.click(function (e) { e.preventDefault(); });

            this.swipeWeekDay = $(".moon-swipe-weekDay", this.element);
            this.swipeMoonStage = $(".moon-swipe-moonStage", this.element);
            this.swipeEsotericDay = $(".moon-swipe-esotericDay", this.element);
            this.swipeZodiac = $(".moon-swipe-zodiac", this.element);
            this.swipeZodiacIcon = $(".moon-swipe-zodiac-icon", this.element);
            this.swipeDetailedText = $(".moon-swipe-detailedText", this.element);
            this.swipePercent = $(".moon-swipe-percent", this.element);
            this.swipeStar1 = $(".moon-swipe-star1", this.element);
            this.swipeStar2 = $(".moon-swipe-star2", this.element);
            this.swipeStar3 = $(".moon-swipe-star3", this.element);
            this.swipeStar4 = $(".moon-swipe-star4", this.element);
            this.swipeStar5 = $(".moon-swipe-star5", this.element);
            this.swipeStars = $(".stars", this.element);

            this.fakeTranspTop = $(".moon-fake-transp-top", this.element);
            this.fakeTranspBottom = $(".moon-fake-transp-bottom", this.element);
            this.fakeTransp = $(".moon-fake-transp-top, .moon-fake-transp-bottom", this.element);

            this.comment = $(".moon-comment", this.element);

            this.tooltip = $(".moon-tooltip", this.element);

            this.textSensor = $(".moon-sensor", this.element);
            this.textSensor.on("mousemove", function (e) {
                that._onSensorMouseMove(e);
            });
            this.textSensor.on("mouseenter", function (e) {
                that._inputState.mouseOnDetails = true;
            });
            this.textSensor.on("mouseleave", function (e) {
                that._inputState.mouseOnDetails = false;
                that._engine.mouseUniPos = 0;
            });

            this.datepicker = $(".moon-datepicker-placeholder");
            this.datepicker.datepicker({
                onSelect: function (dateText) {
                    that.setPosition(astro.dateToJDays(new Date(dateText)));
                    that._wake();
                },
                changeMonth: true,
                changeYear: true

            });
            this.trangles = {
                date: {
                    left: $(".moon-trangle-left-date"),
                    right: $(".moon-trangle-right-date")
                },
                esotericDay: {
                    left: $(".moon-trangle-left-esotericDay"),
                    right: $(".moon-trangle-right-esotericDay")
                },
                moonStage: {
                    left: $(".moon-trangle-left-moonStage"),
                    right: $(".moon-trangle-right-moonStage")
                },
                zodiac: {
                    left: $(".moon-trangle-left-zodiac"),
                    right: $(".moon-trangle-right-zodiac")
                },
            };

            this.trangles.date.left.click(function (e) { e.preventDefault(); that.setDate("prev"); });
            this.trangles.date.right.click(function (e) { e.preventDefault(); that.setDate("next"); });
            this.trangles.esotericDay.left.click(function (e) { e.preventDefault(); that.setEsotericDay("prev"); });
            this.trangles.esotericDay.right.click(function (e) { e.preventDefault(); that.setEsotericDay("next"); });

            this.trangles.moonStage.left.click(function (e) { e.preventDefault(); that.setMoonStage("prev"); });
            this.trangles.moonStage.right.click(function (e) { e.preventDefault(); that.setMoonStage("next"); });

            this.trangles.zodiac.left.click(function (e) { e.preventDefault(); that.setZodiac("prev"); });
            this.trangles.zodiac.right.click(function (e) { e.preventDefault(); that.setZodiac("next"); });

            $(window).focus(function () { that._wake(); })
                     .blur(function () { that._sleep(); });

            $(this.element).on("mousedown", function () { that._firstTouch(); });

            this.options._mode = "lofi"; //add a backing field (switches after preloading, used in logic, while .mode is for seting from outer space)


            if (this.options.mode === "lofi") {
                this.ballLofi.css({ "background-image": "url(" + this.options.sprites.ballFramesUrlLofi + ")" }); //revert to one-image mode
                this.canvasHifi.css({ display: "none" });
                this._preloadLofi(function () {
                    that.options._mode = "lofi";
                    that._refresh();
                });
            }
            if (this.options.mode === "midfi") {
                this.ballLofi.css({ "background-image": "url(" + this.options.sprites.ballFramesUrlMidHifi + ")" });
                this.ballMidfi.css({ "background-image": "url(" + this.options.sprites.ballFramesUrlMidHifi + ")" });
                this.canvasHifi.css({ display: "none" });
                this._preloadMidfi(function () {
                    that.options._mode = "midfi";
                    that._refresh();
                });
            }
            if (this.options.mode === "hifi") {
                this.ballLofi.css({ "background-image": "none" });
                this.ballMidfi.css({ display: "none" });
                this.canvasHifi.css({ display: "block" });
                this._preloadHifi(function () {
                    that.options._mode = "hifi";
                    that._buildMilky();
                    that._refresh();
                });
            }

            this.element.css({ display: "block" }); //need to be visible for correct sizing in swipeLabel();

            this.swipeDate.swipeLabel();
            this.swipeWeekDay.swipeLabel();
            this.swipeMoonStage.swipeLabel();
            this.swipeEsotericDay.swipeLabel();
            this.swipeZodiac.swipeLabel();
            this.swipeZodiacIcon.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 25 });            
            this.swipePercent.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 90 });
            this.swipeStar1.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 14 });
            this.swipeStar2.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 14 });
            this.swipeStar3.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 14 });
            this.swipeStar4.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 14 });
            this.swipeStar5.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 14 });
            //this.swipeDetailedText.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 230, "charsAt100": 180, "uniPos": 0 });
            //this.swipeDetailedText.swipeLabel({ "contentWidthMode": "manual", "contentWidth": this.options.sizes.span2, "uniPos": 0 });
            //swipeDetailedText is inited in _spansSizeRefresh(); a line below
            this._spansSizeRefresh();
            this._refresh();
            
            setTimeout(function () {
                that._firstTouch();
            }, this.options.politeDelay); // wait to let browser load all stuff
        },
        
        _spansSizeRefresh: function () { // used in "run-time" (setOption fires) and in creation
                                         // it's for reposition IINNER SWIPE WIDTH 
                                         // after some divs become hidden
                                         // to continue fit template well

            
            (this.options.blocks.span1===true) ? this.span1.show() : this.span1.hide();
            (this.options.blocks.span3 === true) ? this.span3.show() : this.span3.hide();
            ((this.options.blocks.span1 === false) || (this.options.blocks.span3 === false)) ? this.fakeTransp.hide() : this.fakeTransp.show();
            var detTextWidth = this.options.sizes.span2;
            
            if (this.options.blocks.span1===false) {
                detTextWidth += this.options.sizes.span1;
                detTextWidth += this.options.sizes.margin;
            }
            if (this.options.blocks.span3 === false) {                
                detTextWidth += this.options.sizes.span3;                
                detTextWidth += this.options.sizes.margin;
            }
            var detTextSetup = { "contentWidthMode": "manual", "contentWidth": detTextWidth, "uniPos": 0 };
            if (this.options.sizes.span2CharsAt100!=null) {
                detTextSetup.charsAt100 = this.options.sizes.span2CharsAt100;
            }
            this.swipeDetailedText.swipeLabel(detTextSetup);

              
        },
            
        _onSensorMouseMove: function (e) {
            var that = this;
            var sensorTop = this.textSensor.offset().top;
            var margin = 30;
            var uniPos = (e.pageY - sensorTop - margin) / (this._viewport.height - margin * 2); //0to1
            var uniPos = Math.min(1, Math.max(0, uniPos));
            this._engine.mouseUniPos = uniPos;
            this._firstTouch();
        },
        _datepickerShow: function () {
            this.datepicker.datepicker("show");
        },
        _onMouseMove: function (e) {
            if (this._inputState.pressed === true) {
                var msecNow = Date.now();
                if ((this._inputState.lastMoveTimeStamp !== null) && (this._inputState.lastMovePosition)) {
                    var deltaTime = Math.max(msecNow - this._inputState.lastMoveTimeStamp, 10); //mouse.max to flatten peaks with anomaly shorten periods, produsing huge velocity
                    this._engine.speed = ((this._inputState.lastMovePosition - e.clientX) * astro.jMoonDayLongs / (this.options.sprites.ballWidth * 2)) * 1000 / (deltaTime);
                }
                this._inputState.lastMoveTimeStamp = msecNow;
                this._inputState.lastMovePosition = e.clientX;

                this._inputState.lastActionTimeStamp = msecNow;

                this._engine.position = ((this._inputState.pressedPosition - e.clientX) * astro.jMoonDayLongs / (this.options.sprites.ballWidth * 2)) + this._inputState.pressedJDays;
            }
        },

        _onMouseDown: function (e) {
            var that = this;
            this.element.disableSelection();
            this._engine.speed = 0; //stopping on moveless click
            this._engine.freefly.speed = 0; //stopping on moveless click
            //inputState.lastMoveTimeStamp = Date.now();
            //inputState.lastMovePosition = e.clientX;
            this._inputState.pressedPosition = e.clientX;
            this._inputState.pressedJDays = this._engine.position;
            this._inputState.pressed = true;

            this._firstTouch();
            if (this._interface.toolTipShow) {
                this._interface.toolTipShow = false;
                this.tooltip.css({ opacity: 0 });
            }
            this.tooltip.css({ opacity: 0 });
        },


        _freefly: function () {
            if (this._engine.sleep) return;

            if (this._inputState.pressed === false) {

                if ((Math.abs(this._engine.freefly.starSpeedCalced) < 0.01) &&
                   (Math.abs(this._engine.freefly.blurSpeedCalced) < 0.01) &&
                   (Math.abs(this._engine.freefly.speed) < 0.01) &&
                   (Math.abs(this._engine.speed) < 0.01)) {
                    //clearInterval(this._engine.freefly.interval);
                    //this._engine.freefly.interval = null; //end interval loop
                    this._engine.freefly.starSpeedCalced = 0;
                    this._engine.freefly.blurSpeedCalced = 0;
                    this._engine.freefly.speed = 0;
                    this._engine.speed = 0;
                    if (this._engine.freefly.position === this._engine.position) {
                        //return;
                    } else {
                        this._engine.freefly.position = this._engine.position;
                    }

                    if ((this._inputState.lastActionTimeStamp + 3 * 1000) < Date.now()) { //3 secs
                        this._sleep();
                    }
                }

                this._engine.freefly.speed *= 0.99; //slowing here

                if (Math.abs(this._engine.freefly.speed) < 10) { //slowing harder when slower 260 (60)
                    var speedSign = this._engine.freefly.speed ? this._engine.freefly.speed < 0 ? -1 : 1 : 0;
                    var speedMinus = 0.1;
                    var extraFriction = (Math.abs(this._engine.freefly.speed) < speedMinus) ? this._engine.freefly.speed : (speedSign * speedMinus);
                    this._engine.freefly.speed -= extraFriction;
                }
                if (Math.abs(this._engine.freefly.speed) < 0.2) { this._engine.freefly.speed = 0; } //stop when slower +-2

                if (this._inputState.kineticDrift) {
                    this._engine.position = this._engine.position + (this._engine.freefly.speed / 60); // /
                }
            } else { //pressed==true                
                this._engine.freefly.speed = this._engine.freefly.speed * 0.75 + this._engine.speed * 0.25;
            }
            this._engine.freefly.position = this._engine.freefly.position * 0.75 + this._engine.position * 0.25;

            var oldBlurPosition = this._engine.freefly.blurPosition;
            this._engine.freefly.blurPosition = this._engine.freefly.blurPosition * 0.75 + this._engine.freefly.position * 0.25;
            this._engine.freefly.blurSpeed = this._engine.freefly.blurSpeed * 0.75 + this._engine.freefly.speed * 0.25;
            this._engine.freefly.blurSpeedCalced = (this._engine.freefly.blurPosition - oldBlurPosition) * 60;


            var oldStarPosition = this._engine.freefly.starPosition;
            this._engine.freefly.starPosition = this._engine.freefly.starPosition * 0.80 + this._engine.freefly.blurPosition * 0.2;
            this._engine.freefly.starSpeedCalced = (this._engine.freefly.starPosition - oldStarPosition) * 60;



            //this._refresh(); go out to graphic engine
            this._engine.speed *= 0.75; //slowing for times where no mousemoves hit

        },


        _uniflow: function () {

            if (this._engine.sleep) return;





            this._engine.uniflow.blurMoSwitchState = (this._inputState.mouseOnDetails === true) ? (this._engine.uniflow.blurMoSwitchState * 0.95 + 0.05) : 0;
            this._engine.uniflow.blurMoState = this._engine.uniflow.blurMoState * 0.98 + ((this._inputState.mouseOnDetails === true) ? 0.02 : 0);


            var moStatePow = Math.pow(this._engine.uniflow.blurMoSwitchState, 100);


            var oldBlurPosition = this._engine.uniflow.blurPos;
            this._engine.uniflow.blurPos =
              //  this._engine.uniflow.blurPos * (0.9 - this._engine.uniflow.blurMoSwitchState * 0.2)
              //+ this._engine.mouseUniPos * (0.1 + this._engine.uniflow.blurMoSwitchState * 0.2);
                this._engine.uniflow.blurPos * (0.95 - moStatePow * 0.2) // from .75 to .95
              + this._engine.mouseUniPos * (0.05 + moStatePow * 0.2); //from .25 to .05

            this._engine.uniflow.blurSpeed = (this._engine.uniflow.blurPos - oldBlurPosition) * 60;

            if (Math.abs(this._engine.uniflow.blurSpeed) < 0.001) {
                this._engine.uniflow.blurSpeed = 0;
                return;
            }
            this._refreshDetailedUniPos();


        },

        _onMouseUp: function (e) {
            this.element.enableSelection();
            if (this._inputState.pressed) {
                this._inputState.kineticDrift = true;
                //was pressed, now drift
            }
            this._inputState.pressed = false;
        },

        _wake: function () { //Must be executed in every user interaction.
            var that = this;
            this._engine.sleep = false;
            this._inputState.lastActionTimeStamp = Date.now();
            if (this._engine.freefly.interval === null) {
                this._engine.freefly.interval = setInterval(function () { that._freefly(); }, 1000 / 60); //check for once                
            }
            if (this._engine.uniflow.interval === null) {
                this._engine.uniflow.interval = setInterval(function () { that._uniflow(); }, 1000 / 60); //check for once                
            }
            if (this._engine.render.interval === null) {
                this._engine.render.interval = true;

                var renderLoop = function () {
                    if (!that._engine.sleep) that._refresh();
                    window.requestAnimationFrame(function () { renderLoop(); });
                };
                renderLoop();
            }
        },

        _sleep: function () {;
            this._engine.sleep = true;
            this._engine.render.startTime = null;
        },

        _firstTouch: function () { //checks almost every *CLICKY* intraction, but fires only once. Used start init to hifi.
            if (this._inputState.firstTouch === false) {
                var activeMode = ((this._features.canvas != true) && (this.options.activeMode === "hifi")) ? "midfi" : this.options.activeMode;                
                this._setOption("mode", activeMode);
                this._inputState.firstTouch = true;
            }
            this._wake();
        },


        // called when created,  when changing options and later in timer loop
        _refresh: function () {            
            this._easter();
            var af = this.jDaysToMoonAtlasFrames(this._engine.freefly.position);
            switch (this.options._mode) {
                case "lofi":
                    this.ballLofi.css({
                        "background-position": "left " + -af.atlasPx + "px"
                    });
                    break;
                case "midfi":
                    this.ballLofi.css({
                        "background-position": "left " + -af.atlasPxLo + "px",
                    });
                    this.ballMidfi.css({
                        "background-position": "left " + -af.atlasPxHi + "px",
                        "opacity": af.opacityHi
                    });
                    break;
                case "hifi":
                    this._refreshCanvas(af);
                    break;
            }


            var detailedTextOpacity = Math.pow(1 - Math.min(Math.max(Math.abs(this._engine.freefly.blurSpeed) / 300, 0), 1), 20); //0to300 -> 0 to 1, then pow2                        
            this.swipeDetailedText.css({ opacity: detailedTextOpacity * 0.6 + 0.2 });
            this.swipePercent.css({ opacity: detailedTextOpacity * 0.6 + 0.4 });
            this.swipeStars.css({ opacity: detailedTextOpacity * 0.6 + 0.4 });

            if (this._features.shadows) {
                //somtime was blurSpeed no Calced, maybe need to revert
                this.swipeDetailedText.swipeLabel({ constantBlur: this._engine.freefly.blurSpeedCalced });
                this.swipePercent.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed / 4 });
                this.swipeWeekDay.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed });
                this.swipeEsotericDay.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed });
            }

            var gregDate = astro.jDaysToDate(this._engine.position);
            var moonStageId = this.moonPhaseToMoonStageId(
                    this.jDaysToMoonPhase(
                    this._engine.position));                        
            
            

            var moonStage = this.options.locale.moonStages[moonStageId];
            var esotericDay = this.jDaysToEsotericDay(this._engine.position);

            var esotericDayAtMidnight = this.jDaysToEsotericDay(Math.floor(this._engine.position - astro.timeOffset / (60 * 24)) + 0.5);

            var zodiac = this.schmittDetails(this._engine.position).zodiac;


            var direction = (this._interface.lastShown.jDays === null) ? "left" : (this._engine.position > this._interface.lastShown.jDays) ? "left" : "right";
            var duration = (this._inputState.pressed) || (!this._inputState.kineticDrift) ?
                  Math.min(1600 / Math.abs(this._engine.freefly.speed), 200)
                : Math.min(1600 / Math.abs(this._engine.freefly.speed), 1000);
            if (this._features.domAnimation !== true) { duration = 0; }
            var detailedDuration = duration;
            if (this._features.domAnimationWeakOnly === true) { duration = 0; }

            //
            // smart update for date-sense info
            //
            if (
                    (this._interface.lastShown.gregDate === null) ||                    
                    (this._interface.lastShown.gregDate.getTime() !== gregDate.getTime())
                ) {

                if (this.options.blocks.span1) {
                    this.datepicker.datepicker("setDate", gregDate);                    
                    this.swipeDate.swipeLabel({
                        content: $("<a class='date' href='#'>" + gregDate.getDate() + " " + this.options.locale.monthNames[gregDate.getMonth()] + " " + gregDate.getFullYear() + "</a>"),
                        duration: duration,
                        direction: direction
                    });
                    
                    this.swipeWeekDay.swipeLabel({
                        content: $("<span>").text(this.options.locale.weekDays[gregDate.getDay()]),
                        duration: duration,
                        direction: direction
                    });
                }
                if (this.options.blocks.span3) {
                    var booleanshitPercent = this.gregEsoToBooleanshitPersent(gregDate, esotericDayAtMidnight);

                    this.swipePercent.swipeLabel({
                        content: $("<span>").text(booleanshitPercent + "%"),
                        duration: duration * 3.5,
                        direction: direction
                    });

                    var starDuration = (this._features.domAnimation === true) ? Math.min(1000, Math.max(0, duration * 3.5)) : 0;
                    this.swipeStar1.swipeLabel({
                        content: (booleanshitPercent >= 25) ? $("<div class='star'></div>") : $("<div class='star op50'></div>"),
                        duration: starDuration,
                        direction: direction,
                    });
                    this.swipeStar2.swipeLabel({
                        content: (booleanshitPercent > 40) ? $("<div class='star'></div>") : $("<div class='star op50'></div>"),
                        duration: starDuration,
                        direction: direction
                    });
                    this.swipeStar3.swipeLabel({
                        content: (booleanshitPercent >= 50) ? $("<div class='star'></div>") : $("<div class='star op50'></div>"),
                        duration: starDuration,
                        direction: direction
                    });
                    this.swipeStar4.swipeLabel({
                        content: (booleanshitPercent >= 75) ? $("<div class='star'></div>") : $("<div class='star op50'></div>"),
                        duration: starDuration,
                        direction: direction
                    });
                    this.swipeStar5.swipeLabel({
                        content: (booleanshitPercent > 90) ? $("<div class='star'></div>") : $("<div class='star op50'></div>"),
                        duration: starDuration,
                        direction: direction
                    });

                }                
                this.swipeDetailedText.swipeLabel({
                    content: $("<span>").html(this.gregEsoToBooleanshitDetails(gregDate, esotericDayAtMidnight)),
                    duration: detailedDuration * 2.5,
                    direction: direction
                });

                this._interface.lastShown.gregDate = gregDate; //lastShown
            }
            if (this.options.blocks.span1) {
                var sloDuration = (this._features.domAnimation === true) && ((this._features.domAnimationWeakOnly !== true)) ? Math.min(duration * 2, 1000) : 0;
                //
                // smart update for zodiac-sense info
                //            
                if ((this._interface.lastShown.zodiac === null) ||
                    (this._interface.lastShown.zodiac !== zodiac)) {
                    var zodiacLink = this.options.links.root + this.options.links.zodiac[zodiac];

                    this.swipeZodiac.swipeLabel({
                        content: $("<a target='" + this.options.links.target + "' href='" + zodiacLink + "'>" + this.options.locale.zodiacNames[zodiac] + "</a>"),
                        duration: sloDuration,
                        direction: direction
                    });

                    this.swipeZodiacIcon.swipeLabel({
                        content: $("<div class='zodiac'>").css({ "background-position": "0 " + (12 - zodiac) * 25 + "px" }),
                        duration: sloDuration,
                        direction: direction
                    });

                    this._interface.lastShown.zodiac = zodiac; //lastShown
                }


                //
                // smart update for esotericDay-sense info
                //
                if ((this._interface.lastShown.esotericDay === null) ||
                    (this._interface.lastShown.esotericDay !== esotericDay)) {
                    var esotericLink = this.options.links.root + this.options.links.esotericDay + ((esotericDay > 9) ? "" : "0") + esotericDay;
                    this.swipeEsotericDay.swipeLabel({                        
                        content: $("<a target='"+this.options.links.target+"' href='" + esotericLink + "'>" + esotericDay + "</a>"),                        
                        duration: duration,
                        direction: direction
                    });


                    this._interface.lastShown.esotericDay = esotericDay; //lastShown
                }

                //
                // smart update for moonStage-sense info
                //
                if ((this._interface.lastShown.moonStage === null) ||
                    (this._interface.lastShown.moonStage !== moonStage)) {
                    var moonStageLink = this.options.links.root+this.options.links.moonStages[moonStageId];
                    this.swipeMoonStage.swipeLabel({
                        content: $("<a target='" + this.options.links.target + "' class='stage' href='" + moonStageLink + "'>" + moonStage + "</a>"),
                        duration: sloDuration,
                        direction: direction
                    });

                    this._interface.lastShown.moonStage = moonStage; //lastShown
                }
            }

            this._interface.lastShown.jDays = this._engine.position; //lastShown

            //animation degradation on low fps
            if (this._engine.render.startTime !== null) {
                var deltaTime = Date.now() - this._engine.render.startTime;

                this._engine.render.fps = Math.min(120, 1000 / deltaTime);
                this._engine.render.fpsMean = this._engine.render.fpsMean * 0.9 + this._engine.render.fps * 0.1;

                this._features.domAnimation = (this._engine.render.fpsMean > 20);

            }

            //uncomment to debug fps
            //this.comment.text(this._engine.render.fpsMean);
            this._engine.render.startTime = Date.now();
            this._trigger("change");
        },

        _refreshDetailedUniPos: function () {
            this.swipeDetailedText.swipeLabel({ uniPos: this._engine.uniflow.blurPos });
            this.fakeTransp.css({ opacity: 1 - this._engine.uniflow.blurMoState });
            // funny visual effect for <b>, commented now
            //$("b", this.swipeDetailedText).css({ opacity: 1 - Math.abs(this._engine.uniflow.blurSpeed / 5) });
        },

        _buildMilky: function () {
            if (this._preloadStates.milki) return;
            var that = this;
            // not used now
            //this._milky.background = [];
            //for (var i = 0; i < 2000; i++) {
            //    var newStar = {};
            //    newStar.la = Math.random() * 30 - 15;
            //    newStar.lo = Math.random() * 360;
            //    newStar.v = (Math.pow(Math.random(), 1) * 7) + 0;
            //    newStar.bv = Math.random() * 2 - 1;
            //    if (newStar.bv > 0) newStar.bv *= 2.5;
            //    newStar.rgb = astro.bvToRgb(newStar.bv);
            //    newStar.opacity = astro.vToOpacity(newStar.v, 10);
            //    this._milky.background.push(newStar);
            //};


            $.getJSON('moon-informer/starsd.json', function (data) {
                that._preloadStates.milki = true;
                that._milky.real = [];

                var deltaLo = 0;
                // getting well-sorted delta-coded json and decode
                $.each(data, function (key, value) {
                    var dataStar = data[key];
                    var newStar = {};
                    newStar.lo = dataStar[0] + deltaLo;
                    deltaLo = newStar.lo;
                    newStar.la = dataStar[1];
                    newStar.lo = +deltaLo;
                    newStar.rgb = astro.bvToRgb(dataStar[3]);
                    newStar.opacity = astro.vToOpacity(dataStar[2], 10);
                    that._milky.real.push(newStar);
                });
                if (that._engine.render.interval !== true) //workaround to refresh after load without using render cycle
                    that._refresh();
                that._refresh();
                that._refresh();
                that._refresh();
            });
        },

        _refreshCanvas: function (atlasFrames) {

            var schmitt = this.schmittDetails(this._engine.freefly.starPosition);
            var schmittBefore = this.schmittDetails(this._engine.freefly.starPosition - this._engine.freefly.starSpeedCalced / 60); //used for bluring moved stars
            var schmittAfter = this.schmittDetails(this._engine.freefly.starPosition + this._engine.freefly.starSpeedCalced / 60); //used for bluring moved stars


            var canvas = this._viewport.canvas;

            var zCanvas = this._viewport.zCanvas;


            var ctx = canvas.getContext('2d');
            var ctxz = zCanvas.getContext('2d');


            ctxz.globalAlpha = 0.75;

            ctxz.clearRect(0, 0, canvas.width, canvas.height);
            ctxz.drawImage(canvas, 0, 0); //feedback here
            ctxz.globalAlpha = 1;


            var moonSizeDeg = 1; //0.5 for real            
            var degMul = this._viewport.moonSize / moonSizeDeg;
            this._drawStars(ctxz, this._milky.real, schmitt, schmittBefore, schmittAfter, {
                degMul: degMul,
                sectorDegree: 360,
                opacity: 1,
                lightspeed: true
            });

            moonSizeDeg = 20; //0.5 for real            
            degMul = this._viewport.moonSize / moonSizeDeg;
            this._drawStars(ctxz, this._milky.real, schmitt, schmittBefore, schmittAfter, {
                degMul: degMul,
                sectorDegree: 360,
                opacity: 0.15,
                noLa: true,
                laShift: 0.5 - this._engine.uniflow.blurPos * 1,
            });


            //ctxz.drawImage(zCanvas, 0, 0);            
            this._drawMoon(ctxz, atlasFrames);

            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(zCanvas, 0, 0);

            //console.log(
            //    this._engine.position.toFixed(2)+"   "+
            //    this._engine.speed.toFixed(2)+" | "+
            //    this._engine.freefly.position.toFixed(2)+"   "+
            //    this._engine.freefly.speed.toFixed(2)+" | "+
            //    this._engine.freefly.blurPosition.toFixed(2)+"   "+
            //    this._engine.freefly.blurSpeed.toFixed(2)+" | "+
            //    this._engine.freefly.starPosition.toFixed(2)+"   "+
            //    this._engine.freefly.starSpeedCalced.toFixed(2)
            //    );

        },


        _drawStars: function (ctx, stars, schmitt, schmittBefore, schmittAfter, options) {
            options = $.extend({
                sectorDegree: 360, //default
                degMul: this._viewport.moonSize / 0.5, //default .5 is close to real moon angular size from earth
                opacity: 1,
                stroke: 1,
                lightSpeed: true,
                noLa: false,
                laShift: 0
            }, options);

            var viewportDeg = {}; //cloning and making degrees-verison
            $.each(this._viewport, function (index, value) {
                viewportDeg[index] = value / options.degMul;
            });


            ctx.lineWidth = options.stroke;
            //ctx.beginPath(); //alt
            //var alpha = 1 / Math.min(20, Math.max(1, Math.abs(this._engine.freefly.starSpeedCalced))) * options.opacity; //alt
            //ctx.strokeStyle = "rgba(255,255,255,"+alpha+")"; //alt

            var viewSectorLeft = Math.floor((schmitt.ecliptic.lo + viewportDeg.left) / options.sectorDegree);
            var viewSectorRight = Math.floor((schmitt.ecliptic.lo + viewportDeg.right) / options.sectorDegree);
            for (var vs = viewSectorLeft; vs <= viewSectorRight; vs++) {

                for (var i = 0; i < stars.length; i++) {
                    var star = stars[i];

                    var laTravel = options.noLa ? 0 : Math.min(1, Math.abs(15 / this._engine.freefly.starSpeedCalced));

                    var moonDiff = {
                        lo: (star.lo + (vs * options.sectorDegree) - schmitt.ecliptic.lo),
                        la: (star.la - schmitt.ecliptic.la * laTravel)
                    };
                    moonDiff.x = moonDiff.lo * options.degMul;
                    moonDiff.y = (moonDiff.la + options.laShift) * options.degMul;

                    if ((moonDiff.x > this._viewport.left)
                        && (moonDiff.x < this._viewport.right)
                        && (moonDiff.y > this._viewport.top)
                        && (moonDiff.y < this._viewport.bottom)) {

                        var blurWidth = this._engine.freefly.starSpeedCalced / 2 + 0.5;
                        var alpha = 27
                            * star.opacity
                            / Math.min(options.lightspeed ? 10 : 20, Math.max(1, Math.abs(this._engine.freefly.starSpeedCalced)))
                            * options.opacity; //not this.options, but from function params


                        ctx.beginPath();
                        var eclipticTileShiftBefore = ((schmittBefore.ecliptic.lo <= 90) && (schmitt.ecliptic.lo > 270)) ? +360
                                                   : ((schmittBefore.ecliptic.lo > 270) && (schmitt.ecliptic.lo <= 90)) ? -360 : 0;
                        var blurDiffBefore = {
                            la: (schmittBefore.ecliptic.la - schmitt.ecliptic.la) * laTravel,
                            lo: schmittBefore.ecliptic.lo - schmitt.ecliptic.lo + eclipticTileShiftBefore,
                        };
                        var eclipticTileShiftAfter = ((schmittAfter.ecliptic.lo <= 90) && (schmitt.ecliptic.lo > 270)) ? +360
                                                  : ((schmittAfter.ecliptic.lo > 270) && (schmitt.ecliptic.lo <= 90)) ? -360 : 0;
                        var blurDiffAfter = {
                            la: (schmittAfter.ecliptic.la - schmitt.ecliptic.la) * laTravel,
                            lo: schmittAfter.ecliptic.lo - schmitt.ecliptic.lo + eclipticTileShiftAfter,
                        };

                        ctx.strokeStyle = "rgba(" + star.rgb.r + "," + star.rgb.g + "," + star.rgb.b + "," + alpha + ")";
                        ctx.moveTo(moonDiff.x + this._viewport.moonCenter.x - blurWidth, moonDiff.y + this._viewport.moonCenter.y); //olschool
                        ctx.lineTo(moonDiff.x + this._viewport.moonCenter.x + blurWidth, moonDiff.y + this._viewport.moonCenter.y);
                        ctx.moveTo(moonDiff.x + this._viewport.moonCenter.x + blurDiffBefore.lo * options.degMul, //plus newchool
                                   moonDiff.y + this._viewport.moonCenter.y + blurDiffBefore.la * options.degMul);
                        ctx.lineTo(moonDiff.x + this._viewport.moonCenter.x,
                                   moonDiff.y + this._viewport.moonCenter.y);
                        ctx.lineTo(moonDiff.x + this._viewport.moonCenter.x + blurDiffAfter.lo * options.degMul,
                                   moonDiff.y + this._viewport.moonCenter.y + blurDiffAfter.la * options.degMul);

                        ctx.stroke();

                    }
                }
            }
            //ctx.stroke(); //alt


        },

        _buildmoonCanvasAtlas: function (callback) {

            function _draw2dAtlas(ctx, domObject, ballWidth, ballHeight, framesCount, stride, left) {
                if (framesCount % stride !== 0) { $.error("stride must split frames in whole columns (framesCount%stride must = 0)"); return; }
                var strideHeight = (framesCount / stride) * ballHeight;
                for (var s = 0; s < stride; s++) {
                    ctx.drawImage(domObject,
                        0, s * strideHeight,
                        ballWidth, strideHeight,
                        s * ballWidth + left, 0,
                        ballWidth, strideHeight);
                }
            }

            var that = this;
            function _composeAlphaAsync(ctx, dom, callback) {
                function batchPatch(imageData, dom, progress, callback) {
                    if (progress < dom.height) {
                        if (((that._engine.freefly.starSpeedCalced === 0) && (that._engine.uniflow.blurSpeed === 0)) || (that._features.fastInit)) {
                            var top = progress,
                                step = (that._features.fastInit) ? 1450 : 145;

                            var halfWidth = dom.width / 2;
                            var x, y;
                            for (x = 0; x < halfWidth; x++) {
                                for (y = top; y < top + step; y++) {
                                    //var y = top;
                                    imageData.data[(x + (y * dom.width)) * 4 + 3] = imageData.data[(x + halfWidth + (y * dom.width)) * 4];
                                }
                            }

                            progress = top + step;
                        }
                        setTimeout(function () { batchPatch(imageData, dom, progress, callback); }, 1);


                    } else { // if progress >= dom.height
                        if (callback) { callback(); }
                    }

                }

                var imageData = ctx.getImageData(0, 0, dom.width, dom.height);

                batchPatch(imageData, dom, 0, function () {
                    ctx.putImageData(imageData, 0, 0);
                    if (callback) callback();
                });

            }

            var sprites = this.options.sprites;
            var stride = (this._features.tallAtlas) ? 1 : 5;
            var strideHeight = (sprites.ballFramesCountMidHifi / stride) * sprites.ballHeight;

            var moonDoubleCanvas = $("<canvas width=" + sprites.ballWidth * 2 * stride
                                         + " height=" + strideHeight + "/>")[0];
            var ctxdmc = moonDoubleCanvas.getContext('2d');
            _draw2dAtlas(ctxdmc,
                this._viewport.moonImage,
                sprites.ballWidth,
                sprites.ballHeight,
                sprites.ballFramesCountMidHifi,
                stride,
                0);

            _draw2dAtlas(ctxdmc,
                this._viewport.moonAlphaImage,
                sprites.ballWidth,
                sprites.ballHeight,
                sprites.ballFramesCountMidHifi,
                stride,
                sprites.ballWidth * stride);

            _composeAlphaAsync(ctxdmc, moonDoubleCanvas, function () {

                that._viewport.moonCanvasAtlas = $("<canvas width=" + sprites.ballWidth * stride
                                       + " height=" + strideHeight + "/>")[0];

                var ctxmca = that._viewport.moonCanvasAtlas.getContext('2d');
                ctxmca.drawImage(moonDoubleCanvas, 0, 0);
                $(moonDoubleCanvas).remove(); //does not shure is it works
                if (callback) callback();

            });
        },
        _drawMoon: function (ctx, atlasFrames) {
            var moonCanvasAtlas = this._viewport.moonCanvasAtlas;
            if (moonCanvasAtlas == null) return; // case, such a coldstart with hifi mode preset and image is still loading

            var sprites = this.options.sprites;
            var ctxmc = this._viewport.moonCanvas.getContext("2d");
            var ctxmlc = this._viewport.moonLightCanvas.getContext("2d");
            //ctxmlc.globalCompositeOperation = "lighter";
            ctxmlc.globalAlpha = "0.1";

            ctxmc.clearRect(0, 0, this._viewport.moonCanvas.width, this._viewport.moonCanvas.height);
            ctxmc.globalAlpha = 1;



            ctxmc.drawImage(this._viewport.moonCanvasAtlas,
                atlasFrames.stridePxLo, atlasFrames.atlasPxLo,
                this.options.sprites.ballWidth, this.options.sprites.ballHeight,
                0, 0,
                this.options.sprites.ballWidth, this.options.sprites.ballHeight);
            ctxmc.globalAlpha = atlasFrames.opacityHi;

            ctxmc.drawImage(this._viewport.moonCanvasAtlas,
                atlasFrames.stridePxHi, atlasFrames.atlasPxHi,
                this.options.sprites.ballWidth, this.options.sprites.ballHeight,
                0, 0,
                this.options.sprites.ballWidth, this.options.sprites.ballHeight);

            //ctxmlc.drawImage(this._viewport.moonCanvas, 0, 0);
            ctxmlc.drawImage(this._viewport.moonCanvas, 0, 0);

            //ctx.drawImage(this._viewport.moonLightCanvas, 0, 0);
            ctx.drawImage(this._viewport.moonCanvas, 0, 0);

        },
        _easter: function () {
            if (Math.round(this._engine.position) === 2440423) {
                if (this.easterDiv === undefined) {
                    if (this.snd === undefined) { this.snd = new Audio("moon-informer/img/neil.ogg"); }
                    this.snd.play();
                    this.easterDiv = $("<div class='ball' style='" +
                        "background-image: url(moon-informer/img/laytop-easter.png);" +
                        "background-position: 16px 30px;" +
                        "background-repeat: no-repeat;' />").appendTo(this.ballLofi);
                }
            } else { //any else day
                if (this.easterDiv !== undefined) {
                    this.easterDiv.remove();
                    this.easterDiv = undefined;
                }
            }
        },
        _preloadLofi: function (callback) {
            var that = this;
            if (this._preloadStates.lofi) {
                if (callback) callback();
            } else {
                $('<img>').attr({ src: this.options.sprites.ballFramesUrlLofi }).load(function () {
                    that._preloadStates.lofi = true;
                    if (callback) callback();
                });
            }

        },
        _preloadMidfi: function (callback) {
            var that = this;
            if (this._preloadStates.midfi) {
                if (callback) callback();
            } else {
                $('<img>').attr({ src: this.options.sprites.ballFramesUrlMidHifi }).load(function () {
                    that._preloadStates.midfi = true;
                    if (callback) callback();
                });
            }

        },
        _preloadHifi: function (callback) {
            var that = this;
            function _checkload() {
                if ((that._preloadStates.hifi) && (that._preloadStates.midfi)) {
                    that._buildmoonCanvasAtlas(function () {
                        that._viewport.yCanvas = $('<canvas width="660" height="145"/>')[0];
                        that._viewport.zCanvas = $('<canvas width="660" height="145"/>')[0];
                        that._viewport.moonCanvas = $("<canvas width=" + that.options.sprites.ballWidth + " height=" + that.options.sprites.ballHeight + "/>")[0];
                        that._viewport.moonLightCanvas = $("<canvas width=" + that.options.sprites.ballWidth + " height=" + that.options.sprites.ballHeight + "/>")[0];
                        if (callback) callback();
                    });
                    return true;
                }
                return false;
            }


            if (_checkload(callback)) {
            } else {
                that._viewport.moonImage = $('<img>').attr({ src: this.options.sprites.ballFramesUrlMidHifi }).load(function () {
                    that._preloadStates.midfi = true;
                    _checkload(callback);
                })[0]; //attention [0] moonimage is not a jquery, but a dom object
                that._viewport.moonAlphaImage = $('<img>').attr({ src: this.options.sprites.ballFramesUrlHifiAlpha }).load(function () {
                    that._preloadStates.hifi = true;
                    _checkload(callback);
                })[0];
            }

        },
        // events bound via _on are removed automatically
        // revert other modifications here
        _destroy: function () {
            // remove generated elements
            this.element
                .removeClass("moon-informer")
                .enableSelection()
                .css("background-color", "transparent")
                .html("");
        },
        // _setOptions is called with a hash of all options this are changing
        // always refresh when changing options
        _setOptions: function () {
            // _super and _superApply handle keeping the right this-context
            this._superApply(arguments);
            this._refresh();
        },
        // setter for inner use (and for outer too if you need em)
        setPosition: function (jDays) {
            this._wake();
            this._inputState.kineticDrift = false;
            this._engine.freefly.speed = this._engine.speed = (jDays - this._engine.position) / 60;
            this._engine.position = jDays;
        },
        setDate: function (to) { // to === "next" or "prev"
            this.setPosition((to === "next") ? this._engine.position + 1 : this._engine.position - 1);
        },
        setEsotericDay: function (to) { // to === "next" or "prev"

            var that = this;
            function searchBlock(currentJDays, to) { // to === "next" or "prev"

                var currentEsotericDay = that.jDaysToEsotericDay(currentJDays);
                for (var i = 0; (to === "next") ? i < 2 : i > -2; (to === "next") ? i += 0.1 : i -= 0.1) { //search 2 days with 2.4 hours step
                    if (currentEsotericDay !== that.jDaysToEsotericDay(currentJDays + i)) {
                        return currentJDays + i;
                    }
                }
                return null;

            }

            var firstChange = searchBlock(this._engine.position, to);
            var secondChange = searchBlock(firstChange, to);

            this.setPosition((firstChange + secondChange) / 2);
        },
        setMoonStage: function (to) { // to === "next" or "prev"

            var that = this;
            function searchBlock(currentJDays, to) { // to === "next" or "prev"

                var currentMoonStageId = that.moonPhaseToMoonStageId(that.jDaysToMoonPhase(currentJDays));
                for (var i = 0; (to === "next") ? i < 8 : i > -8; (to === "next") ? i += 0.25 : i -= 0.25) { //search 8 days with 6 hours step
                    if (currentMoonStageId !== that.moonPhaseToMoonStageId(that.jDaysToMoonPhase(currentJDays + i))) {
                        return currentJDays + i;
                    }
                }
                return null;

            }

            var firstChange = searchBlock(this._engine.position, to);
            var secondChange = searchBlock(firstChange, to);

            this.setPosition((firstChange + secondChange) / 2);
        },
        setZodiac: function (to) { // to === "next" or "prev"

            var that = this;
            function searchBlock(currentJDays, to) { // to === "next" or "prev"

                var currentZodiac = that.schmittDetails(currentJDays).zodiac;
                for (var i = 0; (to === "next") ? i < 5 : i > -5; (to === "next") ? i += 0.1 : i -= 0.1) { //search 5 days with 2.4 hours step
                    if (currentZodiac !== that.schmittDetails(currentJDays + i).zodiac) {
                        return currentJDays + i;
                    }
                }
                return null;

            }

            var firstChange = searchBlock(this._engine.position, to);
            var secondChange = searchBlock(firstChange, to);

            this.setPosition((firstChange + secondChange) / 2);
        },
        // _setOption is called for each individual option this is changing
        _setOption: function (key, value) {
            var that = this;
            // prevent invalid color values
            //if (/red|green|blue/.test(key) && (value < 0 || value > 255)) {
            //    return;
            //}
            if (key === "mode") {
                switch (value) {
                    case this.options.mode:
                        break; //only different
                    case "lofi":
                        this._preloadLofi(function () {
                            that.ballLofi.css({
                                "background-image": "url(" + that.options.sprites.ballFramesUrlLofi + ")", //revert to one-image mode
                            });
                            that.ballMidfi.css({
                                display: "none",
                            });
                            that.canvasHifi.css({
                                display: "none",
                            });
                            that.options._mode = "lofi";
                            that._refresh();
                        });
                        break;
                    case "midfi": //background-images will be set on _refresh
                        this._preloadMidfi(function () {
                            that.ballMidfi.css({
                                display: "block",
                            });
                            that.canvasHifi.css({
                                display: "none",
                            });
                            that.ballLofi.css({
                                "background-image": "url(" + that.options.sprites.ballFramesUrlMidHifi + ")", //revert to one-image mode
                            });
                            that.ballMidfi.css({
                                "background-image": "url(" + that.options.sprites.ballFramesUrlMidHifi + ")", //revert to one-image mode
                            });
                            that.options._mode = "midfi";
                            that._refresh();
                        });
                        break;
                    case "hifi":
                        this._preloadMidfi(function () {
                            that.ballMidfi.css({
                                display: "block",
                            });
                            that.canvasHifi.css({
                                display: "none",
                            });
                            that.ballLofi.css({
                                "background-image": "url(" + that.options.sprites.ballFramesUrlMidHifi + ")", //revert to one-image mode
                            });
                            that.ballMidfi.css({
                                "background-image": "url(" + that.options.sprites.ballFramesUrlMidHifi + ")", //revert to one-image mode
                            });
                            that.options._mode = "midfi";
                            that._refresh();

                            that._preloadHifi(function () {
                                that.ballMidfi.css({
                                    display: "none",
                                });
                                that.canvasHifi.css({
                                    display: "block",
                                });
                                that.ballLofi.css({
                                    "background-image": "none", //transparent div now only used to track mouse events
                                });
                                that._buildMilky();
                                that.options._mode = "hifi";
                                that._refresh();
                            });
                        });
                        break;
                }
            }
            if ((key === "blocks")||(key==="sizes"))
            {
                this._spansSizeRefresh();
            }
            this._super(key, value);
        }
    });
})(jQuery);