﻿"use strict";

$(function () {
    var astro = {
        //functions

        normalize: function (value) {
            value = value % 1;
            if (value < 0) { value += 1; }
            return value;
        },
        dateToJDays: function (date) { //original Zeno code by Stephen R. Schmitt
            var cDay = date.getDate();
            var cMonth = date.getMonth() + 1;
            var cYear = date.getFullYear();

            // calculate the Julian date at 12h UT
            var jYear = cYear - Math.floor((12 - cMonth) / 10);
            var jMonth = cMonth + 9;
            if (jMonth >= 12) { jMonth -= 12; }

            var k1 = Math.floor(365.25 * (jYear + 4712));
            var k2 = Math.floor(30.6 * jMonth + 0.5);
            var k3 = Math.floor(Math.floor((jYear / 100) + 49) * 0.75) - 38;

            var jDays = k1 + k2 + cDay + 59; // for dates in Julian calendar
            if (jDays > 2299160) {
                jDays -= k3; // for Gregorian calendar
            }
            return jDays;
        },
        jDaysToDate: function (jDays) {
            var a = jDays + 32044;
            var b = Math.floor((4 * a + 3) / 146097);
            var c = a - Math.floor(146097 * b / 4);
            var d = Math.floor((4 * c + 3) / 1461);
            var e = c - Math.floor((1461 * d) / 4);
            var m = Math.floor((5 * e + 2) / 153);

            var day = e - Math.floor((153 * m + 2) / 5) + 1;
            var month = m + 3 - (12 * Math.floor(m / 10));
            var year = 100 * b + d - 4800 + Math.floor(m / 10);
            return new Date(year, month - 1, day);
        },
        moonPhaseToMoonStageId: function (phase) {
            //var ids = 8;
            //var result =  Math.round((astro.normalize(phase+0)*8));

            var ag = phase * astro.jMoonDayLongs;

            var result = (ag < 1.84566) ? 0 :
                (ag < 5.53699) ? 1 :
                (ag < 9.22831) ? 2 :
                (ag < 12.91963) ? 3 :
                (ag < 16.61096) ? 4 :
                (ag < 20.30228) ? 5 :
                (ag < 23.99361) ? 6 :
                (ag < 27.68493) ? 7 : 0;
            return result;
        },
        bvToRgb: function (bv) {
            var result = {};
            result.r = Math.round(Math.max(0, Math.min(255, 255 - Math.max(0, -bv * 50))));
            result.b = Math.round(Math.max(0, Math.min(255, 255 - Math.max(0, bv * 50))));
            result.g = Math.round((result.r + result.b) / 2);
            return result;
        },
        vToOpacity: function (v, mulOnFive) {
            if (mulOnFive === undefined) mulOnFive = 100;
            return Math.pow(mulOnFive, -v / 5);
        },


        // constants
        jMoonDayLongs: 29.530588853,
        jNewShift: -2451550.1,
    };
    var locale = {
        monthNames: ["января", "февраля", "матра", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
        weekDays: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
        moonStages: ["новолуние", "растущий месяц", "I четверть", "растущая луна", "полнолуние", "убывающая луна", "III четверть", "спадающий месяц"],
        zodiacNames: ["Овен", "Телец", "Близнецы", "Рак", "Лев", "Дева", "Весы", "Скорпион", "Стрелец", "Козерог", "Водолей", "Рыбы"],
        booleanshit: {
            weekDayDetails: [
                "Сегодня велика вероятность того, что сон вещий, в особенности для тех, <b>кто родился в понедельник</b>.",
                "Сны вторника важны и могут сбыться, но только <b>не позднее чем через десять дней</b>.",
                "Сон может быть вещим, если приснится <b>до начала</b> новых лунных суток.",
                "В этот день большинство снов являются вещими.",
                "Сон, скорее всего, исполнится, если он как-то связан с личной жизнью.",
                "Сегодня стоит уделять внимание тем снам, которые вы видели <b>в предрассветные часы</b>.",
                "Сны сегодня очень важны, особенно те, что описывают <b>сюжеты, связанные с развлечениями и удовольствиями</b>."
            ],
            moonDayDetails: [
                "К тому же, сегодня вы можете по средствам сна предугадать грядущие перемены.",
                "Однако если сон не имеет ясного сюжета, значения ему лучше не придавать.",
                "К тому же, события сна довольно скоро станут явью.",
                "Также сегодня сюжет сновидения может содержать <b>важные предостережения</b>.",
                "К тому же, сегодня сны способны нести информацию о состоянии здоровья.",
                "Однако, отдельные события сна могут стать явью и через продолжительное время.",
                "Сон может стать явью, если вы <b>сохраните его в тайне</b>.",
                "Как правило, сегодняшний сон, так или иначе, связан с осуществлением заветной мечты.",
                "И нельзя забывать о том, что сегодня сон может предостеречь от беды.",
                "К тому же, сегодняшний сон в некоторых случаях может сулить неприятные события.",
                "Однако, сегодняшний сон подчиняется влиянию Луны и если сбудется, то только через <b>11 дней</b>.",
                "Помните, что сегодня сны могут дать <b>важные советы</b>.",
                "Некоторые из снов могут нести предостережения о развитии прежних проблем.",
                "Большинство сновидений сегодня обманчиво пугающие, не стоит придавать таким сюжетам существенного значения.",
                "Однако, сегодня Луна делает сны очень важными, они способны предсказывать скорые <b>приятные перемены</b>.",
                "Некоторые сны этого дня под влиянием Луны способны выражать накопившиеся эмоции.",
                "Если вы в замешательстве, сегодняшний сон может дать важную подсказку.",
                "Сон о какой либо вещи сегодня может сулить ее скорое приобретение.",
                "Если в семье накалена обстановка, сон этого дня может предупреждать о серьезных семейных неприятностях.",
                "Сны этого дня могут дать ответ на вопрос, который вас мучил последнее время и в особенности в день накануне.",
                "Плохие сны сегодня это, скорее исключение из правил, но их ни в коем случае нельзя недооценивать.",
                "К тому же, сегодня некоторые сны могут нести предупреждение о неприятностях.",
                "Некоторые сюжеты снов этого дня могут быть связаны с событиями прошлого.",
                "Так же, сегодняшний сон может отражать то, насколько вы довольны собой.",
                "В сюжете сегодняшних снов так же могут быть предупреждения о  том, что вас кто-то обманет.",
                "Некоторые из сегодняшних снов способны пробудить желание действовать,  таким сюжетам стоит верить.",
                "Иногда в сегодняшних снах можно встретить подсказки интуиции.",
                "Вам следует обратить внимание на сны, которые намекают на препятствия в делах.",
                "Тревожные сны сегодня обманчивы.",
                "Если ваш сон имеет фантастический сюжет, его толкование должно быть внимательным, смысл его скрыт в образах."
            ],
            monthDayDetails: [
                "И не забудьте, сегодняшние сны намекают вам на то, как вы близки к своей цели.",
                "Помните, сегодняшний сон может подсказать о том, к чему могут привести проделанная вами работа.",
                "Уделите внимание снам с эмоциональной окраской, они могут предупреждать о судьбоносных переменах.",
                "При толковании важно обратить внимание на то, насколько эмоционально сновидец переживает события сна.",
                "Обратите внимание на тот сон, который вас напугал, он особенно важен.",
                "Обратите внимание на то, что часть снов этого дня может быть связана со здоровьем.",
                "Знайте, сегодня сон может предупредить вас о некой грядущей и важной новости.",
                "Помните, что довольно часто сны сегодняшнего дня говорят о том, насколько вы будете обеспечены материально в будущем.",
                "Особое внимание уделите событиям, которые случились в середине сюжета сна.",
                "Не оставляйте без внимания пугающие сюжеты, они могут вас предупреждать об опасности.",
                "При толковании сконцентрируйте внимание на самом начале сновидения.",
                "Сегодняшний сон о беременности стоит воспринимать только буквально.",
                "Обратите внимание на самые неприятные моменты сновидения, именно им следует придать ключевое значение.",
                "Если сон не имел четкого сюжета, придавать ему значения не стоит.",
                "Сегодня сон может подсказать, что нужно сделать для достижения цели.",
                "Особенно важны сегодня цветные сновидения.",
                "Если сон сегодня был очень приятным, это крайне дурной знак.",
                "Помните, сегодня сны особенно важны, если привиделись они при дождливой погоде.",
                "Если сны сегодняшнего дня способны подтолкнуть к решительным поступкам, их следует принять в серьез.",
                "В этот день сны женщин крайне важны, а вот мужчинам лучше не принимать увиденное на веру.",
                "Обратите внимание на сны, которые видели до полуночи.",
                "Так же сегодня крайне важны сны, что привиделись до полуночи.",
                "Сны раннего утра могут оказаться вещими.",
                "Если сон намекает на грядущие болезни, уделите ему особенное внимание.",
                "Следует быть внимательнее к сновидениям с участием близких.",
                "И знайте, сегодняшний сон во многом связан с вашим материальным положением.",
                "Если вы стоите перед сложным выбором, сегодняшний сон может вам подсказать, как быть.",
                "Все, что вас напугало во сне сегодня, следует анализировать очень внимательно.",
                "Особенно важны сегодня сны, в которых можно расслышать чей-то голос.",
                "Если вы сегодня внезапно проснулись, то сновидение, которое было последним, особенно важно.",
                "Сны этого дня могут быть предупреждением т о крайне внезапных событиях."
            ],
        }
    };
    var booleanshit = {
        weekDay: [33, 15, 17, 30, 17, 17, 25],
        moonDay: [30, 0, 30, 25, 25, 15, 30, 30, 30, 30, 0, 25, 30, 5, 30, 25, 15, 15, 5, 30, 3, 30, 0, 15, 15, 17, 10, 30, 0, 10],
        monthDay: [30, 3, 33, 25, 31, 15, 15, 16, 10, 28, 11, 30, 28, 30, 32, 30, 20, 33, 25, 30, 17, 14, 15, 32, 30, 30, 15, 10, 0, 5, 15]
    };
    

    // the widget definition, where "moon" is the namespace,
    // "informer" the widget name
    $.widget("moon.informer", {
        // default options
        options: {
            sprites: {
                ballWidth: 190, //px mouse = 360 degrees engine - 1/28.. of jDay
                ballHeight: 145,
                ballFramesCountMidHifi: 90,
                ballFramesCountHifiAlpha: 30,
                ballFramesCountLofi: 30,
                ballFramesUrlMidHifi: "moonInformer/img/atlas-190x145x90.jpg",
                ballFramesUrlLofi: "moonInformer/img/atlas-190x145x30.jpg", //lofi
                ballFramesUrlHifiAlpha: "moonInformer/img/atlas-alpha-190x145x90.png"
            },

            mode: "lofi",

            // callbacks
            change: null,
        },

        _engine: {
            position: 2456294, //jDays
            speed: 0, //jDaysPerSec
            mouseUniPos: 0, //in 0to1 uni
            sleep:false,
            freefly: {
                interval: null, //used for setinterval canceling
                position: 2456294, //smooth, not actual version
                speed: 0, //smooth, not actual version
                blurPosition: 2456294, //even more smoother
                blurSpeed: 0, //even more smoother just interapolates freefly.speed
                blurCalced: 0, //even more smoother calculates delta from blurPositon
                starPosition: 2456294, //even much more smoother
                starSpeed: 0, //even much more smoother
                starSpeedCalced: 0, //even much more smoother 
            },
            uniflow: {
                interval: null, //used for setinterval binding, no need to cansel it, actually
                blurPos: 0,
                blurSpeed:0,
                blurMoState:0 //0to1 smooth true-false of _inputState.mouseOnDetails
            }
        },
        _interface: {
            lastShown: { //used for cute refreshing when changes only
                jDays: null, //used for direction detection
                gregDate: null,
                moonStage: null,
                zodiac: null,
                esotericDay: null,
            }
        },

        _inputState: {
            lastMoveTimeStamp: null, //Date.now()
            lastMovePosition: null, //just x as number
            pressed: false,
            pressedPosition: null, //just x as number
            pressedDegrees: null, //just x as number
            mouseOnDetails: false, //used to set speed augmentation in uniflow
        },

        _preloadStates: {
            lofi: false,
            midfi: false,
            hifi: false,
            milki: false,
        },

        _viewport: {
            moonCenter: { x: 140, y: 70 },
            moonSize: 190,
            width: 660,
            height: 130,
            top: -70 + 15, //diff from moon position
            bottom: 130 - 70 + 15,
            left: -140,
            right: 660 - 140,
            canvas: $(".moon-canvas")[0],
            yCanvas: $('<canvas width="660" height="145"/>')[0],
            zCanvas: $('<canvas width="660" height="145"/>')[0],
            moonCanvasAtlas: null,
            moonCanvas: null, //init in _preloadHifi
            moonLightCanvas: null, //init in _preloadHifi
            moonImage: null,
            moonAlphaImage: null,
        },

        _milky: {
            background: [], //not used now
            real: []
        },
        // helper functions
        schmittDetails: function (jDays) {

            var result = {
                distance: null,
                ecliptic: {}, // {la,lo}
                zodiac: null
            };
            // calculate moon's age in days
            var mPhase = this.jDaysToMoonPhase(jDays);

            var mPhaseRad = mPhase * 2 * Math.PI; // Convert phase to radians

            // calculate moon's distance
            var distPhase = 2 * Math.PI * astro.normalize((jDays - 2451562.2) / 27.55454988);
            var dist = 60.4 - 3.3 * Math.cos(distPhase) - 0.6 * Math.cos(2 * mPhaseRad - distPhase) - 0.5 * Math.cos(2 * mPhaseRad);
            result.distance = dist; //->

            // calculate moon's ecliptic latitude
            var nPhaseRad = 2 * Math.PI * astro.normalize((jDays - 2451565.2) / 27.212220817);
            var eclipticLa = 5.1 * Math.sin(nPhaseRad);
            result.ecliptic.la = eclipticLa; //->

            // calculate moon's ecliptic longitude
            var rPhase = astro.normalize((jDays - 2451555.8) / 27.321582241);
            var eclipticLo = 360 * rPhase + 6.3 * Math.sin(distPhase) + 1.3 * Math.sin(2 * mPhaseRad - distPhase) + 0.7 * Math.sin(2 * mPhaseRad);
            eclipticLo = (eclipticLo + 360) % 360;
            result.ecliptic.lo = eclipticLo; //->

            // hmm seems to be wrong
            // upd: seems to be perfectly right, as it is in 2000 epoch,
            // but sorry, mr. Schmitt, scientific astronomy sucks,
            // Ancient Greece astrology rules.
            //
            //result.zodiac = (eclipticLo < 33.18) ? 11
            //    : (eclipticLo < 51.16) ? 0
            //    : (eclipticLo < 93.44) ? 1
            //    : (eclipticLo < 119.48) ? 2
            //    : (eclipticLo < 135.30) ? 3
            //    : (eclipticLo < 173.34) ? 4
            //    : (eclipticLo < 224.17) ? 5
            //    : (eclipticLo < 242.57) ? 6
            //    : (eclipticLo < 271.26) ? 7
            //    : (eclipticLo < 302.49) ? 8
            //    : (eclipticLo < 311.72) ? 9
            //    : (eclipticLo < 348.58) ? 10
            //    : 11;

            result.zodiac = (Math.floor(eclipticLo * 12 / 360) % 12);
            return result;
        },
        jDaysToEsotericDay: function (jDays) {
            //dumb placeholder, not a real algorithm
            var result = Math.round(this.jDaysToMoonPhase(jDays) * 28) + 1;
            return result;

        },
        jDaysToMoonPhase: function (jDays) {
            if ((jDays === undefined) || (jDays === null)) { jDays = this._engine.position; } //default
            var unnormalizedPhase = (jDays + astro.jNewShift) / astro.jMoonDayLongs;
            return astro.normalize(unnormalizedPhase);
        },
        moonPhaseToChops: function(phase,chopsCount,soft) {            
            var floatChop =
                (
                ((phase * chopsCount) % chopsCount)
                + chopsCount)
                % chopsCount;
            var atlasChopLo,
                atlasChopHi,
                opacityHi,
                atlasChop;
            
                atlasChopLo = Math.floor(floatChop);
                atlasChopHi = Math.ceil(floatChop) % chopsCount;            
                opacityHi = floatChop - atlasChopLo;
                atlasChop = Math.round(floatChop) % chopsCount;

            return { atlasChop: atlasChop, atlasChopLo: atlasChopLo, atlasChopHi: atlasChopHi, opacityHi: opacityHi, floatChop: floatChop };

        },
        jDaysToMoonAtlasFrames: function (jDays, mode) {
            if ((jDays === undefined) || (jDays === null)) { jDays = this._engine.position; } //default
            if ((mode === undefined) || (mode === null)) { mode = this.options._mode; } //default
            var phase = this.jDaysToMoonPhase(jDays);

            switch (mode) {                
                case "lofi":

                    var framesCount = this.options.sprites.ballFramesCountLofi;
                    var chops = this.moonPhaseToChops(phase, framesCount, false);
                    return {
                        atlasFrame: chops.atlasChop,
                        atlasPx: chops.atlasChop * this.options.sprites.ballHeight,
    
                    };
                    break; //yepyep

                case "midfi":
                    var framesCount = this.options.sprites.ballFramesCountMidHifi;
                    
                    var chops = this.moonPhaseToChops(phase, framesCount, true); //rich mode - soft=true

                    return {
                        atlasFrameLo: chops.atlasChopLo,
                        atlasFrameHi: chops.atlasChopHi,
                        atlasPxLo: chops.atlasChopLo * this.options.sprites.ballHeight,
                        atlasPxHi: chops.atlasChopHi * this.options.sprites.ballHeight,
                        opacityHi: chops.opacityHi,
                    };                
                case "hifi":
                    var framesCount = this.options.sprites.ballFramesCountMidHifi;
                    var stride = 5;
                    var strideHeight = framesCount / stride;
                    var chops = this.moonPhaseToChops(phase, framesCount, true);
                    return {
                        atlasFrame: chops.atlasChop % strideHeight,
                        atlasPx: (chops.atlasChop % strideHeight) * this.options.sprites.ballHeight,
                        stridePx: Math.floor(chops.atlasChop / strideHeight) * this.options.sprites.ballHeight,

                        atlasPxLo: (chops.atlasChopLo % strideHeight) * this.options.sprites.ballHeight,
                        atlasPxHi: (chops.atlasChopHi % strideHeight) * this.options.sprites.ballHeight,

                        //strideLo: Math.floor(chops.atlasChopLo / (chops.atlasChopLo / stride)),
                        //strideHi: Math.floor(chops.atlasChopHi / (chops.atlasChopHi / stride)),
                        
                        stridePxLo: Math.floor(chops.atlasChopLo / strideHeight) * this.options.sprites.ballWidth,
                        stridePxHi: Math.floor(chops.atlasChopHi / strideHeight) * this.options.sprites.ballWidth,
                        opacityHi: chops.opacityHi,

                    };
                    break; //yepyep
            }

            
        },

        gregEsoToBooleanshitDetails: function (gregDate, esotericDay) {            
            return (
                locale.booleanshit.weekDayDetails[(gregDate.getDay()+6)%7] + " "
              + locale.booleanshit.moonDayDetails[esotericDay-1] + " "
              + locale.booleanshit.monthDayDetails[gregDate.getDate() - 1]
              + " (<a href='#'>подробнее...</a>)"
            );
        },
        gregEsoToBooleanshitPersent: function (gregDate, esotericDay) {            
            return (
                booleanshit.weekDay[(gregDate.getDay()+6)%7] +
              + booleanshit.moonDay[esotericDay-1] + 
              + booleanshit.monthDay[gregDate.getDate() - 1]
            );
        },        


        dev: function () {
            return this._viewport.moonCanvasAtlas;
        },
        // the constructor
        _create: function () {
            var that = this;
            this.element
                // add a class for theming
                .addClass("moon-informer");
            // prevent double click to select text
            //.disableSelection();            
           
            this.ballLofi = $(".moon-ball-lofi", this.element);
            this.ballMidfi = $(".moon-ball-midfi", this.element);

            $(document).on("mousemove", function (e) { that._onMouseMove(e); });
            this.ballLofi.on("mousedown", function (e) { that._onMouseDown(e); });
            this.element.on("mouseup", function (e) { that._onMouseUp(e); });
            $(document).on("mouseup", function (e) { that._onMouseUp(e); });

            this.canvasHifi = $(this._viewport.canvas);
            this.swipeDate = $(".moon-swipe-date", this.element);
            this.swipeDate.on("click", "a", function () { this._datepickerShow(); });
            this.swipeWeekDay = $(".moon-swipe-weekDay", this.element);
            this.swipeMoonStage = $(".moon-swipe-moonStage", this.element);
            this.swipeEsotericDay = $(".moon-swipe-esotericDay", this.element);
            this.swipeZodiac = $(".moon-swipe-zodiac", this.element);
            this.swipeDetailedText = $(".moon-swipe-detailedText", this.element);
            this.swipePercent = $(".moon-swipe-percent", this.element);
            this.swipeStar1 = $(".moon-swipe-star1", this.element);
            this.swipeStar2 = $(".moon-swipe-star2", this.element);
            this.swipeStar3 = $(".moon-swipe-star3", this.element);
            this.swipeStar4 = $(".moon-swipe-star4", this.element);
            this.swipeStar5 = $(".moon-swipe-star5", this.element);
            this.swipeStars = $(".stars", this.element);

            this.textSensor = $(".moon-sensor", this.element);
            this.textSensor.on("mousemove", function (e) {
                that._onSensorMouseMove(e);
            });
            this.textSensor.on("mouseenter", function (e) {
                that._inputState.mouseOnDetails = true;                
            });
            this.textSensor.on("mouseleave", function (e) {
                that._inputState.mouseOnDetails = false;
                that._engine.mouseUniPos = 0;
            });
            
            $(window).focus(function () { that._wake(); })
                     .blur(function () { that._sleep(); });
            this.options._mode = "lofi"; //add a backing field (switches after preloading, used in logic, while .mode is for seting from outer space)
            

            if (this.options.mode === "lofi") {
                this.ballLofi.css({ "background-image": "url(" + this.options.sprites.ballFramesUrlLofi + ")" }); //revert to one-image mode
                this.canvasHifi.css({ display: "none" });
                this._preloadLofi(function () {
                    that.options._mode = "lofi";
                    that._refresh();
                });
            }
            if (this.options.mode === "midfi") {
                this.ballLofi.css({ "background-image": "url(" + this.options.sprites.ballFramesUrlMidHifi + ")" });
                this.ballMidfi.css({ "background-image": "url(" + this.options.sprites.ballFramesUrlMidHifi + ")" });
                this.canvasHifi.css({ display: "none" });
                this._preloadMidfi(function () {
                    that.options._mode = "midfi";
                    that._refresh();
                });
            }
            if (this.options.mode === "hifi") {
                this.ballLofi.css({ "background-image": "none" });
                this.ballMidfi.css({ display: "none" });
                this.canvasHifi.css({ display: "block" });
                this._preloadHifi(function () {
                    that.options._mode = "hifi";
                    that._buildMilky();
                    that._refresh();
                });
            }

            

            this.swipeDate.swipeLabel();
            this.swipeWeekDay.swipeLabel();
            this.swipeMoonStage.swipeLabel();
            this.swipeEsotericDay.swipeLabel();
            this.swipeZodiac.swipeLabel();            
            //this.swipeDetailedText.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 230, "charsAt100": 180, "uniPos": 0 });
            this.swipePercent.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 90 });
            this.swipeStar1.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 14 });
            this.swipeStar2.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 14 });
            this.swipeStar3.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 14 });
            this.swipeStar4.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 14 });
            this.swipeStar5.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 14 });
            
            this.swipeDetailedText.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 220, "uniPos": 0 });
            
            this._refresh();
        },
        _onSensorMouseMove: function (e) {
            var that = this;
            var sensorTop = this.textSensor.offset().top;
            var margin = 30;
            var uniPos = (e.pageY - sensorTop-margin) / (this._viewport.height-margin*2); //0to1
            var uniPos = Math.min(1, Math.max(0, uniPos));
            this._engine.mouseUniPos = uniPos;

            if (this._engine.uniflow.interval === null) {
                this._engine.uniflow.interval = setInterval(function () { that._uniflow(); }, 1000 / 60); //check for once
                this._uniflow();
            }
            
        },
        _datepickerShow: function () {
            $("#hiddenDatepicker").datepicker();  //TODO: remertertert!!
            $("#hiddenDatepicker").datepicker("show");  //TODO: remertertert!!
        },
        _onMouseMove: function (e) {
            if (this._inputState.pressed === true) {
                var msecNow = Date.now();
                if ((this._inputState.lastMoveTimeStamp !== null) && (this._inputState.lastMovePosition)) {
                    var deltaTime = Math.max(msecNow - this._inputState.lastMoveTimeStamp, 10); //mouse.min to flatten peaks with anomaly shorten periods, produsing huge velocity
                    this._engine.speed = ((this._inputState.lastMovePosition - e.clientX) * astro.jMoonDayLongs / (this.options.sprites.ballWidth * 2)) * 1000 / (deltaTime);
                }
                this._inputState.lastMoveTimeStamp = msecNow;
                this._inputState.lastMovePosition = e.clientX;


                this._engine.position = ((this._inputState.pressedPosition - e.clientX) * astro.jMoonDayLongs / (this.options.sprites.ballWidth * 2)) + this._inputState.pressedJDays;
            }
        },

        _onMouseDown: function (e) {
            var that = this;            
            // temp on
            if (this.options.mode==="lofi") this._setOption("mode", "hifi");


            this.element.disableSelection();
            this._engine.speed = 0; //stopping on moveless click
            this._engine.freefly.speed = 0; //stopping on moveless click
            //inputState.lastMoveTimeStamp = Date.now();
            //inputState.lastMovePosition = e.clientX;
            this._inputState.pressedPosition = e.clientX;
            this._inputState.pressedJDays = this._engine.position;
            this._inputState.pressed = true;

            if (this._engine.freefly.interval === null) {
                this._engine.freefly.interval = setInterval(function () { that._freefly(); }, 1000 / 60); //check for once
                this._freefly();
            }
        },


        _freefly: function () {
            if (this._engine.sleep) return;
            
            if (this._inputState.pressed === false) {                

                if ((Math.abs(this._engine.freefly.starSpeedCalced) < 0.01) &&
                   (Math.abs(this._engine.freefly.blurSpeedCalced) < 0.01) &&
                   (Math.abs(this._engine.freefly.speed) < 0.01) &&
                   (Math.abs(this._engine.speed) < 0.01)) {
                    //clearInterval(this._engine.freefly.interval);
                    //this._engine.freefly.interval = null; //end interval loop
                    this._engine.freefly.starSpeedCalced = 0;
                    this._engine.freefly.blurSpeedCalced = 0;
                    this._engine.freefly.speed = 0;
                    this._engine.speed = 0;
                    return;
                }

                this._engine.freefly.speed *= 0.99; //slowing here

                if (Math.abs(this._engine.freefly.speed) < 10) { //slowing harder when slower 260 (60)
                    var speedSign = this._engine.freefly.speed ? this._engine.freefly.speed < 0 ? -1 : 1 : 0;
                    var speedMinus = 0.1;
                    var extraFriction = (Math.abs(this._engine.freefly.speed) < speedMinus) ? this._engine.freefly.speed : (speedSign * speedMinus);
                    this._engine.freefly.speed -= extraFriction;
                }
                if (Math.abs(this._engine.freefly.speed) < 0.2) { this._engine.freefly.speed = 0; } //stop when slower +-2
                this._engine.position = this._engine.position + (this._engine.freefly.speed / 60); // /
            } else { //pressed==true                
                this._engine.freefly.speed = this._engine.freefly.speed * 0.75 + this._engine.speed * 0.25;
            }
            this._engine.freefly.position = this._engine.freefly.position * 0.75 + this._engine.position * 0.25;

            var oldBlurPosition = this._engine.freefly.blurPosition;
            this._engine.freefly.blurPosition = this._engine.freefly.blurPosition * 0.75 + this._engine.freefly.position * 0.25;
            this._engine.freefly.blurSpeed = this._engine.freefly.blurSpeed * 0.75 + this._engine.freefly.speed * 0.25;
            this._engine.freefly.blurSpeedCalced = (this._engine.freefly.blurPosition - oldBlurPosition) * 60;


            var oldStarPosition = this._engine.freefly.starPosition;
            this._engine.freefly.starPosition = this._engine.freefly.starPosition * 0.80 + this._engine.freefly.blurPosition * 0.2;
            this._engine.freefly.starSpeedCalced = (this._engine.freefly.starPosition - oldStarPosition) * 60;




            this._refresh();
            this._engine.speed *= 0.75; //slowing for times where no mousemoves hit

        },


        _uniflow: function () {
            if (this._engine.sleep) return;




            
            this._engine.uniflow.blurMoState = (this._inputState.mouseOnDetails===true) ? this._engine.uniflow.blurMoState * 0.9 + 0.1: 0;

            var moStatePow = Math.pow(this._engine.uniflow.blurMoState, 100);
            
            
            var oldBlurPosition = this._engine.uniflow.blurPos;
            this._engine.uniflow.blurPos =
              //  this._engine.uniflow.blurPos * (0.9 - this._engine.uniflow.blurMoState * 0.2)
              //+ this._engine.mouseUniPos * (0.1 + this._engine.uniflow.blurMoState * 0.2);
                this._engine.uniflow.blurPos * (0.95 - moStatePow * 0.2) // from .75 to .95
              + this._engine.mouseUniPos * (0.05 + moStatePow * 0.2); //from .25 to .05

            this._engine.uniflow.blurSpeed = (this._engine.uniflow.blurPos - oldBlurPosition) * 60;

            if (Math.abs(this._engine.uniflow.blurSpeed) < 0.001) {
                this._engine.uniflow.blurSpeed = 0;
                //return;
            }
            this._refreshDetailedUniPos();

            
        },
        
        _onMouseUp: function (e) {
            this.element.enableSelection();
            this._inputState.pressed = false;            
        },

        _wake: function () {
            this._engine.sleep = false;
        },
        
        _sleep: function () {
            this._engine.sleep = true;
        },

        // called when created, and later when changing options
        _refresh: function () {

            var af = this.jDaysToMoonAtlasFrames(this._engine.freefly.position);
            switch (this.options._mode) {
                case "lofi":
                    this.ballLofi.css({
                        "background-position": "left " + -af.atlasPx + "px"
                    });
                    break;
                case "midfi":
                    this.ballLofi.css({
                        "background-position": "left " + -af.atlasPxLo + "px",
                    });
                    this.ballMidfi.css({
                        "background-position": "left " + -af.atlasPxHi + "px",
                        "opacity": af.opacityHi
                    });
                    break;
                case "hifi":                    
                    this._refreshCanvas(af);
                    break;
            }
            var detailedTextOpacity = Math.pow(1 - Math.min(Math.max(Math.abs(this._engine.freefly.blurSpeed) / 300, 0), 1), 20); //0to300 -> 0 to 1, then pow2                        
            this.swipeDetailedText.css({ opacity: detailedTextOpacity * 0.6 + 0.2 });
            this.swipePercent.css({ opacity: detailedTextOpacity * 0.6 + 0.4 });
            this.swipeStars.css({ opacity: detailedTextOpacity * 0.6 + 0.4 });
            
            this.swipeDetailedText.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed });

            //this.swipePercent.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed });
            this.swipePercent.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed/4 });

            this.swipeWeekDay.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed });
            this.swipeEsotericDay.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed });

            var gregDate = astro.jDaysToDate(this._engine.position);
            var moonStage = locale.moonStages[
                astro.moonPhaseToMoonStageId(
                    this.jDaysToMoonPhase(
                        this._engine.position))];
            var esotericDay = this.jDaysToEsotericDay(this._engine.position);
            var zodiac = this.schmittDetails(this._engine.position).zodiac;


            var direction = (this._interface.lastShown.jDays === null) ? "left" : (this._engine.position > this._interface.lastShown.jDays) ? "left" : "right";
            var duration = this._inputState.pressed ? Math.min(1600 / Math.abs(this._engine.freefly.speed), 200) : Math.min(1600 / Math.abs(this._engine.freefly.speed), 1000);

            // debug
            //console.time('someFunction timer');
            // /debug
            //console.log(duration);

            if (
                    (this._interface.lastShown.gregDate === null) ||
                    (this._interface.lastShown.gregDate.getTime() !== gregDate.getTime()) // smart update for date-sense info
                ) {
                this.swipeDate.swipeLabel({
                    content: $("<a class='date' href='#'>" + gregDate.getDate() + " " + locale.monthNames[gregDate.getMonth()] + " " + gregDate.getFullYear() + "</a>"),
                    duration: duration,
                    direction: direction
                });
                this.swipeWeekDay.swipeLabel({
                    content: $("<span>").text(locale.weekDays[gregDate.getDay()]),
                    duration: duration,
                    direction: direction
                });

                var booleanshitPercent = this.gregEsoToBooleanshitPersent(gregDate, esotericDay);
                
                this.swipePercent.swipeLabel({
                    content: $("<span>").text(booleanshitPercent + "%"),
                    duration: duration*3.5,
                    direction: direction
                });

                this.swipeStar1.swipeLabel({
                    content: (booleanshitPercent >= 25) ? $("<div class='star'></div>") : $("<div class='star op50'></div>"),
                    duration: Math.min(1000,Math.max(200, duration*3.5)),
                    direction: direction
                });
                this.swipeStar2.swipeLabel({
                    content: (booleanshitPercent > 40) ? $("<div class='star'></div>") : $("<div class='star op50'></div>"),
                    duration: Math.min(1000, Math.max(200, duration * 3.5)),
                    direction: direction
                });
                this.swipeStar3.swipeLabel({
                    content: (booleanshitPercent >= 50) ? $("<div class='star'></div>") : $("<div class='star op50'></div>"),
                    duration: Math.min(1000, Math.max(200, duration * 3.5)),
                    direction: direction
                });
                this.swipeStar4.swipeLabel({
                    content: (booleanshitPercent >= 75) ? $("<div class='star'></div>") : $("<div class='star op50'></div>"),
                    duration: Math.min(1000, Math.max(200, duration * 3.5)),
                    direction: direction
                });
                this.swipeStar5.swipeLabel({
                    content: (booleanshitPercent > 90) ? $("<div class='star'></div>") : $("<div class='star op50'></div>"),
                    duration: Math.min(1000, Math.max(200, duration * 3.5)),
                    direction: direction
                });



                this.swipeDetailedText.swipeLabel({
                    content: $("<span>").html(this.gregEsoToBooleanshitDetails(gregDate, esotericDay)),
                    duration: duration * 2.5,
                    direction: direction
                });


                this._interface.lastShown.gregDate = gregDate; //lastShown
            }

            if (
                    (this._interface.lastShown.zodiac === null) ||
                    (this._interface.lastShown.zodiac !== zodiac) // smart update for zodiac-sense info
                ) {
                this.swipeZodiac.swipeLabel({
                    content: $("<a href='#'>" + locale.zodiacNames[zodiac] + "</a>"),
                    duration: Math.min(duration * 2, 1000),
                    direction: direction
                });


                this._interface.lastShown.zodiac = zodiac; //lastShown
            }



            if (
                    (this._interface.lastShown.esotericDay === null) ||
                    (this._interface.lastShown.esotericDay !== esotericDay) // smart update for esotericDay-sense info
                ) {
                this.swipeEsotericDay.swipeLabel({
                    content: $("<a href='#'>" + esotericDay + "</a>"),
                    duration: duration,
                    direction: direction
                });
                

                this._interface.lastShown.esotericDay = esotericDay; //lastShown
            }

            if (
                    (this._interface.lastShown.moonStage === null) ||
                    (this._interface.lastShown.moonStage !== moonStage) // smart update for moonStage-sense info
                ) {
                this.swipeMoonStage.swipeLabel({
                    content: $("<a class='stage' href='#'>" + moonStage + "</a>"),
                    duration: Math.min(duration * 2, 1000),
                    direction: direction
                });

                this._interface.lastShown.moonStage = moonStage; //lastShown
            }

            this._interface.lastShown.jDays = this._engine.position; //lastShown

            // debug
            //console.timeEnd('someFunction timer')
            // /debug


            // trigger a callback/event
            this._trigger("change");
        },

        _refreshDetailedUniPos: function () {
            this.swipeDetailedText.swipeLabel({ uniPos: this._engine.uniflow.blurPos });
        },
        
        _buildMilky: function () {
            if (this._preloadStates.milki) return;
            var that = this;
            // not used now
            //this._milky.background = [];
            //for (var i = 0; i < 2000; i++) {
            //    var newStar = {};
            //    newStar.la = Math.random() * 30 - 15;
            //    newStar.lo = Math.random() * 360;
            //    newStar.v = (Math.pow(Math.random(), 1) * 7) + 0;
            //    newStar.bv = Math.random() * 2 - 1;
            //    if (newStar.bv > 0) newStar.bv *= 2.5;
            //    newStar.rgb = astro.bvToRgb(newStar.bv);
            //    newStar.opacity = astro.vToOpacity(newStar.v, 10);
            //    this._milky.background.push(newStar);
            //};
            

            $.getJSON('moonInformer/starsd.json', function (data) {
                that._preloadStates.milki = true;
                that._milky.real = [];

                var deltaLo = 0;
                // getting well-sorted delta-coded json and decode
                $.each(data, function (key, value) {
                    var dataStar = data[key];
                    var newStar = {};
                    newStar.lo = dataStar[0] + deltaLo;
                    deltaLo = newStar.lo;
                    newStar.la = dataStar[1];
                    newStar.lo = +deltaLo;
                    newStar.rgb = astro.bvToRgb(dataStar[3]);
                    newStar.opacity = astro.vToOpacity(dataStar[2], 10);
                    that._milky.real.push(newStar);
                });
            });
        },

        _refreshCanvas: function (atlasFrames) {

            var schmitt = this.schmittDetails(this._engine.freefly.starPosition);
            var schmittBefore = this.schmittDetails(this._engine.freefly.starPosition - this._engine.freefly.starSpeedCalced / 60); //used for bluring moved stars
            var schmittAfter = this.schmittDetails(this._engine.freefly.starPosition + this._engine.freefly.starSpeedCalced / 60); //used for bluring moved stars


            var canvas = this._viewport.canvas;

            var zCanvas = this._viewport.zCanvas;


            var ctx = canvas.getContext('2d');           
            var ctxz = zCanvas.getContext('2d');


            ctxz.globalAlpha = 0.75;

            ctxz.clearRect(0, 0, canvas.width, canvas.height);
            ctxz.drawImage(canvas, 0, 0); //feedback here
            ctxz.globalAlpha = 1;
            
            
            var moonSizeDeg = 1; //0.5 for real            
            var degMul = this._viewport.moonSize / moonSizeDeg;
            this._drawStars(ctxz, this._milky.real, schmitt, schmittBefore, schmittAfter, { degMul: degMul, sectorDegree: 360, opacity: 1, lightspeed: true });

            moonSizeDeg = 20; //0.5 for real            
            degMul = this._viewport.moonSize / moonSizeDeg;
            this._drawStars(ctxz, this._milky.real, schmitt, schmittBefore, schmittAfter, { degMul: degMul, sectorDegree: 360, opacity: 0.1, noLa: true });

            //ctxz.drawImage(zCanvas, 0, 0);            
            this._drawMoon(ctxz, atlasFrames);

            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(zCanvas, 0, 0);

            //console.log(
            //    this._engine.position.toFixed(2)+"   "+
            //    this._engine.speed.toFixed(2)+" | "+
            //    this._engine.freefly.position.toFixed(2)+"   "+
            //    this._engine.freefly.speed.toFixed(2)+" | "+
            //    this._engine.freefly.blurPosition.toFixed(2)+"   "+
            //    this._engine.freefly.blurSpeed.toFixed(2)+" | "+
            //    this._engine.freefly.starPosition.toFixed(2)+"   "+
            //    this._engine.freefly.starSpeedCalced.toFixed(2)
            //    );

        },


        _drawStars: function (ctx, stars, schmitt, schmittBefore, schmittAfter, options) {
            options = $.extend({
                sectorDegree: 360, //default
                degMul: this._viewport.moonSize / 0.5, //default .5 is close to real moon angular size from earth
                opacity: 1,
                stroke: 1,
            }, options);
            var viewportDeg = $.extend({}, this._viewport); //cloning and making degrees-verison
            $.each(viewportDeg, function (index, value) {
                viewportDeg[index] = value / options.degMul;
            });

            ctx.lineWidth = options.stroke;
            //ctx.beginPath(); //alt
            //var alpha = 1 / Math.min(20, Math.max(1, Math.abs(this._engine.freefly.starSpeedCalced))) * options.opacity; //alt
            //ctx.strokeStyle = "rgba(255,255,255,"+alpha+")"; //alt

            var viewSectorLeft = Math.floor((schmitt.ecliptic.lo + viewportDeg.left) / options.sectorDegree);
            var viewSectorRight = Math.floor((schmitt.ecliptic.lo + viewportDeg.right) / options.sectorDegree);
            for (var vs = viewSectorLeft; vs <= viewSectorRight; vs++) {

                for (var i = 0; i < stars.length; i++) {
                    var star = stars[i];

                    var laTravel = options.noLa ? 0 : Math.min(1, Math.abs(15 / this._engine.freefly.starSpeedCalced));

                    var moonDiff = {
                        lo: (star.lo + (vs * options.sectorDegree) - schmitt.ecliptic.lo),
                        la: (star.la - schmitt.ecliptic.la * laTravel)
                    };
                    moonDiff.x = moonDiff.lo * options.degMul;
                    moonDiff.y = moonDiff.la * options.degMul;

                    if ((moonDiff.x > this._viewport.left)
                        && (moonDiff.x < this._viewport.right)
                        && (moonDiff.y > this._viewport.top)
                        && (moonDiff.y < this._viewport.bottom)) {

                        var blurWidth = this._engine.freefly.starSpeedCalced / 2 + 0.5;
                        var alpha = 27
                            * star.opacity
                            / Math.min(options.lightspeed ? 10 : 20, Math.max(1, Math.abs(this._engine.freefly.starSpeedCalced)))
                            * options.opacity; //not this.options, but from function params


                        ctx.beginPath();
                        var eclipticTileShiftBefore = ((schmittBefore.ecliptic.lo <= 90) && (schmitt.ecliptic.lo > 270)) ? +360
                                                   : ((schmittBefore.ecliptic.lo > 270) && (schmitt.ecliptic.lo <= 90)) ? -360 : 0;
                        var blurDiffBefore = {
                            la: (schmittBefore.ecliptic.la - schmitt.ecliptic.la) * laTravel,
                            lo: schmittBefore.ecliptic.lo - schmitt.ecliptic.lo + eclipticTileShiftBefore,
                        };
                        var eclipticTileShiftAfter = ((schmittAfter.ecliptic.lo <= 90) && (schmitt.ecliptic.lo > 270)) ? +360
                                                  : ((schmittAfter.ecliptic.lo > 270) && (schmitt.ecliptic.lo <= 90)) ? -360 : 0;
                        var blurDiffAfter = {
                            la: (schmittAfter.ecliptic.la - schmitt.ecliptic.la) * laTravel,
                            lo: schmittAfter.ecliptic.lo - schmitt.ecliptic.lo + eclipticTileShiftAfter,
                        };

                        ctx.strokeStyle = "rgba(" + star.rgb.r + "," + star.rgb.g + "," + star.rgb.b + "," + alpha + ")";
                        ctx.moveTo(moonDiff.x + this._viewport.moonCenter.x - blurWidth, moonDiff.y + this._viewport.moonCenter.y); //olschool
                        ctx.lineTo(moonDiff.x + this._viewport.moonCenter.x + blurWidth, moonDiff.y + this._viewport.moonCenter.y);
                        ctx.moveTo(moonDiff.x + this._viewport.moonCenter.x + blurDiffBefore.lo * options.degMul, //plus newchool
                                   moonDiff.y + this._viewport.moonCenter.y + blurDiffBefore.la * options.degMul);
                        ctx.lineTo(moonDiff.x + this._viewport.moonCenter.x,
                                   moonDiff.y + this._viewport.moonCenter.y);
                        ctx.lineTo(moonDiff.x + this._viewport.moonCenter.x + blurDiffAfter.lo * options.degMul,
                                   moonDiff.y + this._viewport.moonCenter.y + blurDiffAfter.la * options.degMul);

                        ctx.stroke();

                    }
                }
            }
            //ctx.stroke(); //alt


        },

        _buildmoonCanvasAtlas: function (callback) {
            
            function _draw2dAtlas(ctx,domObject,ballWidth,ballHeight,framesCount,stride,left) {
                if (framesCount % stride !== 0) { $.error("stride must split frames in whole columns (framesCount%stride must = 0)"); return; }
                var strideHeight = (framesCount / stride)*ballHeight;
                for (var s = 0; s < stride; s++) {
                    ctx.drawImage(domObject,
                        0, s * strideHeight,
                        ballWidth, strideHeight,                        
                        s * ballWidth + left, 0,
                        ballWidth, strideHeight);
                }
            }

            var that = this;
            function _composeAlphaAsync(ctx, dom, callback) {                
                function batchPatch(imageData, dom, progress, callback) {
                    if (progress < dom.height) {
                        if ((that._engine.freefly.starSpeedCalced === 0)&&(that._engine.uniflow.blurSpeed === 0)) {                            
                            var top = progress,
                                step = 145;

                            var halfWidth = dom.width / 2;
                            var x, y;
                            for (x = 0; x < halfWidth; x++) {
                                for (y = top; y < top + step; y++) {
                                    //var y = top;
                                    imageData.data[(x + (y * dom.width)) * 4 + 3] = imageData.data[(x + halfWidth + (y * dom.width)) * 4];
                                }
                            }

                            progress = top + step;
                        } 
                        setTimeout(function() { batchPatch(imageData, dom, progress, callback); }, 1);
                        

                    } else { // if progress >= dom.height
                        if (callback) { callback(); }
                    }
                   
                }

                var imageData = ctx.getImageData(0, 0, dom.width, dom.height);

                batchPatch(imageData, dom, 0, function () {                    
                    ctx.putImageData(imageData, 0, 0);
                    if (callback) callback();
                });

            }
                        
            var sprites = this.options.sprites;
            var stride = 5;
            var strideHeight = (sprites.ballFramesCountMidHifi / stride) * sprites.ballHeight;
            
            var moonDoubleCanvas = $("<canvas width=" + sprites.ballWidth * 2 * stride
                                         + " height=" + strideHeight + "/>")[0];
            var ctxdmc = moonDoubleCanvas.getContext('2d');
            _draw2dAtlas(ctxdmc,
                this._viewport.moonImage,
                sprites.ballWidth,
                sprites.ballHeight,
                sprites.ballFramesCountMidHifi,
                stride,
                0);

            _draw2dAtlas(ctxdmc,
                this._viewport.moonAlphaImage,
                sprites.ballWidth,
                sprites.ballHeight,
                sprites.ballFramesCountMidHifi,
                stride,
                sprites.ballWidth * stride);

            _composeAlphaAsync(ctxdmc, moonDoubleCanvas,function() {

                that._viewport.moonCanvasAtlas = $("<canvas width=" + sprites.ballWidth * stride
                                       + " height=" + strideHeight + "/>")[0];

                var ctxmca = that._viewport.moonCanvasAtlas.getContext('2d');
                ctxmca.drawImage(moonDoubleCanvas, 0, 0);
                $(moonDoubleCanvas).remove(); //does not shure is it works
                if (callback) callback();

            });
            
            //var dmcImageData = ctxdmc.getImageData(0, 0, moonDoubleCanvas.width, moonDoubleCanvas.height);
            
            //for (var x = 0; x < moonDoubleCanvas.width/2; x++) {
            //    for (var y = 0; y < moonDoubleCanvas.height; y++) {
            //        dmcImageData.data[(x + (y * moonDoubleCanvas.width)) * 4 + 3] = dmcImageData.data[(x + stride*sprites.ballWidth + (y * moonDoubleCanvas.width)) * 4];                    
            //    }
            //}           
            //ctxdmc.putImageData(dmcImageData, 0, 0);


        },

        _drawMoon: function (ctx, atlasFrames) {                        
            var moonCanvasAtlas = this._viewport.moonCanvasAtlas;            
            if (moonCanvasAtlas == null) return; // case, such a coldstart with hifi mode preset and image is still loading

            var sprites = this.options.sprites;
            var ctxmc = this._viewport.moonCanvas.getContext("2d");
            var ctxmlc = this._viewport.moonLightCanvas.getContext("2d");
            //ctxmlc.globalCompositeOperation = "lighter";
            ctxmlc.globalAlpha = "0.1";
            
            ctxmc.clearRect(0, 0, this._viewport.moonCanvas.width, this._viewport.moonCanvas.height);
            ctxmc.globalAlpha = 1;

            

            ctxmc.drawImage(this._viewport.moonCanvasAtlas,
                atlasFrames.stridePxLo, atlasFrames.atlasPxLo,
                this.options.sprites.ballWidth, this.options.sprites.ballHeight,
                0, 0,
                this.options.sprites.ballWidth, this.options.sprites.ballHeight);
            ctxmc.globalAlpha = atlasFrames.opacityHi;
            
            ctxmc.drawImage(this._viewport.moonCanvasAtlas,
                atlasFrames.stridePxHi, atlasFrames.atlasPxHi,
                this.options.sprites.ballWidth, this.options.sprites.ballHeight,
                0, 0,
                this.options.sprites.ballWidth, this.options.sprites.ballHeight);

            //ctxmlc.drawImage(this._viewport.moonCanvas, 0, 0);
            ctxmlc.drawImage(this._viewport.moonCanvas, 0, 0);

            //ctx.drawImage(this._viewport.moonLightCanvas, 0, 0);
            ctx.drawImage(this._viewport.moonCanvas, 0, 0);

        },


        _preloadMuchData: function () {
            //fires after _preloadMidfi loaded
        },
        _preloadLofi: function (callback) {
            var that = this;
            if (this._preloadStates.lofi) {
                if (callback) callback();
            } else {
                $('<img>').attr({ src: this.options.sprites.ballFramesUrlLofi }).load(function () {
                    that._preloadStates.lofi = true;
                    if (callback) callback();
                });
            }

        },
        _preloadMidfi: function (callback) {
            var that = this;
            if (this._preloadStates.midfi) {                
                if (callback) callback();
            } else {
                $('<img>').attr({ src: this.options.sprites.ballFramesUrlMidHifi }).load(function () {
                    that._preloadStates.midfi = true;
                    if (callback) callback();
                });
            }
            
        },
        _preloadHifi: function (callback) {            
            var that = this;
            function _checkload() {
                if ((that._preloadStates.hifi) && (that._preloadStates.midfi)) {                    
                    that._buildmoonCanvasAtlas(function () {
                        that._viewport.moonCanvas = $("<canvas width=" + that.options.sprites.ballWidth + " height=" + that.options.sprites.ballHeight + "/>")[0];
                        that._viewport.moonLightCanvas = $("<canvas width=" + that.options.sprites.ballWidth + " height=" + that.options.sprites.ballHeight + "/>")[0];
                        if (callback) callback();                        
                    });                    
                    return true;
                }
                return false;
            }            
            

            if (_checkload(callback)) {
            } else {                
                that._viewport.moonImage = $('<img>').attr({ src: this.options.sprites.ballFramesUrlMidHifi }).load(function () {
                    that._preloadStates.midfi = true;
                    _checkload(callback);
                })[0]; //attention [0] moonimage is not a jquery, but a dom object
                that._viewport.moonAlphaImage = $('<img>').attr({ src: this.options.sprites.ballFramesUrlHifiAlpha }).load(function () {
                    that._preloadStates.hifi = true;
                    _checkload(callback);
                })[0];
            }

        },
        // events bound via _on are removed automatically
        // revert other modifications here
        _destroy: function () {
            // remove generated elements
            this.element
                .removeClass("moon-informer")
                .enableSelection()
                .css("background-color", "transparent");
        },

        // _setOptions is called with a hash of all options this are changing
        // always refresh when changing options
        _setOptions: function () {
            // _super and _superApply handle keeping the right this-context
            this._superApply(arguments);
            //this._refresh();
        },

        // _setOption is called for each individual option this is changing
        _setOption: function (key, value) {
            var that = this;
            // prevent invalid color values
            //if (/red|green|blue/.test(key) && (value < 0 || value > 255)) {
            //    return;
            //}
            if (key === "mode") {


                switch (value) {
                    case "lofi":
                        this._preloadLofi(function () {
                            that.ballLofi.css({
                                "background-image": "url(" + that.options.sprites.ballFramesUrlLofi + ")", //revert to one-image mode
                            });
                            that.ballMidfi.css({
                                display: "none",
                            });
                            that.canvasHifi.css({
                                display: "none",
                            });
                            that.options._mode = "lofi";
                            that._refresh();
                        });
                        break;
                    case "midfi": //background-images will be set on _refresh
                        this._preloadMidfi(function () {
                            that.ballMidfi.css({
                                display: "block",
                            });
                            that.canvasHifi.css({
                                display: "none",
                            });
                            that.ballLofi.css({
                                "background-image": "url(" + that.options.sprites.ballFramesUrlMidHifi + ")", //revert to one-image mode
                            });
                            that.ballMidfi.css({
                                "background-image": "url(" + that.options.sprites.ballFramesUrlMidHifi + ")", //revert to one-image mode
                            });
                            that.options._mode = "midfi";
                            that._refresh();
                        });
                        break;
                    case "hifi":                        
                        this._preloadHifi(function () {
                            that.ballMidfi.css({
                                display: "none",
                            });
                            that.canvasHifi.css({
                                display: "block",
                            });
                            that.ballLofi.css({
                                "background-image": "none", //transparent div now only used to track mouse events
                            });
                            that._buildMilky();
                            that.options._mode = "hifi";                            
                            that._refresh();
                        });
                        break;
                }
            }
            this._super(key, value);
        }
    });

    // initialize with default options
    $("#my-widget1").informer();

});