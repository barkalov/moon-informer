﻿// JavaScript source code

//may be it will go to another js file
var json2013 = $.getJSON("informer-2013.json", function (data) {    
    $.extend(preloadedDayInfos,data);
});


var preloadedDayInfos = {};
preloadedDayInfos.hundredPackStatus = {};

function getDayInfo(jDays) {
    var hundredPack = Math.floor(jDays / 100); //int div
    var info = preloadedDayInfos[jDays];
    var aviable = ((info===undefined)||(info===null))? false: true 
    
}
var astro;
$(function () {
    astro = {
        //functions

        normalize: function (value) {
            value = value % 1;
            if (value < 0) { value += 1 };
            return value;
        },
        dateToJDays: function (date) { //original Zeno code by Stephen R. Schmitt
            var cDay = date.getDate();
            var cMonth = date.getMonth() + 1;
            var cYear = date.getFullYear();

            // calculate the Julian date at 12h UT
            var jYear = cYear - Math.floor((12 - cMonth) / 10);
            var jMonth = cMonth + 9;
            if (jMonth >= 12) { jMonth -= 12; }

            var k1 = Math.floor(365.25 * (jYear + 4712));
            var k2 = Math.floor(30.6 * jMonth + 0.5);
            var k3 = Math.floor(Math.floor((jYear / 100) + 49) * 0.75) - 38;

            var jDays = k1 + k2 + cDay + 59; // for dates in Julian calendar
            if (jDays > 2299160) {
                jDays -= k3; // for Gregorian calendar
            }
            return jDays;
        },
        jDaysToDate: function (jDays) {
            var a = jDays + 32044;
            var b = Math.floor((4 * a + 3) / 146097);
            var c = a - Math.floor(146097 * b / 4);
            var d = Math.floor((4 * c + 3) / 1461);
            var e = c - Math.floor((1461 * d) / 4);
            var m = Math.floor((5 * e + 2) / 153);

            var day = e - Math.floor((153 * m + 2) / 5) + 1;
            var month = m + 3 - (12 * Math.floor(m / 10));
            var year = 100 * b + d - 4800 + Math.floor(m / 10);
            return new Date(year, month - 1, day);
        },
        moonPhaseToMoonStageId: function (phase) {
            //var ids = 8;
            //var result =  Math.round((astro.normalize(phase+0)*8));

            var ag = phase*astro.jMoonDayLongs;
    
            var result=(ag <  1.84566)? 0:
                (ag <  5.53699)?1:
                (ag <  9.22831)?2:
                (ag < 12.91963)?3:
                (ag < 16.61096)?4:
                (ag < 20.30228)?5:
                (ag < 23.99361)?6:
                (ag < 27.68493)?7:0
            return result;
        },
        bvToRgb: function(bv) {
            var result = {};
            result.r=Math.round(Math.max(0,Math.min(255,255-Math.max(0,-bv*50))));
            result.b=Math.round(Math.max(0,Math.min(255,255-Math.max(0, bv*50))));
            result.g = Math.round((result.r + result.b) / 2);
            return result;            
        },
        vToOpacity: function (v, mulOnFive) {
            if (mulOnFive === undefined) mulOnFive = 100;
            return Math.pow(mulOnFive, -v / 5);
        },


        // constants
        jMoonDayLongs: 29.530588853,
        jNewShift: -2451550.1,
    }
    var locale = {
        monthNames: ["января", "февраля", "матра", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
        weekDays: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
        moonStages: ["новолуние", "растущий месяц", "I четверть", "растущая луна", "полнолуние", "убывающая луна", "III четверть", "спадающий месяц"],
        zodiacNames: ["Овен", "Телец", "Близнецы", "Рак", "Лев", "Дева", "Весы", "Скорпион", "Стрелец", "Козерог", "Водолей", "Рыбы"],
        detailedTexts: [
            "Сны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные события осуществятся <b>в течение 10 суток</b>. Однако короткие сны сегодня всъерьёз воспринимать не стоит.",
            "Сборная России <b>по хоккею</b> обыграла канадцев в матче за бронзовые медали молодежного (до 20 лет) <b>чемпионата мира</b>, проходившего в Уфе. Об этом сообщается на сайте ИИХФ.",
            "Игра закончилась <b>со счетом 6:5</b>, победную для россиян шайбу Валерий Ничушкин забросил <b>на второй минуте овертайма</b>. Также в составе сборной России отличились Александр Хохлачев, Наиль Якупов (дважды), Кирилл Дьяков и Евгений Мозер.",
            "Сборная Канады осталась без медалей на молодежном ЧМ <b>впервые за 15 турниров</b>. Глава ФХР Владислав Третьяк заявил после окончания матча за третье место, что <b>в целом доволен</b> результатом россиян.",
            "Я никакого провала не вижу. <b>Молодежный хоккей непредсказуем</b>, психология у них еще неустойчивая. Они еще не умеют выигрывать и <b>не умеют проигрывать</b>, сказал Третьяк.",
            "В полуфинале россияне <b>уступили сборной Швеции</b> в серии буллитов. После этого, в СМИ появились слухи о нарушениях дисциплины со стороны хоккеистов, которые якобы <b>слишком много времени проводили вне гостиницы</b>."
        ]

    };


    // the widget definition, where "moon" is the namespace,
    // "informer" the widget name
    $.widget("moon.informer", {
        // default options
        options: {
            sprites: {
                ballWidth: 190, //px mouse = 360 degrees engine - 1/28.. of jDay
                ballHeight: 145,
                ballFramesCount: 90,
            },

            mode: "midfi",

            red: 255,
            green: 0,
            blue: 0,

            // callbacks
            change: null,
            random: null
        },

        _engine: {
            position: 2456294, //jDays
            speed: 0, //jDaysPerSec
            freefly: {
                interval: null, //used for setinterval canceling
                position: 2456294, //smooth, not actual version
                speed: 0, //smooth, not actual version
                blurPosition: 2456294, //even more smoother
                blurSpeed: 0, //even more smoother just interapolates freefly.speed
                blurCalced: 0, //even more smoother calculates delta from blurPositon
                starPosition: 2456294, //even much more smoother
                starSpeed: 0, //even much more smoother
                starSpeedCalced: 0, //even much more smoother 
                

            }
        },
        _interface: {
            lastShown: { //used for cute refreshing when changes only
                jDays: null, //used for direction detection
                gregDate: null,
                moonStage: null,
                zodiac: null,
                esotericDay: null,
            }
        },

        _inputState: {
            lastMoveTimeStamp: null, //Date.now()
            lastMovePosition: null, //just x as number
            pressed: false,
            pressedPosition: null, //just x as number
            pressedDegrees: null //just x as number
        },
        
  
        _viewport: {
            moonCenter: {x:140, y:70},
            moonSize : 190,
            width : 660,
            height : 130,
            top : -70 + 15, //diff from moon position
            bottom : 130 - 70 + 15,
            left : -140,
            right: 660 - 140,
            canvas: $(".moon-canvas")[0],
            yCanvas: $('<canvas width="660" height="145"/>')[0],
            zCanvas: $('<canvas width="660" height="145"/>')[0],
        },

        _milky: {
            background: [],
            real: []
        },
        schmittDetails: function (jDays) {

            var result = {
                distance: null,
                ecliptic: {}, // {la,lo}
                zodiac: null
            };
            // calculate moon's age in days
            var mPhase = this.jDaysToMoonPhase(jDays);            

            var mPhaseRad = mPhase * 2 * Math.PI; // Convert phase to radians

            // calculate moon's distance
            var distPhase = 2 * Math.PI * astro.normalize((jDays - 2451562.2) / 27.55454988);
            var dist = 60.4 - 3.3 * Math.cos(distPhase) - 0.6 * Math.cos(2 * mPhaseRad - distPhase) - 0.5 * Math.cos(2 * mPhaseRad);
            result.distance = dist; //->

            // calculate moon's ecliptic latitude
            var nPhaseRad = 2 * Math.PI * astro.normalize((jDays - 2451565.2) / 27.212220817);
            var eclipticLa = 5.1 * Math.sin(nPhaseRad)
            result.ecliptic.la = eclipticLa; //->

            // calculate moon's ecliptic longitude
            var rPhase = astro.normalize((jDays - 2451555.8) / 27.321582241)
            var eclipticLo = 360 * rPhase + 6.3 * Math.sin(distPhase) + 1.3 * Math.sin(2 * mPhaseRad - distPhase) + 0.7 * Math.sin(2 * mPhaseRad)
            eclipticLo = (eclipticLo+360) % 360;
            result.ecliptic.lo = eclipticLo; //->
            //console.log(eclipticLo);

            // hmm seems to be wrong
            // upd: seems to be perfectly right, as it is in 2000 epoch,
            // but sorry, mr. Schmitt, scientific astronomy sucks,
            // Ancient Greece astrology rules.
            //
            //result.zodiac = (eclipticLo < 33.18) ? 11
            //    : (eclipticLo < 51.16) ? 0
            //    : (eclipticLo < 93.44) ? 1
            //    : (eclipticLo < 119.48) ? 2
            //    : (eclipticLo < 135.30) ? 3
            //    : (eclipticLo < 173.34) ? 4
            //    : (eclipticLo < 224.17) ? 5
            //    : (eclipticLo < 242.57) ? 6
            //    : (eclipticLo < 271.26) ? 7
            //    : (eclipticLo < 302.49) ? 8
            //    : (eclipticLo < 311.72) ? 9
            //    : (eclipticLo < 348.58) ? 10
            //    : 11;

            result.zodiac = (Math.floor(eclipticLo * 12 / 360) % 12);
            return result;
        },
        jDaysToEsotericDay: function(jDays) {
            //dumb placeholder, not a real algorithm
            var result= Math.round(this.jDaysToMoonPhase(jDays)*28)+1;
            return result;

    },
        jDaysToMoonPhase: function (jDays) {
            if ((jDays == undefined) || (jDays == null)) { jDays = this._engine.position; } //default
            var unnormalizedPhase = (jDays + astro.jNewShift) / astro.jMoonDayLongs;
            return astro.normalize(unnormalizedPhase);
        },
        jDaysToMoonAtlasFrames: function (jDays) {
            if ((jDays == undefined) || (jDays == null)) { jDays = this._engine.position; } //default
            var framesCount = this.options.sprites.ballFramesCount;
            var floatFrame =
                (
                ((this.jDaysToMoonPhase(jDays) * framesCount) % framesCount)
                + framesCount)
                % framesCount;
            var atlasFrame =
                            (
                            (Math.round(floatFrame) % framesCount)
                            + framesCount)
                            % framesCount;
            var atlasFrameLo =
                            (
                            (Math.floor(floatFrame) % framesCount)
                            + framesCount)
                            % framesCount;
            var atlasFrameHi =
                            (
                            (Math.ceil(floatFrame) % framesCount)
                            + framesCount)
                            % framesCount;
            var opacityHi = floatFrame - atlasFrameLo;
            return { atlasFrame: atlasFrame, atlasFrameLo: atlasFrameLo, atlasFrameHi: atlasFrameHi, opacityHi: opacityHi, floatFrame: floatFrame };
        },

        // the constructor
        _create: function () {
            that = this;
            this.element
                // add a class for theming
                .addClass("moon-informer")
                // prevent double click to select text
                //.disableSelection();

            this.buildMilky();

            this.ballLofi = $(".moon-ball-lofi", this.element);
            this.ballMidfi = $(".moon-ball-midfi", this.element);
            $(document).on("mousemove", this._onMouseMove);
            this.ballLofi.on("mousedown", this._onMouseDown);
            this.element.on("mouseup", this._onMouseUp);
            $(document).on("mouseup", this._onMouseUp)
            this.swipeDate = $(".moon-swipe-date", this.element);
            this.swipeDate.on("click", "a", function () { that._datepickerShow() });
            this.swipeWeekDay = $(".moon-swipe-weekDay", this.element);
            this.swipeMoonStage = $(".moon-swipe-moonStage", this.element);
            this.swipeEsotericDay = $(".moon-swipe-esotericDay", this.element);
            this.swipeZodiac = $(".moon-swipe-zodiac", this.element);
            this.swipeDetailedText = $(".moon-swipe-detailedText", this.element);
            this.swipeDate.swipeLabel();
            this.swipeWeekDay.swipeLabel();
            this.swipeMoonStage.swipeLabel();
            this.swipeEsotericDay.swipeLabel();
            this.swipeZodiac.swipeLabel();
            this.swipeDetailedText.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 230  });
            this._refresh();
        },

        _datepickerShow: function() {
            $("#hiddenDatepicker").datepicker("show");  //TODO: remertertert!!
        },
        _onMouseMove: function (e) {
            if (that._inputState.pressed === true) {
                var msecNow = Date.now();
                if ((that._inputState.lastMoveTimeStamp != null) && (that._inputState.lastMovePosition)) {
                    var deltaTime = Math.max(msecNow - that._inputState.lastMoveTimeStamp,10); //mouse.min to flatten peaks with anomaly shorten periods, produsing huge velocity
                    that._engine.speed = ((that._inputState.lastMovePosition - e.clientX) * astro.jMoonDayLongs / (that.options.sprites.ballWidth * 2)) * 1000 / (deltaTime);
                }
                that._inputState.lastMoveTimeStamp = msecNow;
                that._inputState.lastMovePosition = e.clientX;
                

                that._engine.position = ((that._inputState.pressedPosition - e.clientX) * astro.jMoonDayLongs / (that.options.sprites.ballWidth * 2)) + that._inputState.pressedJDays;                
            }
        },

        _onMouseDown: function (e) {
            that.element.disableSelection();
            that._engine.speed = 0; //stopping on moveless click
            that._engine.freefly.speed = 0; //stopping on moveless click
            //inputState.lastMoveTimeStamp = Date.now();
            //inputState.lastMovePosition = e.clientX;
            that._inputState.pressedPosition = e.clientX;
            that._inputState.pressedJDays = that._engine.position;
            that._inputState.pressed = true;

            if (that._engine.freefly.interval === null) {
                that._engine.freefly.interval = setInterval(that._freefly, 1000 / 60) //check for once
                that._freefly();
            }
        },


        _freefly: function () {

            if (that._inputState.pressed == false) {

                if ((Math.abs(that._engine.freefly.starSpeedCalced) < 0.01) &&
                   (Math.abs(that._engine.freefly.blurSpeedCalced) < 0.01) &&
                   (Math.abs(that._engine.freefly.speed) < 0.01) &&
                   (Math.abs(that._engine.speed) < 0.01)) {
                    //clearInterval(that._engine.freefly.interval);
                    //that._engine.freefly.interval = null; //end interval loop
                    that._engine.freefly.starSpeedCalced = 0;
                    that._engine.freefly.blurSpeedCalced = 0;
                    that._engine.freefly.speed = 0;
                    that._engine.speed = 0;
                    return;
                }

                that._engine.freefly.speed *= 0.99; //slowing here

                if (Math.abs(that._engine.freefly.speed) < 10) { //slowing harder when slower 260 (60)
                    var speedSign = that._engine.freefly.speed ? that._engine.freefly.speed < 0 ? -1 : 1 : 0;
                    var speedMinus = 0.1;
                    var extraFriction = (Math.abs(that._engine.freefly.speed) < speedMinus) ? that._engine.freefly.speed : (speedSign * speedMinus);                    
                    that._engine.freefly.speed -= extraFriction;
                }
                if (Math.abs(that._engine.freefly.speed) < 0.2) { that._engine.freefly.speed = 0 } //stop when slower +-2
                that._engine.position = that._engine.position + (that._engine.freefly.speed / 60); // /
            } else { //pressed==true                
                that._engine.freefly.speed = that._engine.freefly.speed * 0.75 + that._engine.speed * 0.25;
            }
            that._engine.freefly.position = that._engine.freefly.position * 0.75 + that._engine.position * 0.25;

            var oldBlurPosition = that._engine.freefly.blurPosition;
            that._engine.freefly.blurPosition = that._engine.freefly.blurPosition * 0.75 + that._engine.freefly.position * 0.25;
            that._engine.freefly.blurSpeed = that._engine.freefly.blurSpeed * 0.75 + that._engine.freefly.speed * 0.25;
            that._engine.freefly.blurSpeedCalced = (that._engine.freefly.blurPosition - oldBlurPosition) * 60;
            
            
            var oldStarPosition = that._engine.freefly.starPosition;
            that._engine.freefly.starPosition =  that._engine.freefly.starPosition * 0.80 + that._engine.freefly.blurPosition * 0.2;
            that._engine.freefly.starSpeedCalced = (that._engine.freefly.starPosition - oldStarPosition) * 60;




            that._refresh();
            that._engine.speed *= 0.75; //slowing for times where no mousemoves hit

        },

        _onMouseUp: function (e) {
            that.element.enableSelection();
            that._inputState.pressed = false;
        },



        // called when created, and later when changing options
        _refresh: function () {


            //console.log(this._engine.position, this.schmittDetails(this._engine.position).distance * 6371);
            //console.log(this._engine.speed, this._engine.freefly.speed);
            var af = this.jDaysToMoonAtlasFrames(this._engine.freefly.position);
            switch (this.options.mode) {
                case "lofi":
                    this.ballLofi.css({
                        "background-position": "left " + -af.atlasFrame * this.options.sprites.ballHeight + "px"
                    });
                    break;
                case "midfi":
                    this.ballLofi.css({
                        "background-position": "left " + -af.atlasFrameLo * this.options.sprites.ballHeight + "px"
                    });
                    this.ballMidfi.css({
                        "background-position": "left " + -af.atlasFrameHi * this.options.sprites.ballHeight + "px",
                        "opacity": af.opacityHi
                    });
                    break;
            }
            var detailedTextOpacity = Math.pow(1 - Math.min(Math.max(Math.abs(that._engine.freefly.blurSpeed) / 300, 0), 1), 20); //0to300 -> 0 to 1, then pow2                        
            that.swipeDetailedText.css({ opacity: detailedTextOpacity*0.6+0.2 });
            that.swipeDetailedText.swipeLabel({ constantBlur: that._engine.freefly.blurSpeed });

//            that.swipeZodiac.swipeLabel({ constantBlur: that._engine.freefly.blurSpeed });

            that.swipeWeekDay.swipeLabel({ constantBlur: that._engine.freefly.blurSpeed });
            that.swipeEsotericDay.swipeLabel({ constantBlur: that._engine.freefly.blurSpeed });

            var gregDate = astro.jDaysToDate(this._engine.position);            
            var moonStage = locale.moonStages[
                astro.moonPhaseToMoonStageId(
                    this.jDaysToMoonPhase(
                        this._engine.position))];
            var esotericDay = this.jDaysToEsotericDay(this._engine.position);
            var zodiac = this.schmittDetails(this._engine.position).zodiac;
            

            var direction = (this._interface.lastShown.jDays === null) ? "left" : (this._engine.position > this._interface.lastShown.jDays) ? "left" : "right";            
            var duration = this._inputState.pressed ? Math.min(1600 / Math.abs(this._engine.freefly.speed), 200) : Math.min(1600 / Math.abs(this._engine.freefly.speed), 1000);

            // debug
            //console.time('someFunction timer');
            // /debug


            if (
                    (this._interface.lastShown.gregDate === null) ||
                    (this._interface.lastShown.gregDate.getTime() != gregDate.getTime()) // smart update for date-sense info
                ) {
                this.swipeDate.swipeLabel({
                    content: $("<a class='date' href='#'>"+gregDate.getDate() + " " + locale.monthNames[gregDate.getMonth()] + " " + gregDate.getFullYear()+"</a>"),
                    duration: duration,
                    direction: direction
                });
                this.swipeWeekDay.swipeLabel({
                    content: $("<span>").text(locale.weekDays[gregDate.getDay()]),
                    duration: duration,
                    direction: direction
                });
                this.swipeDetailedText.swipeLabel({
                    content: $("<span>").html(locale.detailedTexts[Math.round(this._engine.position)%6]),
                    duration: duration*2.5,
                    direction: direction
                });


                this._interface.lastShown.gregDate = gregDate; //lastShown
            }

            if (
                    (this._interface.lastShown.zodiac === null) ||
                    (this._interface.lastShown.zodiac != zodiac) // smart update for zodiac-sense info
                ) {                                
                this.swipeZodiac.swipeLabel({
                    content: $("<a href='#'>" + locale.zodiacNames[zodiac] + "</a>"),
                    duration: Math.min(duration*2,1000),
                    direction: direction
                });
                

                this._interface.lastShown.zodiac = zodiac; //lastShown
            }



            if (
                    (this._interface.lastShown.esotericDay === null) ||
                    (this._interface.lastShown.esotericDay != esotericDay) // smart update for esotericDay-sense info
                ) {
                this.swipeEsotericDay.swipeLabel({
                    content: $("<a href='#'>"+esotericDay+"</a>"),
                    duration: duration,
                    direction: direction
                });
                ;

                this._interface.lastShown.esotericDay = esotericDay; //lastShown
            }

            if (
                    (this._interface.lastShown.moonStage === null) ||
                    (this._interface.lastShown.moonStage != moonStage) // smart update for moonStage-sense info
                ) {
                this.swipeMoonStage.swipeLabel({
                    content: $("<a class='stage' href='#'>" + moonStage+"</a>"),
                    duration: Math.min(duration * 2, 1000),
                    direction: direction
                });

                this._interface.lastShown.moonStage = moonStage; //lastShown
            }

            this._interface.lastShown.jDays = this._engine.position; //lastShown
            
            // debug
            //console.timeEnd('someFunction timer')
            // /debug

            that._refreshCanvas();

            // trigger a callback/event
            this._trigger("change");
        },

        buildMilky: function () {
            that._milky.background = [];
            for (var i = 0; i < 2000; i++) {
                var newStar = {};
                newStar.la = Math.random() * 30 - 15;
                newStar.lo = Math.random() * 360;
                newStar.v = (Math.pow(Math.random(), 1) * 7)+0;
                newStar.bv = Math.random() * 2 - 1;
                if (newStar.bv > 0) newStar.bv *= 2.5;
                newStar.rgb = astro.bvToRgb(newStar.bv);
                newStar.opacity = astro.vToOpacity(newStar.v,10);
                that._milky.background.push(newStar);
            };
       

            $.getJSON('moonInformer/starsd.json', function (data) {

                var deltaLo = 0;
                // getting well-sorted delta-coded json and decode
                $.each(data, function (key, value) {
                    var dataStar = data[key];
                    var newStar = {};
                    newStar.lo = dataStar[0]+deltaLo;
                    deltaLo = newStar.lo;
                    newStar.la = dataStar[1];
                    newStar.lo = +deltaLo;
                    newStar.rgb = astro.bvToRgb(dataStar[3]);
                    newStar.opacity = astro.vToOpacity(dataStar[2], 10);
                    that._milky.real.push(newStar);
                });
                //console.log("jason fin", that._milky.real);
            });
        },

        _refreshCanvas: function () {
                        
            var schmitt = that.schmittDetails(that._engine.freefly.starPosition);
            var schmittBefore = that.schmittDetails(that._engine.freefly.starPosition - that._engine.freefly.starSpeedCalced/60); //used for bluring moved stars
            var schmittAfter = that.schmittDetails(that._engine.freefly.starPosition + that._engine.freefly.starSpeedCalced/60); //used for bluring moved stars
            
            //console.log(schmitt);
            
            var canvas = that._viewport.canvas;
            var yCanvas = that._viewport.yCanvas;
            var zCanvas = that._viewport.zCanvas;
            

            var ctx = canvas.getContext('2d');
            var ctxy = yCanvas.getContext('2d');
            var ctxz = zCanvas.getContext('2d');

            ctxy.clearRect(0, 0, canvas.width, canvas.height);
            ctxy.drawImage(canvas, 0, 0); //feedback here
            ctxy.globalAlpha = 0.75;

            ctxz.clearRect(0, 0, canvas.width, canvas.height);
            ctxz.drawImage(yCanvas, 0, 0); //feedback here
            //ctxz.globalCompositeOperation = "lighter";
            var moonSizeDeg = 1; //0.5 for real            
            var degMul = that._viewport.moonSize / moonSizeDeg;
            that._drawStars(ctxz, that._milky.real, schmitt, schmittBefore, schmittAfter, { degMul: degMul, sectorDegree: 360, opacity: 1, lightspeed:true });

            moonSizeDeg = 20; //0.5 for real            
            degMul = that._viewport.moonSize / moonSizeDeg;
            that._drawStars(ctxz, that._milky.real, schmitt, schmittBefore, schmittAfter, { degMul: degMul, sectorDegree: 360, opacity: 0.1, noLa:true });
            
            //ctxz.drawImage(zCanvas, 0, 0);


            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(zCanvas, 0, 0);

            //console.log(
            //    that._engine.position.toFixed(2)+"   "+
            //    that._engine.speed.toFixed(2)+" | "+
            //    that._engine.freefly.position.toFixed(2)+"   "+
            //    that._engine.freefly.speed.toFixed(2)+" | "+
            //    that._engine.freefly.blurPosition.toFixed(2)+"   "+
            //    that._engine.freefly.blurSpeed.toFixed(2)+" | "+
            //    that._engine.freefly.starPosition.toFixed(2)+"   "+
            //    that._engine.freefly.starSpeedCalced.toFixed(2)
            //    );

        },


        _drawStars: function (ctx, stars, schmitt, schmittBefore, schmittAfter, options) {
            options = $.extend({
            sectorDegree: 360, //default
            degMul: that._viewport.moonSize / .5, //default .5 is close to real moon angular size from earth
            opacity: 1,
            stroke:1,
            }, options);
            var viewportDeg = $.extend({}, that._viewport); //cloning and making degrees-verison
            $.each(viewportDeg, function (index, value) {
                viewportDeg[index] = value / options.degMul;
            });

            ctx.lineWidth=options.stroke;
            //ctx.beginPath(); //alt
            //var alpha = 1 / Math.min(20, Math.max(1, Math.abs(that._engine.freefly.starSpeedCalced))) * options.opacity; //alt
            //ctx.strokeStyle = "rgba(255,255,255,"+alpha+")"; //alt
            
            var viewSectorLeft = Math.floor((schmitt.ecliptic.lo + viewportDeg.left) / options.sectorDegree);
            var viewSectorRight = Math.floor((schmitt.ecliptic.lo + viewportDeg.right) / options.sectorDegree);
            for (var vs = viewSectorLeft; vs <= viewSectorRight; vs++) {

                for (var i = 0; i < stars.length; i++) {
                    var star = stars[i];

                    var laTravel = options.noLa?0:Math.min(1, Math.abs(15 / that._engine.freefly.starSpeedCalced));
                    
                    var moonDiff = {
                        lo: (star.lo + (vs * options.sectorDegree) - schmitt.ecliptic.lo),
                        la: (star.la - schmitt.ecliptic.la*laTravel)
                    }
                    moonDiff.x = moonDiff.lo * options.degMul;
                    moonDiff.y = moonDiff.la * options.degMul;

                    if ((moonDiff.x > that._viewport.left)
                        && (moonDiff.x < that._viewport.right)
                        && (moonDiff.y > that._viewport.top)
                        && (moonDiff.y < that._viewport.bottom)) {

                            var blurWidth = that._engine.freefly.starSpeedCalced/2 + 0.5;
                            var alpha = 27
                                * star.opacity
                                / Math.min(options.lightspeed?10:20, Math.max(1, Math.abs(that._engine.freefly.starSpeedCalced)))
                                * options.opacity; //not this.options, but from function params
                                

                            ctx.beginPath();
                            var eclipticTileShiftBefore = ((schmittBefore.ecliptic.lo <= 90) && (schmitt.ecliptic.lo > 270)) ? +360
                                                       : ((schmittBefore.ecliptic.lo > 270) && (schmitt.ecliptic.lo <= 90)) ? -360 : 0;
                            blurDiffBefore = {
                                la: (schmittBefore.ecliptic.la - schmitt.ecliptic.la)*laTravel,
                                lo: schmittBefore.ecliptic.lo - schmitt.ecliptic.lo + eclipticTileShiftBefore,
                            }
                            var eclipticTileShiftAfter = ((schmittAfter.ecliptic.lo <= 90) && (schmitt.ecliptic.lo > 270)) ? +360
                                                      : ((schmittAfter.ecliptic.lo > 270) && (schmitt.ecliptic.lo <= 90)) ? -360 : 0;
                            blurDiffAfter = {
                                la: (schmittAfter.ecliptic.la - schmitt.ecliptic.la) * laTravel,
                                lo: schmittAfter.ecliptic.lo - schmitt.ecliptic.lo + eclipticTileShiftAfter,
                            }

                            ctx.strokeStyle = "rgba(" + star.rgb.r + "," + star.rgb.g + "," + star.rgb.b + "," + alpha + ")";
                            ctx.moveTo(moonDiff.x + that._viewport.moonCenter.x - blurWidth, moonDiff.y + that._viewport.moonCenter.y); //olschool
                            ctx.lineTo(moonDiff.x + that._viewport.moonCenter.x + blurWidth, moonDiff.y + that._viewport.moonCenter.y);
                            ctx.moveTo(moonDiff.x + that._viewport.moonCenter.x + blurDiffBefore.lo * options.degMul, //plus newchool
                                       moonDiff.y + that._viewport.moonCenter.y + blurDiffBefore.la * options.degMul);
                            ctx.lineTo(moonDiff.x + that._viewport.moonCenter.x,
                                       moonDiff.y + that._viewport.moonCenter.y);
                            ctx.lineTo(moonDiff.x + that._viewport.moonCenter.x + blurDiffAfter.lo * options.degMul,
                                       moonDiff.y + that._viewport.moonCenter.y + blurDiffAfter.la * options.degMul);

                            ctx.stroke();

                    }
                }
            }
            //ctx.stroke(); //alt


        },

        // events bound via _on are removed automatically
        // revert other modifications here
        _destroy: function () {
            // remove generated elements
            this.element
                .removeClass("moon-informer")
                .enableSelection()
                .css("background-color", "transparent");
        },

        // _setOptions is called with a hash of all options that are changing
        // always refresh when changing options
        _setOptions: function () {
            // _super and _superApply handle keeping the right this-context
            this._superApply(arguments);
            this._refresh();
        },

        // _setOption is called for each individual option that is changing
        _setOption: function (key, value) {
            // prevent invalid color values
            //if (/red|green|blue/.test(key) && (value < 0 || value > 255)) {
            //    return;
            //}
            if ((key === "mode") && (value = "lofi")) { this.ballMidfi.css({"opacity": 0}); }
            this._super(key, value);
        }
    });

    // initialize with default options
    $("#my-widget1").informer();

});