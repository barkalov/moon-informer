﻿"use strict";

//may be it will go to another js file
//var json2013 = $.getJSON("informer-2013.json", function (data) {
//    $.extend(preloadedDayInfos, data);
//});


//var preloadedDayInfos = {};
//preloadedDayInfos.hundredPackStatus = {};

//function getDayInfo(jDays) {
//    var hundredPack = Math.floor(jDays / 100); //int div
//    var info = preloadedDayInfos[jDays];
//    var aviable = ((info === undefined) || (info === null)) ? false : true;

//}

$(function () {
    var astro = {
        //functions

        normalize: function (value) {
            value = value % 1;
            if (value < 0) { value += 1; }
            return value;
        },
        dateToJDays: function (date) { //original Zeno code by Stephen R. Schmitt
            var cDay = date.getDate();
            var cMonth = date.getMonth() + 1;
            var cYear = date.getFullYear();

            // calculate the Julian date at 12h UT
            var jYear = cYear - Math.floor((12 - cMonth) / 10);
            var jMonth = cMonth + 9;
            if (jMonth >= 12) { jMonth -= 12; }

            var k1 = Math.floor(365.25 * (jYear + 4712));
            var k2 = Math.floor(30.6 * jMonth + 0.5);
            var k3 = Math.floor(Math.floor((jYear / 100) + 49) * 0.75) - 38;

            var jDays = k1 + k2 + cDay + 59; // for dates in Julian calendar
            if (jDays > 2299160) {
                jDays -= k3; // for Gregorian calendar
            }
            return jDays;
        },
        jDaysToDate: function (jDays) {
            var a = jDays + 32044;
            var b = Math.floor((4 * a + 3) / 146097);
            var c = a - Math.floor(146097 * b / 4);
            var d = Math.floor((4 * c + 3) / 1461);
            var e = c - Math.floor((1461 * d) / 4);
            var m = Math.floor((5 * e + 2) / 153);

            var day = e - Math.floor((153 * m + 2) / 5) + 1;
            var month = m + 3 - (12 * Math.floor(m / 10));
            var year = 100 * b + d - 4800 + Math.floor(m / 10);
            return new Date(year, month - 1, day);
        },
        moonPhaseToMoonStageId: function (phase) {
            //var ids = 8;
            //var result =  Math.round((astro.normalize(phase+0)*8));

            var ag = phase * astro.jMoonDayLongs;

            var result = (ag < 1.84566) ? 0 :
                (ag < 5.53699) ? 1 :
                (ag < 9.22831) ? 2 :
                (ag < 12.91963) ? 3 :
                (ag < 16.61096) ? 4 :
                (ag < 20.30228) ? 5 :
                (ag < 23.99361) ? 6 :
                (ag < 27.68493) ? 7 : 0;
            return result;
        },
        bvToRgb: function (bv) {
            var result = {};
            result.r = Math.round(Math.max(0, Math.min(255, 255 - Math.max(0, -bv * 50))));
            result.b = Math.round(Math.max(0, Math.min(255, 255 - Math.max(0, bv * 50))));
            result.g = Math.round((result.r + result.b) / 2);
            return result;
        },
        vToOpacity: function (v, mulOnFive) {
            if (mulOnFive === undefined) mulOnFive = 100;
            return Math.pow(mulOnFive, -v / 5);
        },


        // constants
        jMoonDayLongs: 29.530588853,
        jNewShift: -2451550.1,
    };
    var locale = {
        monthNames: ["января", "февраля", "матра", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
        weekDays: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
        moonStages: ["новолуние", "растущий месяц", "I четверть", "растущая луна", "полнолуние", "убывающая луна", "III четверть", "спадающий месяц"],
        zodiacNames: ["Овен", "Телец", "Близнецы", "Рак", "Лев", "Дева", "Весы", "Скорпион", "Стрелец", "Козерог", "Водолей", "Рыбы"],
        detailedTexts: [
            "1 Сны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные события осуществятся <b>в течение 10 суток</b>. Однако короткие сны сегодня всъерьёз воспринимать не стоит.",
            "2 Сборная России <b>по хоккею</b> обыграла канадцев в матче за бронзовые медали молодежного (до 20 лет) <b>чемпионата мира</b>, проходившего в Уфе. Об этом сообщается на сайте ИИХФ.",
            "3 Игра закончилась <b>со счетом 6:5</b>, победную для россиян шайбу Валерий Ничушкин забросил <b>на второй минуте овертайма</b>. Также в составе сборной России отличились Александр Хохлачев, Наиль Якупов (дважды), Кирилл Дьяков и Евгений Мозер.",
            "4 Сборная Канады осталась без медалей на молодежном ЧМ <b>впервые за 15 турниров</b>. Глава ФХР Владислав Третьяк заявил после окончания матча за третье место, что <b>в целом доволен</b> результатом россиян.",
            "5 Я никакого провала не вижу. <b>Молодежный хоккей непредсказуем</b>, психология у них еще неустойчивая. Они еще не умеют выигрывать и <b>не умеют проигрывать</b>, сказал Третьяк.",
            "6 В полуфинале россияне <b>уступили сборной Швеции</b> в серии буллитов. После этого, в СМИ появились слухи о нарушениях дисциплины со стороны хоккеистов, которые якобы <b>слишком много времени проводили вне гостиницы</b>.",
            "7 Сны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные события осуществятся <b>в течение 10 суток</b>. Однако короткие сны сегодня всъерьёз воспринимать не стоит. Сны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные события осуществятся <b>в течение 10 суток</b>. Однако короткие сны сегодня всъерьёз воспринимать не стоит.",
            "8 Сны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные события осуществятся <b>в течение 10 суток</b>.",
            "9 Сны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные",
            "10 Сны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные события осуществятся <b>в течение 10 суток</b>. Однако короткие сны сегодня всъерьёз воспринимать не стоит. Сны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные соб.",
            "11 Сны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные события осуществятся <b>в течение 10 суток</b>. Однако короткие сны сегодня всъерьёз воспринимать не стоит. Сны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные события осуществятся <b>в течение 10 суток</b>. Однако коит.",
            "12 Сны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные события осуществятся <b>в течение 10 суток</b>. Однако короткие сны сегодня всъерьёз воспринимать не стоит. Сны сбываются <b>с очСны сбываются <b>с очень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные события осуществятся <b>в течение 10 суток</b>. Однако короткие сны сегодня всъерьёз воспринимать не стоит. Сны сбываются <b>с очень большой вероятностью</b>.  ень большой вероятностью</b>. Обратите внимание на&nbsp;то, что увиденные события осуществятся <b>в течение 10 суток</b>. Однако короткие сны сегодня всъерьёз воспринимать не стоит.",

        ]

    };


    // the widget definition, where "moon" is the namespace,
    // "informer" the widget name
    $.widget("moon.informer", {
        // default options
        options: {
            sprites: {
                ballWidth: 190, //px mouse = 360 degrees engine - 1/28.. of jDay
                ballHeight: 145,
                ballFramesCountMidHifi: 90,
                ballFramesCountHifiAlpha: 30,
                ballFramesCountLofi: 30,
                ballFramesUrlMidHifi: "moonInformer/img/atlas-190x145x90.jpg",
                ballFramesUrlLofi: "moonInformer/img/atlas-190x145x30.jpg", //lofi
                ballFramesUrlHifiAlpha: "moonInformer/img/atlas-alpha-190x145x90.png"
            },

            mode: "lofi",

            // callbacks
            change: null,
        },

        _engine: {
            position: 2456294, //jDays
            speed: 0, //jDaysPerSec
            freefly: {
                interval: null, //used for setinterval canceling
                position: 2456294, //smooth, not actual version
                speed: 0, //smooth, not actual version
                blurPosition: 2456294, //even more smoother
                blurSpeed: 0, //even more smoother just interapolates freefly.speed
                blurCalced: 0, //even more smoother calculates delta from blurPositon
                starPosition: 2456294, //even much more smoother
                starSpeed: 0, //even much more smoother
                starSpeedCalced: 0, //even much more smoother 


            }
        },
        _interface: {
            lastShown: { //used for cute refreshing when changes only
                jDays: null, //used for direction detection
                gregDate: null,
                moonStage: null,
                zodiac: null,
                esotericDay: null,
            }
        },

        _inputState: {
            lastMoveTimeStamp: null, //Date.now()
            lastMovePosition: null, //just x as number
            pressed: false,
            pressedPosition: null, //just x as number
            pressedDegrees: null //just x as number
        },

        _preloadStates: {
            lofi: false,
            midfi: false,
            hifi: false,
            milki: false,
        },

        _viewport: {
            moonCenter: { x: 140, y: 70 },
            moonSize: 190,
            width: 660,
            height: 130,
            top: -70 + 15, //diff from moon position
            bottom: 130 - 70 + 15,
            left: -140,
            right: 660 - 140,
            canvas: $(".moon-canvas")[0],
            yCanvas: $('<canvas width="660" height="145"/>')[0],
            zCanvas: $('<canvas width="660" height="145"/>')[0],
            moonCanvas: null,
            moonImage: null,
            moonAlphaImage: null,
        },

        _milky: {
            background: [], //not used now
            real: []
        },
        schmittDetails: function (jDays) {

            var result = {
                distance: null,
                ecliptic: {}, // {la,lo}
                zodiac: null
            };
            // calculate moon's age in days
            var mPhase = this.jDaysToMoonPhase(jDays);

            var mPhaseRad = mPhase * 2 * Math.PI; // Convert phase to radians

            // calculate moon's distance
            var distPhase = 2 * Math.PI * astro.normalize((jDays - 2451562.2) / 27.55454988);
            var dist = 60.4 - 3.3 * Math.cos(distPhase) - 0.6 * Math.cos(2 * mPhaseRad - distPhase) - 0.5 * Math.cos(2 * mPhaseRad);
            result.distance = dist; //->

            // calculate moon's ecliptic latitude
            var nPhaseRad = 2 * Math.PI * astro.normalize((jDays - 2451565.2) / 27.212220817);
            var eclipticLa = 5.1 * Math.sin(nPhaseRad);
            result.ecliptic.la = eclipticLa; //->

            // calculate moon's ecliptic longitude
            var rPhase = astro.normalize((jDays - 2451555.8) / 27.321582241);
            var eclipticLo = 360 * rPhase + 6.3 * Math.sin(distPhase) + 1.3 * Math.sin(2 * mPhaseRad - distPhase) + 0.7 * Math.sin(2 * mPhaseRad);
            eclipticLo = (eclipticLo + 360) % 360;
            result.ecliptic.lo = eclipticLo; //->

            // hmm seems to be wrong
            // upd: seems to be perfectly right, as it is in 2000 epoch,
            // but sorry, mr. Schmitt, scientific astronomy sucks,
            // Ancient Greece astrology rules.
            //
            //result.zodiac = (eclipticLo < 33.18) ? 11
            //    : (eclipticLo < 51.16) ? 0
            //    : (eclipticLo < 93.44) ? 1
            //    : (eclipticLo < 119.48) ? 2
            //    : (eclipticLo < 135.30) ? 3
            //    : (eclipticLo < 173.34) ? 4
            //    : (eclipticLo < 224.17) ? 5
            //    : (eclipticLo < 242.57) ? 6
            //    : (eclipticLo < 271.26) ? 7
            //    : (eclipticLo < 302.49) ? 8
            //    : (eclipticLo < 311.72) ? 9
            //    : (eclipticLo < 348.58) ? 10
            //    : 11;

            result.zodiac = (Math.floor(eclipticLo * 12 / 360) % 12);
            return result;
        },
        jDaysToEsotericDay: function (jDays) {
            //dumb placeholder, not a real algorithm
            var result = Math.round(this.jDaysToMoonPhase(jDays) * 28) + 1;
            return result;

        },
        jDaysToMoonPhase: function (jDays) {
            if ((jDays === undefined) || (jDays === null)) { jDays = this._engine.position; } //default
            var unnormalizedPhase = (jDays + astro.jNewShift) / astro.jMoonDayLongs;
            return astro.normalize(unnormalizedPhase);
        },
        moonPhaseToChops: function(phase,chopsCount,soft) {            
            var floatChop =
                (
                ((phase * chopsCount) % chopsCount)
                + chopsCount)
                % chopsCount;
            var atlasChopLo,
                atlasChopHi,
                opacityHi,
                atlasChop;
            
            if (soft) {
                atlasChopLo = Math.floor(floatChop);
                atlasChopHi = Math.ceil(floatChop) % chopsCount;            
                opacityHi = floatChop - atlasChopLo;
            } else {
                atlasChop = Math.round(floatChop) % chopsCount;
            }
            return { atlasChop: atlasChop, atlasChopLo: atlasChopLo, atlasChopHi: atlasChopHi, opacityHi: opacityHi, floatChop: floatChop };

        },


        jDaysToMoonAtlasFrames: function (jDays, mode) {
            if ((jDays === undefined) || (jDays === null)) { jDays = this._engine.position; } //default
            if ((mode === undefined) || (mode === null)) { mode = this.options._mode; } //default
            var phase = this.jDaysToMoonPhase(jDays);

            switch (mode) {                
                case "lofi":

                    var framesCount = this.options.sprites.ballFramesCountLofi;
                    var chops = this.moonPhaseToChops(phase, framesCount, false);
                    return {
                        atlasFrame: chops.atlasChop,
                        atlasPx: chops.atlasChop * this.options.sprites.ballHeight,
    
                    };
                    break; //yepyep

                case "midfi":
                    var framesCount = this.options.sprites.ballFramesCountMidHifi;
                    
                    var chops = this.moonPhaseToChops(phase, framesCount, true); //rich mode - soft=true

                    return {
                        atlasFrameLo: chops.atlasChopLo,
                        atlasFrameHi: chops.atlasChopHi,
                        atlasPxLo: chops.atlasChopLo * this.options.sprites.ballHeight,
                        atlasPxHi: chops.atlasChopHi * this.options.sprites.ballHeight,
                        opacityHi: chops.opacityHi,
                    };                
                case "hifi":
                    var framesCount = this.options.sprites.ballFramesCountMidHifi;
                    var chops = this.moonPhaseToChops(phase, framesCount, false);
                    return {
                        atlasFrame: chops.atlasChop,
                        atlasPx: chops.atlasChop * this.options.sprites.ballHeight,
    
                    };
                    break; //yepyep
            }

            
        },

        // the constructor
        _create: function () {
            var that = this;
            this.element
                // add a class for theming
                .addClass("moon-informer");
            // prevent double click to select text
            //.disableSelection();            
           
            this.ballLofi = $(".moon-ball-lofi", this.element);
            this.ballMidfi = $(".moon-ball-midfi", this.element);
            $(document).on("mousemove", function (e) { that._onMouseMove(e); });
            this.ballLofi.on("mousedown", function (e) { that._onMouseDown(e); });
            this.element.on("mouseup", function (e) { that._onMouseUp(e); });
            $(document).on("mouseup", function (e) { that._onMouseUp(e); });

            this.canvasHifi = $(this._viewport.canvas);
            this.swipeDate = $(".moon-swipe-date", this.element);
            this.swipeDate.on("click", "a", function () { this._datepickerShow(); });
            this.swipeWeekDay = $(".moon-swipe-weekDay", this.element);
            this.swipeMoonStage = $(".moon-swipe-moonStage", this.element);
            this.swipeEsotericDay = $(".moon-swipe-esotericDay", this.element);
            this.swipeZodiac = $(".moon-swipe-zodiac", this.element);
            this.swipeDetailedText = $(".moon-swipe-detailedText", this.element);

            if (this.options.mode === "lofi") {
                this.ballLofi.css({ "background-image": "url(" + this.options.sprites.ballFramesUrlLofi + ")" }); //revert to one-image mode
                this.canvasHifi.css({ display: "none" });
                this._preloadLofi(function () {
                    that.options._mode = "lofi";
                    that._refresh();
                });
            }
            if (this.options.mode === "midfi") {
                this.ballLofi.css({ "background-image": "url(" + this.options.sprites.ballFramesUrlMidHifi + ")" });
                this.ballMidfi.css({ "background-image": "url(" + this.options.sprites.ballFramesUrlMidHifi + ")" });
                this.canvasHifi.css({ display: "none" });
                this._preloadMidfi(function () {
                    that.options._mode = "midfi";
                    that._refresh();
                });
            }
            if (this.options.mode === "hifi") {
                this.ballLofi.css({ "background-image": "none" });
                this.ballMidfi.css({ display: "none" });
                this.canvasHifi.css({ display: "block" });
                this._preloadHifi(function () {
                    that.options._mode = "hifi";
                    that._buildMilky();
                    that._refresh();
                });
            }

            this.options._mode = "lofi"; //add a backing field (switches after preloading, used in logic, while .mode is for seting from outer space)

            this.swipeDate.swipeLabel();
            this.swipeWeekDay.swipeLabel();
            this.swipeMoonStage.swipeLabel();
            this.swipeEsotericDay.swipeLabel();
            this.swipeZodiac.swipeLabel();
            this.swipeDetailedText.swipeLabel({ "contentWidthMode": "manual", "contentWidth": 230, "charsAt100": 170 });
            this._refresh();
        },

        _datepickerShow: function () {
            $("#hiddenDatepicker").datepicker();  //TODO: remertertert!!
            $("#hiddenDatepicker").datepicker("show");  //TODO: remertertert!!
        },
        _onMouseMove: function (e) {
            if (this._inputState.pressed === true) {
                var msecNow = Date.now();
                if ((this._inputState.lastMoveTimeStamp !== null) && (this._inputState.lastMovePosition)) {
                    var deltaTime = Math.max(msecNow - this._inputState.lastMoveTimeStamp, 10); //mouse.min to flatten peaks with anomaly shorten periods, produsing huge velocity
                    this._engine.speed = ((this._inputState.lastMovePosition - e.clientX) * astro.jMoonDayLongs / (this.options.sprites.ballWidth * 2)) * 1000 / (deltaTime);
                }
                this._inputState.lastMoveTimeStamp = msecNow;
                this._inputState.lastMovePosition = e.clientX;


                this._engine.position = ((this._inputState.pressedPosition - e.clientX) * astro.jMoonDayLongs / (this.options.sprites.ballWidth * 2)) + this._inputState.pressedJDays;
            }
        },

        _onMouseDown: function (e) {
            var that = this;

            // temp on
            if (this.options.mode==="lofi") this._setOption("mode", "hifi");


            this.element.disableSelection();
            this._engine.speed = 0; //stopping on moveless click
            this._engine.freefly.speed = 0; //stopping on moveless click
            //inputState.lastMoveTimeStamp = Date.now();
            //inputState.lastMovePosition = e.clientX;
            this._inputState.pressedPosition = e.clientX;
            this._inputState.pressedJDays = this._engine.position;
            this._inputState.pressed = true;

            if (this._engine.freefly.interval === null) {
                this._engine.freefly.interval = setInterval(function () { that._freefly(); }, 1000 / 60); //check for once
                this._freefly();
            }
        },


        _freefly: function () {

            if (this._inputState.pressed === false) {

                if ((Math.abs(this._engine.freefly.starSpeedCalced) < 0.01) &&
                   (Math.abs(this._engine.freefly.blurSpeedCalced) < 0.01) &&
                   (Math.abs(this._engine.freefly.speed) < 0.01) &&
                   (Math.abs(this._engine.speed) < 0.01)) {
                    //clearInterval(this._engine.freefly.interval);
                    //this._engine.freefly.interval = null; //end interval loop
                    this._engine.freefly.starSpeedCalced = 0;
                    this._engine.freefly.blurSpeedCalced = 0;
                    this._engine.freefly.speed = 0;
                    this._engine.speed = 0;
                    return;
                }

                this._engine.freefly.speed *= 0.99; //slowing here

                if (Math.abs(this._engine.freefly.speed) < 10) { //slowing harder when slower 260 (60)
                    var speedSign = this._engine.freefly.speed ? this._engine.freefly.speed < 0 ? -1 : 1 : 0;
                    var speedMinus = 0.1;
                    var extraFriction = (Math.abs(this._engine.freefly.speed) < speedMinus) ? this._engine.freefly.speed : (speedSign * speedMinus);
                    this._engine.freefly.speed -= extraFriction;
                }
                if (Math.abs(this._engine.freefly.speed) < 0.2) { this._engine.freefly.speed = 0; } //stop when slower +-2
                this._engine.position = this._engine.position + (this._engine.freefly.speed / 60); // /
            } else { //pressed==true                
                this._engine.freefly.speed = this._engine.freefly.speed * 0.75 + this._engine.speed * 0.25;
            }
            this._engine.freefly.position = this._engine.freefly.position * 0.75 + this._engine.position * 0.25;

            var oldBlurPosition = this._engine.freefly.blurPosition;
            this._engine.freefly.blurPosition = this._engine.freefly.blurPosition * 0.75 + this._engine.freefly.position * 0.25;
            this._engine.freefly.blurSpeed = this._engine.freefly.blurSpeed * 0.75 + this._engine.freefly.speed * 0.25;
            this._engine.freefly.blurSpeedCalced = (this._engine.freefly.blurPosition - oldBlurPosition) * 60;


            var oldStarPosition = this._engine.freefly.starPosition;
            this._engine.freefly.starPosition = this._engine.freefly.starPosition * 0.80 + this._engine.freefly.blurPosition * 0.2;
            this._engine.freefly.starSpeedCalced = (this._engine.freefly.starPosition - oldStarPosition) * 60;




            this._refresh();
            this._engine.speed *= 0.75; //slowing for times where no mousemoves hit

        },

        _onMouseUp: function (e) {
            this.element.enableSelection();
            this._inputState.pressed = false;
        },



        // called when created, and later when changing options
        _refresh: function () {

            var af = this.jDaysToMoonAtlasFrames(this._engine.freefly.position);            
            switch (this.options._mode) {
                case "lofi":
                    this.ballLofi.css({
                        "background-position": "left " + -af.atlasPx + "px"
                    });
                    break;
                case "midfi":
                    this.ballLofi.css({
                        "background-position": "left " + -af.atlasPxLo + "px",
                    });
                    this.ballMidfi.css({
                        "background-position": "left " + -af.atlasPxHi + "px",
                        "opacity": af.opacityHi
                    });
                    break;
                case "hifi":                    
                    this._refreshCanvas(af);
                    break;
            }
            var detailedTextOpacity = Math.pow(1 - Math.min(Math.max(Math.abs(this._engine.freefly.blurSpeed) / 300, 0), 1), 20); //0to300 -> 0 to 1, then pow2                        
            this.swipeDetailedText.css({ opacity: detailedTextOpacity * 0.6 + 0.2 });
            this.swipeDetailedText.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed });

            //            this.swipeZodiac.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed });

            this.swipeWeekDay.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed });
            this.swipeEsotericDay.swipeLabel({ constantBlur: this._engine.freefly.blurSpeed });

            var gregDate = astro.jDaysToDate(this._engine.position);
            var moonStage = locale.moonStages[
                astro.moonPhaseToMoonStageId(
                    this.jDaysToMoonPhase(
                        this._engine.position))];
            var esotericDay = this.jDaysToEsotericDay(this._engine.position);
            var zodiac = this.schmittDetails(this._engine.position).zodiac;


            var direction = (this._interface.lastShown.jDays === null) ? "left" : (this._engine.position > this._interface.lastShown.jDays) ? "left" : "right";
            var duration = this._inputState.pressed ? Math.min(1600 / Math.abs(this._engine.freefly.speed), 200) : Math.min(1600 / Math.abs(this._engine.freefly.speed), 1000);

            // debug
            //console.time('someFunction timer');
            // /debug
            //console.log(duration);

            if (
                    (this._interface.lastShown.gregDate === null) ||
                    (this._interface.lastShown.gregDate.getTime() !== gregDate.getTime()) // smart update for date-sense info
                ) {
                this.swipeDate.swipeLabel({
                    content: $("<a class='date' href='#'>" + gregDate.getDate() + " " + locale.monthNames[gregDate.getMonth()] + " " + gregDate.getFullYear() + "</a>"),
                    duration: duration,
                    direction: direction
                });
                this.swipeWeekDay.swipeLabel({
                    content: $("<span>").text(locale.weekDays[gregDate.getDay()]),
                    duration: duration,
                    direction: direction
                });
                this.swipeDetailedText.swipeLabel({
                    content: $("<span>").html(locale.detailedTexts[Math.floor(this._engine.position) % 12]),
                    duration: duration * 2.5,
                    direction: direction
                });


                this._interface.lastShown.gregDate = gregDate; //lastShown
            }

            if (
                    (this._interface.lastShown.zodiac === null) ||
                    (this._interface.lastShown.zodiac !== zodiac) // smart update for zodiac-sense info
                ) {
                this.swipeZodiac.swipeLabel({
                    content: $("<a href='#'>" + locale.zodiacNames[zodiac] + "</a>"),
                    duration: Math.min(duration * 2, 1000),
                    direction: direction
                });


                this._interface.lastShown.zodiac = zodiac; //lastShown
            }



            if (
                    (this._interface.lastShown.esotericDay === null) ||
                    (this._interface.lastShown.esotericDay !== esotericDay) // smart update for esotericDay-sense info
                ) {
                this.swipeEsotericDay.swipeLabel({
                    content: $("<a href='#'>" + esotericDay + "</a>"),
                    duration: duration,
                    direction: direction
                });
                

                this._interface.lastShown.esotericDay = esotericDay; //lastShown
            }

            if (
                    (this._interface.lastShown.moonStage === null) ||
                    (this._interface.lastShown.moonStage !== moonStage) // smart update for moonStage-sense info
                ) {
                this.swipeMoonStage.swipeLabel({
                    content: $("<a class='stage' href='#'>" + moonStage + "</a>"),
                    duration: Math.min(duration * 2, 1000),
                    direction: direction
                });

                this._interface.lastShown.moonStage = moonStage; //lastShown
            }

            this._interface.lastShown.jDays = this._engine.position; //lastShown

            // debug
            //console.timeEnd('someFunction timer')
            // /debug


            // trigger a callback/event
            this._trigger("change");
        },

        _buildMilky: function () {
            if (this._preloadStates.milki) return;
            var that = this;
            // not used now
            //this._milky.background = [];
            //for (var i = 0; i < 2000; i++) {
            //    var newStar = {};
            //    newStar.la = Math.random() * 30 - 15;
            //    newStar.lo = Math.random() * 360;
            //    newStar.v = (Math.pow(Math.random(), 1) * 7) + 0;
            //    newStar.bv = Math.random() * 2 - 1;
            //    if (newStar.bv > 0) newStar.bv *= 2.5;
            //    newStar.rgb = astro.bvToRgb(newStar.bv);
            //    newStar.opacity = astro.vToOpacity(newStar.v, 10);
            //    this._milky.background.push(newStar);
            //};
            

            $.getJSON('moonInformer/starsd.json', function (data) {
                that._preloadStates.milki = true;
                that._milky.real = [];

                var deltaLo = 0;
                // getting well-sorted delta-coded json and decode
                $.each(data, function (key, value) {
                    var dataStar = data[key];
                    var newStar = {};
                    newStar.lo = dataStar[0] + deltaLo;
                    deltaLo = newStar.lo;
                    newStar.la = dataStar[1];
                    newStar.lo = +deltaLo;
                    newStar.rgb = astro.bvToRgb(dataStar[3]);
                    newStar.opacity = astro.vToOpacity(dataStar[2], 10);
                    that._milky.real.push(newStar);
                });
            });
        },

        _refreshCanvas: function (atlasFrames) {

            var schmitt = this.schmittDetails(this._engine.freefly.starPosition);
            var schmittBefore = this.schmittDetails(this._engine.freefly.starPosition - this._engine.freefly.starSpeedCalced / 60); //used for bluring moved stars
            var schmittAfter = this.schmittDetails(this._engine.freefly.starPosition + this._engine.freefly.starSpeedCalced / 60); //used for bluring moved stars


            var canvas = this._viewport.canvas;
            var yCanvas = this._viewport.yCanvas;
            var zCanvas = this._viewport.zCanvas;


            var ctx = canvas.getContext('2d');
            var ctxy = yCanvas.getContext('2d');
            var ctxz = zCanvas.getContext('2d');

            ctxy.clearRect(0, 0, canvas.width, canvas.height);
            ctxy.drawImage(canvas, 0, 0); //feedback here
            ctxy.globalAlpha = 0.75;

            ctxz.clearRect(0, 0, canvas.width, canvas.height);
            ctxz.drawImage(yCanvas, 0, 0); //feedback here
            //ctxz.globalCompositeOperation = "lighter";
            var moonSizeDeg = 1; //0.5 for real            
            var degMul = this._viewport.moonSize / moonSizeDeg;
            this._drawStars(ctxz, this._milky.real, schmitt, schmittBefore, schmittAfter, { degMul: degMul, sectorDegree: 360, opacity: 1, lightspeed: true });

            moonSizeDeg = 20; //0.5 for real            
            degMul = this._viewport.moonSize / moonSizeDeg;
            this._drawStars(ctxz, this._milky.real, schmitt, schmittBefore, schmittAfter, { degMul: degMul, sectorDegree: 360, opacity: 0.1, noLa: true });

            //ctxz.drawImage(zCanvas, 0, 0);
            this._drawMoon(ctxz, atlasFrames);

            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(zCanvas, 0, 0);

            //console.log(
            //    this._engine.position.toFixed(2)+"   "+
            //    this._engine.speed.toFixed(2)+" | "+
            //    this._engine.freefly.position.toFixed(2)+"   "+
            //    this._engine.freefly.speed.toFixed(2)+" | "+
            //    this._engine.freefly.blurPosition.toFixed(2)+"   "+
            //    this._engine.freefly.blurSpeed.toFixed(2)+" | "+
            //    this._engine.freefly.starPosition.toFixed(2)+"   "+
            //    this._engine.freefly.starSpeedCalced.toFixed(2)
            //    );

        },


        _drawStars: function (ctx, stars, schmitt, schmittBefore, schmittAfter, options) {
            options = $.extend({
                sectorDegree: 360, //default
                degMul: this._viewport.moonSize / 0.5, //default .5 is close to real moon angular size from earth
                opacity: 1,
                stroke: 1,
            }, options);
            var viewportDeg = $.extend({}, this._viewport); //cloning and making degrees-verison
            $.each(viewportDeg, function (index, value) {
                viewportDeg[index] = value / options.degMul;
            });

            ctx.lineWidth = options.stroke;
            //ctx.beginPath(); //alt
            //var alpha = 1 / Math.min(20, Math.max(1, Math.abs(this._engine.freefly.starSpeedCalced))) * options.opacity; //alt
            //ctx.strokeStyle = "rgba(255,255,255,"+alpha+")"; //alt

            var viewSectorLeft = Math.floor((schmitt.ecliptic.lo + viewportDeg.left) / options.sectorDegree);
            var viewSectorRight = Math.floor((schmitt.ecliptic.lo + viewportDeg.right) / options.sectorDegree);
            for (var vs = viewSectorLeft; vs <= viewSectorRight; vs++) {

                for (var i = 0; i < stars.length; i++) {
                    var star = stars[i];

                    var laTravel = options.noLa ? 0 : Math.min(1, Math.abs(15 / this._engine.freefly.starSpeedCalced));

                    var moonDiff = {
                        lo: (star.lo + (vs * options.sectorDegree) - schmitt.ecliptic.lo),
                        la: (star.la - schmitt.ecliptic.la * laTravel)
                    };
                    moonDiff.x = moonDiff.lo * options.degMul;
                    moonDiff.y = moonDiff.la * options.degMul;

                    if ((moonDiff.x > this._viewport.left)
                        && (moonDiff.x < this._viewport.right)
                        && (moonDiff.y > this._viewport.top)
                        && (moonDiff.y < this._viewport.bottom)) {

                        var blurWidth = this._engine.freefly.starSpeedCalced / 2 + 0.5;
                        var alpha = 27
                            * star.opacity
                            / Math.min(options.lightspeed ? 10 : 20, Math.max(1, Math.abs(this._engine.freefly.starSpeedCalced)))
                            * options.opacity; //not this.options, but from function params


                        ctx.beginPath();
                        var eclipticTileShiftBefore = ((schmittBefore.ecliptic.lo <= 90) && (schmitt.ecliptic.lo > 270)) ? +360
                                                   : ((schmittBefore.ecliptic.lo > 270) && (schmitt.ecliptic.lo <= 90)) ? -360 : 0;
                        var blurDiffBefore = {
                            la: (schmittBefore.ecliptic.la - schmitt.ecliptic.la) * laTravel,
                            lo: schmittBefore.ecliptic.lo - schmitt.ecliptic.lo + eclipticTileShiftBefore,
                        };
                        var eclipticTileShiftAfter = ((schmittAfter.ecliptic.lo <= 90) && (schmitt.ecliptic.lo > 270)) ? +360
                                                  : ((schmittAfter.ecliptic.lo > 270) && (schmitt.ecliptic.lo <= 90)) ? -360 : 0;
                        var blurDiffAfter = {
                            la: (schmittAfter.ecliptic.la - schmitt.ecliptic.la) * laTravel,
                            lo: schmittAfter.ecliptic.lo - schmitt.ecliptic.lo + eclipticTileShiftAfter,
                        };

                        ctx.strokeStyle = "rgba(" + star.rgb.r + "," + star.rgb.g + "," + star.rgb.b + "," + alpha + ")";
                        ctx.moveTo(moonDiff.x + this._viewport.moonCenter.x - blurWidth, moonDiff.y + this._viewport.moonCenter.y); //olschool
                        ctx.lineTo(moonDiff.x + this._viewport.moonCenter.x + blurWidth, moonDiff.y + this._viewport.moonCenter.y);
                        ctx.moveTo(moonDiff.x + this._viewport.moonCenter.x + blurDiffBefore.lo * options.degMul, //plus newchool
                                   moonDiff.y + this._viewport.moonCenter.y + blurDiffBefore.la * options.degMul);
                        ctx.lineTo(moonDiff.x + this._viewport.moonCenter.x,
                                   moonDiff.y + this._viewport.moonCenter.y);
                        ctx.lineTo(moonDiff.x + this._viewport.moonCenter.x + blurDiffAfter.lo * options.degMul,
                                   moonDiff.y + this._viewport.moonCenter.y + blurDiffAfter.la * options.degMul);

                        ctx.stroke();

                    }
                }
            }
            //ctx.stroke(); //alt


        },

        _buildMoonCanvas: function () {
            var sprites = this.options.sprites;
            var moonDoubleCanvas = $("<canvas width=" + sprites.ballWidth * 2 + " height=" + (sprites.ballHeight * sprites.ballFramesCountMidHifi) + "/>")[0];
            var ctxdmc = moonDoubleCanvas.getContext('2d');
            ctxdmc.drawImage(this._viewport.moonImage,
                0, 0,
                sprites.ballWidth, sprites.ballHeight * sprites.ballFramesCountMidHifi);
            ctxdmc.drawImage(this._viewport.moonAlphaImage,
                sprites.ballWidth, 0,
                sprites.ballWidth, sprites.ballHeight * sprites.ballFramesCountMidHifi);
            var dmcImageData = ctxdmc.getImageData(0, 0, moonDoubleCanvas.width, moonDoubleCanvas.height);
            
            for (var x = 0; x < moonDoubleCanvas.width/2; x++) {
                for (var y = 0; y < moonDoubleCanvas.height; y++) {
                    dmcImageData.data[(x + (y * moonDoubleCanvas.width)) * 4+3] = dmcImageData.data[(x+190 + (y * moonDoubleCanvas.width)) * 4]

                    //console.log(dmcImageData.data[x * y * 4 + 3]);
                    //dmcImageData.data[x * y * 4 + 3] = dmcImageData.data[(x + 190) * y * 4]
                    //dmcImageData.data[((x+1) * (((y+1)*moonDoubleCanvas.width)-1) * 4)] = 128;
                    //dmcImageData.data[((x + 1) * (((y + 1) * moonDoubleCanvas.width) - 1) * 4)] = 128;
                    
                }
            }

            

            console.log(dmcImageData);

            ctxdmc.putImageData(dmcImageData, 0, 0);
            this._viewport.moonCanvas=$("<canvas width=" + sprites.ballWidth + " height=" + (sprites.ballHeight * sprites.ballFramesCountMidHifi) + "/>")[0];

            var ctxmc = this._viewport.moonCanvas.getContext('2d');
            ctxmc.drawImage(moonDoubleCanvas, 0, 0);

        },

        _drawMoon: function (ctx, atlasFrames) {
            var moonCanvas = this._viewport.moonCanvas;
            if (moonCanvas == null ) return; // case, such a coldstart with hifi mode preset and image is still loading
            ctx.drawImage(this._viewport.moonCanvas,
                0, atlasFrames.atlasPx,
                this.options.sprites.ballWidth, this.options.sprites.ballHeight,
                0, 0,
                this.options.sprites.ballWidth, this.options.sprites.ballHeight);
        },

        _preloadMuchData: function () {
            //fires after _preloadMidfi loaded
        },
        _preloadLofi: function (callback) {
            var that = this;
            if (this._preloadStates.lofi) {
                if (callback) callback();
            } else {
                $('<img>').attr({ src: this.options.sprites.ballFramesUrlLofi }).load(function () {
                    that._preloadStates.lofi = true;
                    if (callback) callback();
                });
            }

        },
        _preloadMidfi: function (callback) {
            var that = this;
            if (this._preloadStates.midfi) {                
                if (callback) callback();
            } else {
                $('<img>').attr({ src: this.options.sprites.ballFramesUrlMidHifi }).load(function () {
                    that._preloadStates.midfi = true;
                    if (callback) callback();
                });
            }
            
        },
        _preloadHifi: function (callback) {            
            var that = this;
            function _checkload() {
                if ((that._preloadStates.hifi) && (that._preloadStates.midfi)) {                    
                    that._buildMoonCanvas();
                    if (callback) callback();                    
                    return true;
                }
                return false;
            }            
            

            if (_checkload(callback)) {
            } else {                
                that._viewport.moonImage = $('<img>').attr({ src: this.options.sprites.ballFramesUrlMidHifi }).load(function () {
                    that._preloadStates.midfi = true;
                    _checkload(callback);
                })[0]; //attention [0] moonimage is not a jquery, but a dom object
                that._viewport.moonAlphaImage = $('<img>').attr({ src: this.options.sprites.ballFramesUrlHifiAlpha }).load(function () {
                    that._preloadStates.hifi = true;
                    _checkload(callback);
                })[0];
            }

        },
        // events bound via _on are removed automatically
        // revert other modifications here
        _destroy: function () {
            // remove generated elements
            this.element
                .removeClass("moon-informer")
                .enableSelection()
                .css("background-color", "transparent");
        },

        // _setOptions is called with a hash of all options this are changing
        // always refresh when changing options
        _setOptions: function () {
            // _super and _superApply handle keeping the right this-context
            this._superApply(arguments);
            //this._refresh();
        },

        // _setOption is called for each individual option this is changing
        _setOption: function (key, value) {
            var that = this;
            // prevent invalid color values
            //if (/red|green|blue/.test(key) && (value < 0 || value > 255)) {
            //    return;
            //}
            if (key === "mode") {


                switch (value) {
                    case "lofi":
                        this._preloadLofi(function () {
                            that.ballLofi.css({
                                "background-image": "url(" + that.options.sprites.ballFramesUrlLofi + ")", //revert to one-image mode
                            });
                            that.ballMidfi.css({
                                display: "none",
                            });
                            that.canvasHifi.css({
                                display: "none",
                            });
                            that.options._mode = "lofi";
                            that._refresh();
                        });
                        break;
                    case "midfi": //background-images will be set on _refresh
                        this._preloadMidfi(function () {
                            that.ballMidfi.css({
                                display: "block",
                            });
                            that.canvasHifi.css({
                                display: "none",
                            });
                            that.ballLofi.css({
                                "background-image": "url(" + that.options.sprites.ballFramesUrlMidHifi + ")", //revert to one-image mode
                            });
                            that.ballMidfi.css({
                                "background-image": "url(" + that.options.sprites.ballFramesUrlMidHifi + ")", //revert to one-image mode
                            });
                            that.options._mode = "midfi";
                            that._refresh();
                        });
                        break;
                    case "hifi":                        
                        this._preloadHifi(function () {
                            that.ballMidfi.css({
                                display: "none",
                            });
                            that.canvasHifi.css({
                                display: "block",
                            });
                            that.ballLofi.css({
                                "background-image": "none", //transparent div now only used to track mouse events
                            });
                            that._buildMilky();
                            that.options._mode = "hifi";                            
                            that._refresh();
                        });
                        break;
                }
            }
            this._super(key, value);
        }
    });

    // initialize with default options
    $("#my-widget1").informer();

});