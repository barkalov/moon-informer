(function ($) {

    var methods = {
        init: function (optionsExplit) {

            // Create some defaults, extending them with any options that were provided
            globalOptions = $.extend({
                "duration": 100,
                "direction": "left",
                "content": null,
                "contentWidthMode": "inline", // or "self" or "manual"
                "contentWidth": null, // may be set for manual mode 
                "constantBlur": 0,

            }, optionsExplit);
            return this.each(function () {

                function _buildPresent(content, respawnedGoner) {
                    var goner = respawnedGoner ? respawnedGoner : $("<div/>"); //default value = fresh div
                    var buildedPresent = goner
                        .removeClass("goner")
                        .addClass("present")
                        .css({
                            "overflow": "hidden",
                            "white-space": (options.contentWidthMode === "inline") ? "nowrap" : "inherit",
                            "display": "inline-block",
                            "margin-bottom": "-2px",
                            float: "none",
                        });
                    var presentSpan = respawnedGoner ? $("span", respawnedGoner) : $("<span/>").appendTo(buildedPresent);

                    if ($.type(content) === "string") { //do need i?        
                        presentSpan.html(content);
                    } else {
                        presentSpan.html("");
                        content.appendTo(presentSpan);
                    }


                    if (options.contentWidthMode !== "inline") {
                        presentSpan.css({
                            width: (options.contentWidthMode === "self") ? $this.width() : options.contentWidth, //or "manual"
                            position: "relative",
                            display: "block",
                        });
                        buildedPresent.css({
                            width: (options.contentWidthMode === "self") ? $this.width() : options.contentWidth, //or "manual"
                            float: "left",
                        });
                    }
                    return buildedPresent;


                };

                var $this = $(this),
                    data = $this.data('swipeLabel'),
                    options = globalOptions;
                var present, //dom (jquery) onbject
                    grave = []; //object pool, empty on start
                // If the plugin hasn't been initialized yet
                if (!data) {

                    $this.addClass("swipeLabel")
                    var existedContent = $this.html();
                    $this.html("")

                    present = _buildPresent(existedContent).appendTo($this);

                } else {
                    present = data.present;
                    grave = data.grave;
                    options = $.extend(data.options, optionsExplit); //update explicit
                }



                //HotStart/reuse from here                
                if ((optionsExplit != undefined) && (optionsExplit.content != null)) {
                    var goner = present;


                    var newPresent = grave.pop();
                    newPresent = _buildPresent(options.content, newPresent); //fire in both cases, create new or opdate graved - see logic in function

                    goner.removeClass("present").addClass("goner")
                        .stop(true, false)
                        .animate({
                            "width": 0,
                            "opacity": 0
                        }, options.duration, function () {
                            //goner.remove()
                            goner.css({ display: "none" });
                            (grave.length < 10) ? grave.push(goner) : goner.remove();
                        });



                    newPresent
                        .css(
                        {
                            width: 0, //collapse before adding to document and before animate
                            opacity: 0,
                            display: "inline-block"
                        }); //collapse before adding to document and before animate
                    if (options.direction == "left") {
                        newPresent.appendTo($this);
                    } else {
                        newPresent.prependTo($this);
                    }

                    var newPresentSpan = $("span", newPresent);

                    var widthGoal = (options.contentWidthMode === "inline") ? newPresentSpan.width() : (options.contentWidthMode === "self") ? $this.width() : options.contentWidth;  //or "manual"
                    newPresent.animate({
                        "width": widthGoal,
                        "opacity": 1
                    }, options.duration);


                    $this.stop(true, false).animate({
                        "min-width": widthGoal,
                    }, Math.max(options.duration, 200));

                    present = newPresent;

                }

                $this.data("swipeLabel", {
                    options: options,
                    present: present,
                    grave: grave,
                });

                //_constantBlur
                var shadesCount = Math.min(10, Math.round(Math.abs(options.constantBlur / 1)));
                var cssShadowSet = "";
                for (var i = 0; i < shadesCount; i++) {
                    iBri = i / shadesCount; //beware shadesCount=0, pass in loop only i>0!
                    cssShadowSet += +(iBri * Math.sqrt(Math.abs(options.constantBlur)) * 9) + "px 0 0 rgba(255,255,255," + iBri / 2 + "), ";
                    cssShadowSet += -(iBri * Math.sqrt(Math.abs(options.constantBlur)) * 9) + "px 0 0 rgba(255,255,255," + iBri / 2 + "), ";
                }
                $this.css("text-shadow", cssShadowSet.slice(0, -2));


            });
        },
        destroy: function () {

            return this.each(function () {

                var $this = $(this),
                    data = $this.data('swipeLabel');

                // Namespacing FTW
                $(window).unbind('.swipeLabel');
                data.swipeLabel.remove();
                $this.removeData('swipeLabel');

            })

        },
        reposition: function () { },
        show: function () { },
        hide: function () { },
        update: function (content) { }
    };

    $.fn.swipeLabel = function (method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.swipeLabel');
        }

    };
})(jQuery);