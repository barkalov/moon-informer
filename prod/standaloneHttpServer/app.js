'use strict';
const logger = require("./logger");
const noCache = false; // for dev
let path = require('path');
let express = require('express');
let app = express();

// view engine setup
const expressStaticOptions = {};
const oneHour = 1000 * 60 * 60 * 1;
if (!noCache) {
  expressStaticOptions.immutable = true;
  expressStaticOptions.maxAge = oneHour;
}
app.use(express.static(path.join(__dirname, 'static'), expressStaticOptions));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = (req.app.get('env') === 'development') ? err : {};
  //res.locals.error = err;
  // render the error page
  res.status(err.status || 500);
  res.render('serversideError');
  //throw err;
});

module.exports = app;
